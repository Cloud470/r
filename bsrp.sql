/*
Navicat MySQL Data Transfer

Source Server         : faaez
Source Server Version : 50505
Source Host           : 217.182.173.169:3306
Source Database       : bsrp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-27 16:29:29
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `addon_account`
-- ----------------------------
DROP TABLE IF EXISTS `addon_account`;
CREATE TABLE `addon_account` (
  `name` varchar(60) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon_account
-- ----------------------------
INSERT INTO `addon_account` VALUES ('bank_savings', 'Livret Bleu', '0');
INSERT INTO `addon_account` VALUES ('caution', 'Caution', '0');
INSERT INTO `addon_account` VALUES ('property_black_money', 'Argent Sale Propriété', '0');
INSERT INTO `addon_account` VALUES ('society_aircraftdealer', 'Airliner', '1');
INSERT INTO `addon_account` VALUES ('society_ambulance', 'Ambulance', '1');
INSERT INTO `addon_account` VALUES ('society_ammunation', 'Ammunation', '1');
INSERT INTO `addon_account` VALUES ('society_army', 'Army', '1');
INSERT INTO `addon_account` VALUES ('society_avocat', 'Avocat', '1');
INSERT INTO `addon_account` VALUES ('society_bahama', 'Bahama Mas', '1');
INSERT INTO `addon_account` VALUES ('society_banker', 'Banque', '1');
INSERT INTO `addon_account` VALUES ('society_biker', 'Biker', '1');
INSERT INTO `addon_account` VALUES ('society_boatdealer', 'Marina', '1');
INSERT INTO `addon_account` VALUES ('society_brinks', 'Brinks', '1');
INSERT INTO `addon_account` VALUES ('society_cardealer', 'Concessionnaire', '1');
INSERT INTO `addon_account` VALUES ('society_cartel', 'Cartel', '1');
INSERT INTO `addon_account` VALUES ('society_famillies', 'Famillies', '1');
INSERT INTO `addon_account` VALUES ('society_fbi', 'FBI', '1');
INSERT INTO `addon_account` VALUES ('society_mafia', 'Mafia', '1');
INSERT INTO `addon_account` VALUES ('society_mecano', 'Mécano', '1');
INSERT INTO `addon_account` VALUES ('society_mercenaire', 'mercenaire', '1');
INSERT INTO `addon_account` VALUES ('society_mercenaire_black', 'mercenaire black', '1');
INSERT INTO `addon_account` VALUES ('society_motoshop', 'Moto-Concess', '1');
INSERT INTO `addon_account` VALUES ('society_ms13', 'ms13', '1');
INSERT INTO `addon_account` VALUES ('society_ms13_black', 'ms13 black', '1');
INSERT INTO `addon_account` VALUES ('society_police', 'Police', '1');
INSERT INTO `addon_account` VALUES ('society_realestateagent', 'Agent immobilier', '1');
INSERT INTO `addon_account` VALUES ('society_state', 'State', '1');
INSERT INTO `addon_account` VALUES ('society_syndicat', 'syndicat', '1');
INSERT INTO `addon_account` VALUES ('society_syndicat_black', 'syndicat black', '1');
INSERT INTO `addon_account` VALUES ('society_taxe_brinks', 'Brinks Taxe', '1');
INSERT INTO `addon_account` VALUES ('society_taxi', 'Taxi', '1');
INSERT INTO `addon_account` VALUES ('society_unicorn', 'Unicorn', '1');
INSERT INTO `addon_account` VALUES ('society_vagos', 'Vagos', '1');
INSERT INTO `addon_account` VALUES ('society_vigne', 'Vigneron', '1');

-- ----------------------------
-- Table structure for `addon_account_data`
-- ----------------------------
DROP TABLE IF EXISTS `addon_account_data`;
CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=778 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon_account_data
-- ----------------------------
INSERT INTO `addon_account_data` VALUES ('726', 'society_aircraftdealer', '0', null);
INSERT INTO `addon_account_data` VALUES ('727', 'society_ambulance', '1000', null);
INSERT INTO `addon_account_data` VALUES ('728', 'society_ammunation', '1844', null);
INSERT INTO `addon_account_data` VALUES ('729', 'society_army', '0', null);
INSERT INTO `addon_account_data` VALUES ('730', 'society_avocat', '0', null);
INSERT INTO `addon_account_data` VALUES ('731', 'society_bahama', '0', null);
INSERT INTO `addon_account_data` VALUES ('733', 'society_banker', '0', null);
INSERT INTO `addon_account_data` VALUES ('734', 'society_biker', '732450', null);
INSERT INTO `addon_account_data` VALUES ('735', 'society_boatdealer', '0', null);
INSERT INTO `addon_account_data` VALUES ('736', 'society_brinks', '0', null);
INSERT INTO `addon_account_data` VALUES ('737', 'society_cardealer', '260001', null);
INSERT INTO `addon_account_data` VALUES ('738', 'society_cartel', '0', null);
INSERT INTO `addon_account_data` VALUES ('739', 'society_famillies', '0', null);
INSERT INTO `addon_account_data` VALUES ('740', 'society_fbi', '0', null);
INSERT INTO `addon_account_data` VALUES ('741', 'society_mafia', '0', null);
INSERT INTO `addon_account_data` VALUES ('742', 'society_mecano', '26770', 'steam:11000010c2709d9');
INSERT INTO `addon_account_data` VALUES ('743', 'society_mercenaire', '0', null);
INSERT INTO `addon_account_data` VALUES ('744', 'society_mercenaire_black', '0', null);
INSERT INTO `addon_account_data` VALUES ('745', 'society_motoshop', '2340', null);
INSERT INTO `addon_account_data` VALUES ('746', 'society_ms13', '0', null);
INSERT INTO `addon_account_data` VALUES ('747', 'society_ms13_black', '0', null);
INSERT INTO `addon_account_data` VALUES ('748', 'society_police', '42110', null);
INSERT INTO `addon_account_data` VALUES ('749', 'society_state', '0', null);
INSERT INTO `addon_account_data` VALUES ('750', 'society_syndicat', '0', null);
INSERT INTO `addon_account_data` VALUES ('751', 'society_syndicat_black', '0', null);
INSERT INTO `addon_account_data` VALUES ('752', 'society_taxe_brinks', '0', null);
INSERT INTO `addon_account_data` VALUES ('753', 'society_taxi', '2072', null);
INSERT INTO `addon_account_data` VALUES ('754', 'society_unicorn', '0', null);
INSERT INTO `addon_account_data` VALUES ('755', 'society_vagos', '350000', null);
INSERT INTO `addon_account_data` VALUES ('756', 'society_vigne', '0', null);
INSERT INTO `addon_account_data` VALUES ('757', 'caution', '0', 'steam:11000010c2709d9');
INSERT INTO `addon_account_data` VALUES ('758', 'bank_savings', '0', 'steam:11000010c2709d9');
INSERT INTO `addon_account_data` VALUES ('759', 'property_black_money', '0', 'steam:11000010c2709d9');
INSERT INTO `addon_account_data` VALUES ('760', 'property_black_money', '0', 'steam:110000104c989b7');
INSERT INTO `addon_account_data` VALUES ('761', 'caution', '0', 'steam:110000104c989b7');
INSERT INTO `addon_account_data` VALUES ('762', 'bank_savings', '0', 'steam:110000104c989b7');
INSERT INTO `addon_account_data` VALUES ('763', 'property_black_money', '0', 'steam:11000010c2b4bd6');
INSERT INTO `addon_account_data` VALUES ('764', 'bank_savings', '0', 'steam:11000010c2b4bd6');
INSERT INTO `addon_account_data` VALUES ('765', 'caution', '0', 'steam:11000010c2b4bd6');
INSERT INTO `addon_account_data` VALUES ('766', 'society_ammu', '0', null);
INSERT INTO `addon_account_data` VALUES ('767', 'society_realestateagent', '215000', null);
INSERT INTO `addon_account_data` VALUES ('768', 'property_black_money', '0', 'steam:110000117fd8bdf');
INSERT INTO `addon_account_data` VALUES ('769', 'bank_savings', '0', 'steam:110000117fd8bdf');
INSERT INTO `addon_account_data` VALUES ('770', 'caution', '0', 'steam:110000117fd8bdf');
INSERT INTO `addon_account_data` VALUES ('771', 'caution', '0', 'steam:11000010f9264c0');
INSERT INTO `addon_account_data` VALUES ('772', 'bank_savings', '0', 'steam:11000010f9264c0');
INSERT INTO `addon_account_data` VALUES ('773', 'property_black_money', '0', 'steam:11000010f9264c0');
INSERT INTO `addon_account_data` VALUES ('775', 'caution', '0', 'steam:110000105e314d0');
INSERT INTO `addon_account_data` VALUES ('776', 'bank_savings', '0', 'steam:110000105e314d0');
INSERT INTO `addon_account_data` VALUES ('777', 'property_black_money', '0', 'steam:110000105e314d0');

-- ----------------------------
-- Table structure for `addon_inventory`
-- ----------------------------
DROP TABLE IF EXISTS `addon_inventory`;
CREATE TABLE `addon_inventory` (
  `name` varchar(60) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon_inventory
-- ----------------------------
INSERT INTO `addon_inventory` VALUES ('property', 'Propriété', '0');
INSERT INTO `addon_inventory` VALUES ('society_aircraftdealer', 'Airliner', '1');
INSERT INTO `addon_inventory` VALUES ('society_ambulance', 'Ambulance', '1');
INSERT INTO `addon_inventory` VALUES ('society_ammunation', 'Ammunation', '1');
INSERT INTO `addon_inventory` VALUES ('society_army', 'Army', '1');
INSERT INTO `addon_inventory` VALUES ('society_avocat', 'Avocat', '1');
INSERT INTO `addon_inventory` VALUES ('society_bahama', 'Bahama Mas', '1');
INSERT INTO `addon_inventory` VALUES ('society_biker', 'Biker', '1');
INSERT INTO `addon_inventory` VALUES ('society_boatdealer', 'Marina', '1');
INSERT INTO `addon_inventory` VALUES ('society_brinks', 'Brinks', '1');
INSERT INTO `addon_inventory` VALUES ('society_cardealer', 'Concesionnaire', '1');
INSERT INTO `addon_inventory` VALUES ('society_cartel', 'Cartel', '1');
INSERT INTO `addon_inventory` VALUES ('society_famillies', 'Famillies', '1');
INSERT INTO `addon_inventory` VALUES ('society_fbi', 'FBI', '1');
INSERT INTO `addon_inventory` VALUES ('society_mafia', 'Mafia', '1');
INSERT INTO `addon_inventory` VALUES ('society_mecano', 'Mécano', '1');
INSERT INTO `addon_inventory` VALUES ('society_motoshop', 'Moto-concess', '1');
INSERT INTO `addon_inventory` VALUES ('society_police', 'Police', '1');
INSERT INTO `addon_inventory` VALUES ('society_state', 'State', '1');
INSERT INTO `addon_inventory` VALUES ('society_taxi', 'Taxi', '1');
INSERT INTO `addon_inventory` VALUES ('society_unicorn', 'Unicorn', '1');
INSERT INTO `addon_inventory` VALUES ('society_unicorn_fridge', 'Unicorn (frigo)', '1');
INSERT INTO `addon_inventory` VALUES ('society_vagos', 'Vagos', '1');
INSERT INTO `addon_inventory` VALUES ('society_vigne', 'Vigneron', '1');

-- ----------------------------
-- Table structure for `addon_inventory_items`
-- ----------------------------
DROP TABLE IF EXISTS `addon_inventory_items`;
CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon_inventory_items
-- ----------------------------
INSERT INTO `addon_inventory_items` VALUES ('160', 'society_ammunation', 'acetone', '122', null);
INSERT INTO `addon_inventory_items` VALUES ('161', 'society_ammunation', 'coke', '0', null);
INSERT INTO `addon_inventory_items` VALUES ('162', 'society_ammunation', 'lithium', '0', null);
INSERT INTO `addon_inventory_items` VALUES ('163', 'society_ammunation', 'meth', '0', null);
INSERT INTO `addon_inventory_items` VALUES ('164', 'society_ammunation', 'weed_pooch', '60', null);
INSERT INTO `addon_inventory_items` VALUES ('165', 'society_ammunation', 'opium_pooch', '56', null);
INSERT INTO `addon_inventory_items` VALUES ('166', 'society_ammunation', 'ketamine_pooch', '70', null);
INSERT INTO `addon_inventory_items` VALUES ('167', 'society_ammunation', 'methlab', '0', null);
INSERT INTO `addon_inventory_items` VALUES ('168', 'society_ammunation', 'meth_pooch', '66', null);
INSERT INTO `addon_inventory_items` VALUES ('169', 'society_ammunation', 'assaultrifle', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('170', 'society_ammunation', 'acier', '42', null);
INSERT INTO `addon_inventory_items` VALUES ('171', 'society_ammunation', 'pistol', '50', null);
INSERT INTO `addon_inventory_items` VALUES ('172', 'society_ammunation', 'douille', '0', null);
INSERT INTO `addon_inventory_items` VALUES ('173', 'society_ammunation', 'poudre', '36', null);
INSERT INTO `addon_inventory_items` VALUES ('174', 'society_ammunation', 'carbon', '40', null);
INSERT INTO `addon_inventory_items` VALUES ('175', 'society_ammunation', 'sim', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('176', 'society_ammunation', 'mg_ammo', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('177', 'society_ammunation', 'ball_ammo', '101', null);
INSERT INTO `addon_inventory_items` VALUES ('178', 'society_ammunation', 'cigarette', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('179', 'society_ammunation', 'nightstick', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('180', 'society_ammunation', 'flashlight', '1', null);
INSERT INTO `addon_inventory_items` VALUES ('181', 'society_ammunation', 'clip', '26', null);
INSERT INTO `addon_inventory_items` VALUES ('182', 'property', 'acetone', '0', 'steam:11000010c2709d9');
INSERT INTO `addon_inventory_items` VALUES ('183', 'property', 'acier', '0', 'steam:11000010c2709d9');
INSERT INTO `addon_inventory_items` VALUES ('184', 'society_taxi', 'douille', '38', null);

-- ----------------------------
-- Table structure for `aircraft_categories`
-- ----------------------------
DROP TABLE IF EXISTS `aircraft_categories`;
CREATE TABLE `aircraft_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aircraft_categories
-- ----------------------------
INSERT INTO `aircraft_categories` VALUES ('plane', 'Planes');
INSERT INTO `aircraft_categories` VALUES ('heli', 'Helicopters');

-- ----------------------------
-- Table structure for `aircraftdealer_aircrafts`
-- ----------------------------
DROP TABLE IF EXISTS `aircraftdealer_aircrafts`;
CREATE TABLE `aircraftdealer_aircrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aircraftdealer_aircrafts
-- ----------------------------

-- ----------------------------
-- Table structure for `aircrafts`
-- ----------------------------
DROP TABLE IF EXISTS `aircrafts`;
CREATE TABLE `aircrafts` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aircrafts
-- ----------------------------
INSERT INTO `aircrafts` VALUES ('Alpha Z1', 'alphaz1', '1121000', 'plane');
INSERT INTO `aircrafts` VALUES ('Besra', 'besra', '1000000', 'plane');
INSERT INTO `aircrafts` VALUES ('Cuban 800', 'cuban800', '240000', 'plane');
INSERT INTO `aircrafts` VALUES ('Dodo', 'dodo', '500000', 'plane');
INSERT INTO `aircrafts` VALUES ('Duster', 'duster', '175000', 'plane');
INSERT INTO `aircrafts` VALUES ('Howard NX25', 'howard', '975000', 'plane');
INSERT INTO `aircrafts` VALUES ('Luxor', 'luxor', '1500000', 'plane');
INSERT INTO `aircrafts` VALUES ('Luxor Deluxe ', 'luxor2', '1750000', 'plane');
INSERT INTO `aircrafts` VALUES ('Mallard', 'stunt', '250000', 'plane');
INSERT INTO `aircrafts` VALUES ('Mammatus', 'mammatus', '300000', 'plane');
INSERT INTO `aircrafts` VALUES ('Nimbus', 'nimbus', '900000', 'plane');
INSERT INTO `aircrafts` VALUES ('Rogue', 'rogue', '1000000', 'plane');
INSERT INTO `aircrafts` VALUES ('Sea Breeze', 'seabreeze', '850000', 'plane');
INSERT INTO `aircrafts` VALUES ('Shamal', 'shamal', '1150000', 'plane');
INSERT INTO `aircrafts` VALUES ('Ultra Light', 'microlight', '50000', 'plane');
INSERT INTO `aircrafts` VALUES ('Velum', 'velum2', '450000', 'plane');
INSERT INTO `aircrafts` VALUES ('Vestra', 'vestra', '950000', 'plane');
INSERT INTO `aircrafts` VALUES ('Buzzard', 'buzzard2', '500000', 'heli');
INSERT INTO `aircrafts` VALUES ('Frogger', 'frogger', '800000', 'heli');
INSERT INTO `aircrafts` VALUES ('Havok', 'havok', '250000', 'heli');
INSERT INTO `aircrafts` VALUES ('Maverick', 'maverick', '750000', 'heli');
INSERT INTO `aircrafts` VALUES ('Sea Sparrow', 'seasparrow', '815000', 'heli');
INSERT INTO `aircrafts` VALUES ('SuperVolito', 'supervolito', '1000000', 'heli');
INSERT INTO `aircrafts` VALUES ('SuperVolito Carbon', 'supervolito2', '1250000', 'heli');
INSERT INTO `aircrafts` VALUES ('Swift', 'swift', '1000000', 'heli');
INSERT INTO `aircrafts` VALUES ('Swift Deluxe', 'swift2', '1250000', 'heli');
INSERT INTO `aircrafts` VALUES ('Volatus', 'volatus', '1250000', 'heli');

-- ----------------------------
-- Table structure for `baninfo`
-- ----------------------------
DROP TABLE IF EXISTS `baninfo`;
CREATE TABLE `baninfo` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of baninfo
-- ----------------------------
INSERT INTO `baninfo` VALUES ('steam:110000112d7fdb5', 'license:c602038bf2b715426bc34b2056f92bc0434209f4', 'live:1055521878197382', 'no info', 'discord:317024435045662720', 'ip:86.208.214.120', 'Ethan ROSS | QuentinFR');

-- ----------------------------
-- Table structure for `banlist`
-- ----------------------------
DROP TABLE IF EXISTS `banlist`;
CREATE TABLE `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `added` datetime NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of banlist
-- ----------------------------

-- ----------------------------
-- Table structure for `banlisthistory`
-- ----------------------------
DROP TABLE IF EXISTS `banlisthistory`;
CREATE TABLE `banlisthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of banlisthistory
-- ----------------------------

-- ----------------------------
-- Table structure for `batterie_phone`
-- ----------------------------
DROP TABLE IF EXISTS `batterie_phone`;
CREATE TABLE `batterie_phone` (
  `owner` varchar(255) NOT NULL,
  `pourcentage` int(11) NOT NULL DEFAULT 100,
  PRIMARY KEY (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of batterie_phone
-- ----------------------------

-- ----------------------------
-- Table structure for `billing`
-- ----------------------------
DROP TABLE IF EXISTS `billing`;
CREATE TABLE `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of billing
-- ----------------------------
INSERT INTO `billing` VALUES ('2', 'steam:11000010a68611e', 'steam:110000111907ebc', 'society', 'society_vagos', 'Racket : Raket', '350000');
INSERT INTO `billing` VALUES ('3', 'steam:11000010a68611e', 'steam:110000111907ebc', 'society', 'society_vagos', 'Racket : Raket', '150000');
INSERT INTO `billing` VALUES ('4', 'steam:11000010a68611e', 'steam:110000111907ebc', 'society', 'society_vagos', 'Racket : Raket', '3000');
INSERT INTO `billing` VALUES ('31', 'steam:1100001374208bd', 'steam:11000010b5bc1f5', 'society', 'society_mecano', 'Mecano', '100');
INSERT INTO `billing` VALUES ('33', 'steam:110000130ff5ec6', 'steam:11000010a68611e', 'society', 'society_ambulance', 'Ambulance', '250');
INSERT INTO `billing` VALUES ('47', 'steam:110000130ff5ec6', 'steam:11000010a68611e', 'society', 'society_ambulance', 'Ambulance', '500');
INSERT INTO `billing` VALUES ('121', 'steam:11000010a68611e', 'steam:110000111907ebc', 'society', 'society_cardealer', 'Concessionnaire', '71500');
INSERT INTO `billing` VALUES ('138', 'steam:110000130ff5ec6', 'steam:11000010b7c00f1', 'society', 'society_mecano', 'Mecano', '1000');
INSERT INTO `billing` VALUES ('161', 'steam:11000010c2b4bd6', 'steam:11000010c2709d9', 'society', 'society_ammunation', 'Ammu-Nation', '120');

-- ----------------------------
-- Table structure for `boat_categories`
-- ----------------------------
DROP TABLE IF EXISTS `boat_categories`;
CREATE TABLE `boat_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of boat_categories
-- ----------------------------
INSERT INTO `boat_categories` VALUES ('boat', 'Boats');
INSERT INTO `boat_categories` VALUES ('import', 'Import');

-- ----------------------------
-- Table structure for `boatdealer_boats`
-- ----------------------------
DROP TABLE IF EXISTS `boatdealer_boats`;
CREATE TABLE `boatdealer_boats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of boatdealer_boats
-- ----------------------------

-- ----------------------------
-- Table structure for `boats`
-- ----------------------------
DROP TABLE IF EXISTS `boats`;
CREATE TABLE `boats` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of boats
-- ----------------------------
INSERT INTO `boats` VALUES ('Seashark', 'seashark', '1500', 'boat');
INSERT INTO `boats` VALUES ('Seashark2', 'seashark2', '1500', 'boat');
INSERT INTO `boats` VALUES ('Yacht Seashark', 'seashark3', '1500', 'boat');
INSERT INTO `boats` VALUES ('Suntrap', 'suntrap', '1500', 'boat');
INSERT INTO `boats` VALUES ('Dinghy', 'dinghy', '2500', 'boat');
INSERT INTO `boats` VALUES ('Dinghy2 ', 'dinghy2', '2500', 'boat');
INSERT INTO `boats` VALUES ('Yacht Dinghy', 'dinghy4', '1500', 'boat');
INSERT INTO `boats` VALUES ('Tropic', 'tropic', '10000', 'boat');
INSERT INTO `boats` VALUES ('Yacht Tropic', 'tropic2', '10000', 'boat');
INSERT INTO `boats` VALUES ('Squalo', 'squalo', '12000', 'boat');
INSERT INTO `boats` VALUES ('Yacht Toro', 'toro2', '15000', 'boat');
INSERT INTO `boats` VALUES ('Toro', 'toro', '15000', 'boat');
INSERT INTO `boats` VALUES ('Jetmax', 'jetmax', '17500', 'boat');
INSERT INTO `boats` VALUES ('Voilier Marquis', 'marquis', '45500', 'boat');
INSERT INTO `boats` VALUES ('Yatch de Luxe', 'sr650fly', '400000', 'import');

-- ----------------------------
-- Table structure for `cardealer_vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `cardealer_vehicles`;
CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cardealer_vehicles
-- ----------------------------
INSERT INTO `cardealer_vehicles` VALUES ('47', 'sentinel2', '25000');
INSERT INTO `cardealer_vehicles` VALUES ('57', 'ren_lecar', '9999');
INSERT INTO `cardealer_vehicles` VALUES ('58', 'sultanrs', '365000');

-- ----------------------------
-- Table structure for `characters`
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'M',
  `height` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES ('1', 'steam:11000010c2709d9', 'Romain', 'Paganotto', '18-02-1997', 'm', '175');
INSERT INTO `characters` VALUES ('2', 'steam:110000104c989b7', 'Harry', 'Chards', '14-02-1990', 'm', '200');
INSERT INTO `characters` VALUES ('3', 'steam:11000010c2b4bd6', 'Alex', 'Rony', '17-02-1997', 'm', '187');
INSERT INTO `characters` VALUES ('4', 'steam:110000117fd8bdf', 'Joe', 'Cook', '05-11-1995', 'm', '190');
INSERT INTO `characters` VALUES ('5', 'steam:11000010f9264c0', 'Neil', 'Barrett', '31/10/1997', 'm', '186');
INSERT INTO `characters` VALUES ('6', 'steam:110000105e314d0', 'Jean-Pierre', 'Chapin', '11-02-1995', 'm', '185');

-- ----------------------------
-- Table structure for `datastore`
-- ----------------------------
DROP TABLE IF EXISTS `datastore`;
CREATE TABLE `datastore` (
  `name` varchar(60) NOT NULL,
  `label` varchar(255) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datastore
-- ----------------------------
INSERT INTO `datastore` VALUES ('property', 'Propriété', '0');
INSERT INTO `datastore` VALUES ('society_ambulance', 'Ambulance', '1');
INSERT INTO `datastore` VALUES ('society_ammunation', 'Ammunation', '1');
INSERT INTO `datastore` VALUES ('society_army', 'Army', '1');
INSERT INTO `datastore` VALUES ('society_avocat', 'Avocat', '1');
INSERT INTO `datastore` VALUES ('society_ballas_weapons', 'ballas weapon', '1');
INSERT INTO `datastore` VALUES ('society_biker', 'Biker', '1');
INSERT INTO `datastore` VALUES ('society_brinks', 'Brinks', '1');
INSERT INTO `datastore` VALUES ('society_cartel', 'Cartel', '1');
INSERT INTO `datastore` VALUES ('society_famillies', 'Famillies', '1');
INSERT INTO `datastore` VALUES ('society_fbi', 'FBI', '1');
INSERT INTO `datastore` VALUES ('society_kano', 'Kano', '1');
INSERT INTO `datastore` VALUES ('society_kano_weapons', 'kano weapon', '1');
INSERT INTO `datastore` VALUES ('society_mafia', 'Mafia', '1');
INSERT INTO `datastore` VALUES ('society_mercenaire', 'mercenaire', '1');
INSERT INTO `datastore` VALUES ('society_mercenaire_weapons', 'mercenaire Weapon', '1');
INSERT INTO `datastore` VALUES ('society_ms13', 'ms13 Weapon', '1');
INSERT INTO `datastore` VALUES ('society_ms13_weapons', 'ms13 weapon', '1');
INSERT INTO `datastore` VALUES ('society_police', 'Police', '1');
INSERT INTO `datastore` VALUES ('society_state', 'State', '1');
INSERT INTO `datastore` VALUES ('society_syndicat', 'syndicat', '1');
INSERT INTO `datastore` VALUES ('society_taxi', 'Taxi', '1');
INSERT INTO `datastore` VALUES ('society_unicorn', 'Unicorn', '1');
INSERT INTO `datastore` VALUES ('society_vagos', 'Vagos', '1');
INSERT INTO `datastore` VALUES ('society_vigne', 'Vigneron', '1');
INSERT INTO `datastore` VALUES ('user_ears', 'Ears', '0');
INSERT INTO `datastore` VALUES ('user_glasses', 'Glasses', '0');
INSERT INTO `datastore` VALUES ('user_helmet', 'Helmet', '0');
INSERT INTO `datastore` VALUES ('user_mask', 'Mask', '0');

-- ----------------------------
-- Table structure for `datastore_data`
-- ----------------------------
DROP TABLE IF EXISTS `datastore_data`;
CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `owner` varchar(60) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_datastore_owner_name` (`owner`,`name`),
  KEY `index_datastore_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1243 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of datastore_data
-- ----------------------------
INSERT INTO `datastore_data` VALUES ('1187', 'society_ambulance', null, '{}');
INSERT INTO `datastore_data` VALUES ('1188', 'society_ammunation', null, '{\"weapons\":[{\"name\":\"WEAPON_FLASHLIGHT\",\"count\":0},{\"name\":\"WEAPON_NIGHTSTICK\",\"count\":0},{\"name\":\"WEAPON_PISTOL\",\"count\":1},{\"name\":\"WEAPON_ASSAULTRIFLE\",\"count\":0},{\"name\":\"WEAPON_PISTOL50\",\"count\":0},{\"name\":\"WEAPON_STUNGUN\",\"count\":1}]}');
INSERT INTO `datastore_data` VALUES ('1189', 'society_army', null, '{}');
INSERT INTO `datastore_data` VALUES ('1190', 'society_avocat', null, '{}');
INSERT INTO `datastore_data` VALUES ('1191', 'society_ballas', null, '{}');
INSERT INTO `datastore_data` VALUES ('1192', 'society_ballas_weapons', null, '{}');
INSERT INTO `datastore_data` VALUES ('1193', 'society_biker', null, '{\"weapons\":[{\"name\":\"WEAPON_ASSAULTRIFLE\",\"count\":1},{\"name\":\"WEAPON_CARBINERIFLE\",\"count\":0},{\"name\":\"WEAPON_REVOLVER\",\"count\":0},{\"name\":\"WEAPON_COMBATPISTOL\",\"count\":13},{\"name\":\"WEAPON_ASSAULTSMG\",\"count\":1},{\"name\":\"WEAPON_FLASHLIGHT\",\"count\":1},{\"name\":\"WEAPON_APPISTOL\",\"count\":0}]}');
INSERT INTO `datastore_data` VALUES ('1194', 'society_brinks', null, '{}');
INSERT INTO `datastore_data` VALUES ('1195', 'society_cartel', null, '{}');
INSERT INTO `datastore_data` VALUES ('1196', 'society_famillies', null, '{}');
INSERT INTO `datastore_data` VALUES ('1197', 'society_fbi', null, '{}');
INSERT INTO `datastore_data` VALUES ('1198', 'society_kano', null, '{}');
INSERT INTO `datastore_data` VALUES ('1199', 'society_kano_weapons', null, '{}');
INSERT INTO `datastore_data` VALUES ('1200', 'society_mafia', null, '{}');
INSERT INTO `datastore_data` VALUES ('1201', 'society_mercenaire', null, '{}');
INSERT INTO `datastore_data` VALUES ('1202', 'society_mercenaire_weapons', null, '{}');
INSERT INTO `datastore_data` VALUES ('1203', 'society_ms13', null, '{}');
INSERT INTO `datastore_data` VALUES ('1204', 'society_ms13_weapons', null, '{}');
INSERT INTO `datastore_data` VALUES ('1205', 'society_police', null, '{}');
INSERT INTO `datastore_data` VALUES ('1206', 'society_state', null, '{}');
INSERT INTO `datastore_data` VALUES ('1207', 'society_syndicat', null, '{}');
INSERT INTO `datastore_data` VALUES ('1208', 'society_taxi', null, '{\"garage\":[]}');
INSERT INTO `datastore_data` VALUES ('1209', 'society_unicorn', null, '{}');
INSERT INTO `datastore_data` VALUES ('1210', 'society_vagos', null, '{}');
INSERT INTO `datastore_data` VALUES ('1211', 'society_vigne', null, '{}');
INSERT INTO `datastore_data` VALUES ('1212', 'user_mask', 'steam:11000010c2709d9', '{\"skin\":{\"mask_1\":5,\"mask_2\":0},\"hasMask\":true}');
INSERT INTO `datastore_data` VALUES ('1213', 'property', 'steam:11000010c2709d9', '{}');
INSERT INTO `datastore_data` VALUES ('1214', 'user_helmet', 'steam:11000010c2709d9', '{}');
INSERT INTO `datastore_data` VALUES ('1215', 'user_ears', 'steam:11000010c2709d9', '{}');
INSERT INTO `datastore_data` VALUES ('1216', 'user_glasses', 'steam:11000010c2709d9', '{}');
INSERT INTO `datastore_data` VALUES ('1217', 'property', 'steam:110000104c989b7', '{}');
INSERT INTO `datastore_data` VALUES ('1218', 'user_mask', 'steam:110000104c989b7', '{}');
INSERT INTO `datastore_data` VALUES ('1219', 'user_helmet', 'steam:110000104c989b7', '{}');
INSERT INTO `datastore_data` VALUES ('1220', 'user_glasses', 'steam:110000104c989b7', '{}');
INSERT INTO `datastore_data` VALUES ('1221', 'user_ears', 'steam:110000104c989b7', '{}');
INSERT INTO `datastore_data` VALUES ('1222', 'property', 'steam:11000010c2b4bd6', '{}');
INSERT INTO `datastore_data` VALUES ('1223', 'user_ears', 'steam:11000010c2b4bd6', '{}');
INSERT INTO `datastore_data` VALUES ('1224', 'user_helmet', 'steam:11000010c2b4bd6', '{}');
INSERT INTO `datastore_data` VALUES ('1225', 'user_glasses', 'steam:11000010c2b4bd6', '{}');
INSERT INTO `datastore_data` VALUES ('1226', 'user_mask', 'steam:11000010c2b4bd6', '{}');
INSERT INTO `datastore_data` VALUES ('1227', 'society_ammu', null, '{}');
INSERT INTO `datastore_data` VALUES ('1228', 'user_glasses', 'steam:110000117fd8bdf', '{}');
INSERT INTO `datastore_data` VALUES ('1229', 'user_helmet', 'steam:110000117fd8bdf', '{}');
INSERT INTO `datastore_data` VALUES ('1230', 'user_ears', 'steam:110000117fd8bdf', '{}');
INSERT INTO `datastore_data` VALUES ('1231', 'property', 'steam:110000117fd8bdf', '{}');
INSERT INTO `datastore_data` VALUES ('1232', 'user_mask', 'steam:110000117fd8bdf', '{}');
INSERT INTO `datastore_data` VALUES ('1233', 'user_mask', 'steam:11000010f9264c0', '{}');
INSERT INTO `datastore_data` VALUES ('1234', 'user_helmet', 'steam:11000010f9264c0', '{}');
INSERT INTO `datastore_data` VALUES ('1235', 'property', 'steam:11000010f9264c0', '{}');
INSERT INTO `datastore_data` VALUES ('1236', 'user_ears', 'steam:11000010f9264c0', '{}');
INSERT INTO `datastore_data` VALUES ('1237', 'user_glasses', 'steam:11000010f9264c0', '{}');
INSERT INTO `datastore_data` VALUES ('1238', 'user_ears', 'steam:110000105e314d0', '{}');
INSERT INTO `datastore_data` VALUES ('1239', 'user_glasses', 'steam:110000105e314d0', '{}');
INSERT INTO `datastore_data` VALUES ('1240', 'user_helmet', 'steam:110000105e314d0', '{}');
INSERT INTO `datastore_data` VALUES ('1241', 'property', 'steam:110000105e314d0', '{}');
INSERT INTO `datastore_data` VALUES ('1242', 'user_mask', 'steam:110000105e314d0', '{}');

-- ----------------------------
-- Table structure for `demandevisa`
-- ----------------------------
DROP TABLE IF EXISTS `demandevisa`;
CREATE TABLE `demandevisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomrp` varchar(100) DEFAULT NULL,
  `age` date DEFAULT NULL,
  `inforp` varchar(5000) DEFAULT NULL,
  `histoire` varchar(5000) DEFAULT NULL,
  `exprp` varchar(5000) DEFAULT NULL,
  `discord` varchar(100) DEFAULT NULL,
  `statut` int(1) DEFAULT NULL,
  `identifier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demandevisa
-- ----------------------------
INSERT INTO `demandevisa` VALUES ('12', 'Jordan James', '1996-12-07', 'LSPD', 'L\'histoire de James Jordan : Depuis qu’il est tout petit, il a vécu dans une caserne avec des policiers car malheureusement ses parents l’ont abandonnés. Il a tout fais pour pouvoir passer son diplôme de gardien de la paix et l’a réussi avec succès, suite à sa remise de diplôme il a travaillé à Los Angeles en tant qu’officier. Il y a quelque jours, il a été promu à Los Santos.\r\n\r\n', 'California RP\r\nArietys RP\r\nEt bien d\'autres dont je me souviens plus les noms', 'Satuko#7797', '1', 'steam:11000013ce87d46');
INSERT INTO `demandevisa` VALUES ('13', 'Foch Ryan', '1999-02-02', 'Illégal', 'Ryan jeune de cité arrivé en ville prêt a tout pour faire de l\'argent.\r\nIl n\'hésitera pas a trahir ses collaborateurs si une affaire tourne mal.\r\nIl est intelligent débrouillard mais ne pense qu\'a l\'argent ce qui peux lui faire default.', 'Je joue depuis environ 2 semaines grâce a EpicGames airlines :) \r\nPendant ces 2 semaines j\'ai fait du rp légal en intérim et participé a des petits évents mais aussi de l\'illégal\r\nJ\'ai longtemps regardé du rp et connait a peu prés ces particularités, j\'ai également lu les règles.\r\n', 'texor#2730', '1', 'steam:110000115d54ec8');
INSERT INTO `demandevisa` VALUES ('14', 'Davis Killian ', '1995-09-05', 'Illégal / légal ', 'Mon personnage sera un Ghost : \r\nNom de Code: Frogmen\r\nNom: Davis \r\nPrénom: Killian\r\nAge: 25 ans\r\nDate de naissance: 05/09/1995\r\nLieu de Naissance: Californie\r\nÉtudes: Etudes scientifiques \r\nFormation/années de services: De très bon résultat scolaire. Il rentre tout d\'abord a 18 ans dans l\'armée poussé par sont père qui n\'est d\'autre qu\'un général de l\'armée américaine. Au bout de 2 ans et après être fait remarque sur de grandes opérations par les Navy Seals il reçus une propositions pour faire les tests et les rejoindre se qui aura lieu et il fut élus major de sa promo.\r\nMissions effectués en tant qu\'Officier: Faisant parti de la 1ere équipe des Navy Seals c\'est a dire de ma Naval Spécial Warfare Command Group One (NSWG-1) et de l\'équipe 1 de cette unité il à effectué un très grand nombre de mission CLASSE TOP SECRET \r\nMissions effectuées en tant que Commando:Faisant parti de la 1ere équipe des Navy Seals c\'est a dire de ma Naval Spécial Warfare Command Group One (NSWG-1) et de l\'équipe 1 de cette unité il à effectué un très grand nombre de mission CLASSE TOP SECRET \r\nCaractère: Calme, mais peut si il le faut très vite énerver, réfléchis et intelligent. \r\nCompétences: Très bon tireur, conducteur, nageur, sportif et furtif du a son passé.\r\nNota bene: Avant de quitter les Navy Seals pour venger sont meilleur pote qui a été abattu lors d\'une mission par des terroristes ils avait le grade de Sergent Chef et dirigeais l\'unité 1. Ils a dut quitter l\'armée car les haut gradée ne voulait rien faire pour rapatrier le corp de sont meilleur ami mais aussi pour le venger. Il parti donc a 25 ans et devenu un mercenaire très réputé d\'où le fait qu\'ils est été recrutée par les Ghost.', 'J\'ai commencer le RP tout d\'abord sur Arma 3 et Garry\'s Mod puis depuis Decembre je fais du rp sur un serveur Gta RP (Newstart qui est un serveur Semi Whitelist ). Sur se serveur j\'ai commencé par faire du légal puis je me suis lancer dans un RP mafia ensuite Gang ( Bloods ) et maintenant un RP police. Je connais les Bases du RP et je sais si il le faut me remettre en question. En espérant que ma candidature vous suffise et vous plaise, bonne continuations Killian Davis.', 'KLIWAN #0672', '1', 'steam:11000011b56043a');
INSERT INTO `demandevisa` VALUES ('15', 'Gabriel Costa', '1987-08-03', 'Illégal/Ghost', 'Dans le dossier des Ghost ', '+1200 heures gmod +300 fiveM', 'Majorspic', '1', 'steam:11000011530fefa');
INSERT INTO `demandevisa` VALUES ('16', 'Hamilton Darry', '1990-10-20', 'Du rp illégal au sein du groupe Ghost', 'Darren est née en Californie, sa mère l\'a abandonné a sa naissance, son père s\'est fait, grand trafiquant au sein de la Californie. Darren décide de tout changer et d\'oublier toutes les mauvaises expériences de sa famille. Il se lance donc dans l\'armée a Los Santos pour servir sa patrie mais il se rendra compte que l\'Etat n\'est pas plus honnête que le monde dans lequel son père à vécu en Californie. Lors de son service militaire, Darren rencontrera Gabriel, Blanco, Killian et Georges. Il vont exercer plusieurs missions pour l’État. Depuis sa dernière mission  il avait une tout autre vision du gouvernement, pour lui cela n\'était qu’un énorme tas de merdes vivantes chacun ne pensant qu\'à leurs gueules et qu\'à leurs frics. Alors ses jeunes amis arrêta leur service et se mirent l\'idée en tête de créer une organisation illégale pour devenir les rois de Los Santos...', 'Plus de 1000 heures sur FiveM, je me considère comme un joueur compétente, voyant l\'ouverture de ce serveur je lui donne une chance et c\'est pour cette raison que je veux le rejoindre en espérant qu\'il marche avec succès', 'Rotays #0262', '1', 'steam:110000112ca1eb4');
INSERT INTO `demandevisa` VALUES ('17', 'Eras Blanco', '1994-07-16', 'Illégal et Légal', 'L\'histoire de mon personnage est expliqué dans le dossier des Ghost', 'J\'ai à peu prêt 1500 heure de rp sur Five M. J\'ai été le Parrain d\'une mafia nommé la Cosa Nostra. Nous avions plusieurs business comme des drogues ou des ventes d\'objets types traceurs GPS, avec ce personnage j\'ai été le patron d\'un bar nommé l\'unicorn. J\'ai été le gérant d\'une brasserie.\r\nPar la suite, j\'ai fais un rp gang (Bloods) nous avions la weed, nous faisions bouger la ville car des organisations ou même des gangs ne faisaient rien en ville. C\'est à dire qu\'ils n\'osaient rien faire par peur de perdre leur personnage. Alors nous faisions des guerres face à d\'autres groupes. Donc ce personnage de Bloods permettait principalement à faire bouger les groupes qui n\'osaient rien faire.\r\nPour finir, j\'ai fais un rp O\'Leary, c\'est à dire un rp fermier, nous faisions de l\'illégal, pêche (requins, tortues) puis notre but était de ce faire respecter partout en ville. L\'un des buts principaux et que nous interceptions toutes les marchandises qui montait dans le nord comme les armes, drogues ou autre. Chaque personne qui osait braquer une supérette ou une banque dans le nord tombait alors sur nous. ', 'Friox#0408 ou sur le serveur discord BLanco Eras', '1', 'steam:11000010f6a8e70');
INSERT INTO `demandevisa` VALUES ('18', 'Spike Davis', '1979-06-15', 'Vie légale et à côté Ghost ', 'Tout est sur le Dossier &quot;Ghost&quot;', 'Whitelist New Start | GTA LITE ', 'Dyptik #4691 ', '1', 'steam:11000013cf19312');
INSERT INTO `demandevisa` VALUES ('19', 'Dargo Tim', '1989-06-25', 'Légale/Illégale/Ghost', 'Voir dossier des Ghost', 'Ce serveur sera mon premier sur FiveM mais je suit beaucoup de stream RP et pense être en mesure de RP.', 'Odaxy#4144', '1', 'steam:1100001348da95b');

-- ----------------------------
-- Table structure for `dpkeybinds`
-- ----------------------------
DROP TABLE IF EXISTS `dpkeybinds`;
CREATE TABLE `dpkeybinds` (
  `id` varchar(50) DEFAULT NULL,
  `keybind1` varchar(50) DEFAULT 'num4',
  `emote1` varchar(255) DEFAULT '',
  `keybind2` varchar(50) DEFAULT 'num5',
  `emote2` varchar(255) DEFAULT '',
  `keybind3` varchar(50) DEFAULT 'num6',
  `emote3` varchar(255) DEFAULT '',
  `keybind4` varchar(50) DEFAULT 'num7',
  `emote4` varchar(255) DEFAULT '',
  `keybind5` varchar(50) DEFAULT 'num8',
  `emote5` varchar(255) DEFAULT '',
  `keybind6` varchar(50) DEFAULT 'num9',
  `emote6` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dpkeybinds
-- ----------------------------
INSERT INTO `dpkeybinds` VALUES ('steam:11000010c2709d9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
INSERT INTO `dpkeybinds` VALUES ('steam:110000104c989b7', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
INSERT INTO `dpkeybinds` VALUES ('steam:11000010c2b4bd6', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
INSERT INTO `dpkeybinds` VALUES ('steam:110000117fd8bdf', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
INSERT INTO `dpkeybinds` VALUES ('steam:11000010f9264c0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
INSERT INTO `dpkeybinds` VALUES ('steam:110000105e314d0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');

-- ----------------------------
-- Table structure for `fine_types`
-- ----------------------------
DROP TABLE IF EXISTS `fine_types`;
CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fine_types
-- ----------------------------
INSERT INTO `fine_types` VALUES ('1', 'Usage abusif du klaxon', '30', '0');
INSERT INTO `fine_types` VALUES ('2', 'Franchir une ligne continue', '40', '0');
INSERT INTO `fine_types` VALUES ('3', 'Circulation à contresens', '250', '0');
INSERT INTO `fine_types` VALUES ('4', 'Demi-tour non autorisé', '250', '0');
INSERT INTO `fine_types` VALUES ('5', 'Circulation hors-route', '170', '0');
INSERT INTO `fine_types` VALUES ('6', 'Non-respect des distances de sécurité', '30', '0');
INSERT INTO `fine_types` VALUES ('7', 'Arrêt dangereux / interdit', '150', '0');
INSERT INTO `fine_types` VALUES ('8', 'Stationnement gênant / interdit', '70', '0');
INSERT INTO `fine_types` VALUES ('9', 'Non respect  de la priorité à droite', '70', '0');
INSERT INTO `fine_types` VALUES ('10', 'Non-respect à un véhicule prioritaire', '90', '0');
INSERT INTO `fine_types` VALUES ('11', 'Non-respect d\'un stop', '105', '0');
INSERT INTO `fine_types` VALUES ('12', 'Non-respect d\'un feu rouge', '130', '0');
INSERT INTO `fine_types` VALUES ('13', 'Dépassement dangereux', '100', '0');
INSERT INTO `fine_types` VALUES ('14', 'Véhicule non en état', '100', '0');
INSERT INTO `fine_types` VALUES ('15', 'Conduite sans permis', '1500', '0');
INSERT INTO `fine_types` VALUES ('16', 'Délit de fuite', '800', '0');
INSERT INTO `fine_types` VALUES ('17', 'Excès de vitesse < 5 kmh', '90', '0');
INSERT INTO `fine_types` VALUES ('18', 'Excès de vitesse 5-15 kmh', '120', '0');
INSERT INTO `fine_types` VALUES ('19', 'Excès de vitesse 15-30 kmh', '180', '0');
INSERT INTO `fine_types` VALUES ('20', 'Excès de vitesse > 30 kmh', '300', '0');
INSERT INTO `fine_types` VALUES ('21', 'Entrave de la circulation', '110', '1');
INSERT INTO `fine_types` VALUES ('22', 'Dégradation de la voie publique', '90', '1');
INSERT INTO `fine_types` VALUES ('23', 'Trouble à l\'ordre publique', '90', '1');
INSERT INTO `fine_types` VALUES ('24', 'Entrave opération de police', '130', '1');
INSERT INTO `fine_types` VALUES ('25', 'Insulte envers / entre civils', '75', '1');
INSERT INTO `fine_types` VALUES ('26', 'Outrage à agent de police', '110', '1');
INSERT INTO `fine_types` VALUES ('27', 'Menace verbale ou intimidation envers civil', '90', '1');
INSERT INTO `fine_types` VALUES ('28', 'Menace verbale ou intimidation envers policier', '150', '1');
INSERT INTO `fine_types` VALUES ('29', 'Manifestation illégale', '250', '1');
INSERT INTO `fine_types` VALUES ('30', 'Tentative de corruption', '1500', '1');
INSERT INTO `fine_types` VALUES ('31', 'Arme blanche sortie en ville', '120', '2');
INSERT INTO `fine_types` VALUES ('32', 'Arme léthale sortie en ville', '300', '2');
INSERT INTO `fine_types` VALUES ('33', 'Port d\'arme non autorisé (défaut de license)', '600', '2');
INSERT INTO `fine_types` VALUES ('34', 'Port d\'arme illégal', '700', '2');
INSERT INTO `fine_types` VALUES ('35', 'Pris en flag lockpick', '300', '2');
INSERT INTO `fine_types` VALUES ('36', 'Vol de voiture', '1800', '2');
INSERT INTO `fine_types` VALUES ('37', 'Vente de drogue', '1500', '2');
INSERT INTO `fine_types` VALUES ('38', 'Fabriquation de drogue', '1500', '2');
INSERT INTO `fine_types` VALUES ('39', 'Possession de drogue', '650', '2');
INSERT INTO `fine_types` VALUES ('40', 'Prise d\'ôtage civil', '1500', '2');
INSERT INTO `fine_types` VALUES ('41', 'Prise d\'ôtage agent de l\'état', '2000', '2');
INSERT INTO `fine_types` VALUES ('42', 'Braquage particulier', '650', '2');
INSERT INTO `fine_types` VALUES ('43', 'Braquage magasin', '650', '2');
INSERT INTO `fine_types` VALUES ('44', 'Braquage de banque', '1500', '2');
INSERT INTO `fine_types` VALUES ('45', 'Tir sur civil', '2000', '3');
INSERT INTO `fine_types` VALUES ('46', 'Tir sur agent de l\'état', '2500', '3');
INSERT INTO `fine_types` VALUES ('47', 'Tentative de meurtre sur civil', '3000', '3');
INSERT INTO `fine_types` VALUES ('48', 'Tentative de meurtre sur agent de l\'état', '5000', '3');
INSERT INTO `fine_types` VALUES ('49', 'Meurtre sur civil', '10000', '3');
INSERT INTO `fine_types` VALUES ('50', 'Meurte sur agent de l\'état', '30000', '3');
INSERT INTO `fine_types` VALUES ('51', 'Meurtre involontaire', '1800', '3');
INSERT INTO `fine_types` VALUES ('52', 'Escroquerie à l\'entreprise', '2000', '2');

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('acetone', 'Acetone', '50', '0', '1');
INSERT INTO `items` VALUES ('acier', 'Acier', '-1', '0', '1');
INSERT INTO `items` VALUES ('advancedrifle', 'CTAR-21', '-1', '0', '1');
INSERT INTO `items` VALUES ('alcool', 'alcool', '-1', '0', '1');
INSERT INTO `items` VALUES ('alcool_cargo', 'Cargaison d\'alcool', '-1', '0', '1');
INSERT INTO `items` VALUES ('alive_chicken', 'Poulet vivant', '20', '0', '1');
INSERT INTO `items` VALUES ('ammoanalyzer', 'Ammo Analyzer', '1', '0', '1');
INSERT INTO `items` VALUES ('appistol', 'Colt SCAMP', '-1', '0', '1');
INSERT INTO `items` VALUES ('assaultrifle', 'AK-47', '-1', '0', '1');
INSERT INTO `items` VALUES ('assaultrifle_mk2', 'assaultrifle_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('assaultshotgun', 'UTAS UTS-15', '-1', '0', '1');
INSERT INTO `items` VALUES ('assaultsmg', 'P90', '-1', '0', '1');
INSERT INTO `items` VALUES ('autoshotgun', 'AA-12', '-1', '0', '1');
INSERT INTO `items` VALUES ('bague', 'Bague', '2', '0', '1');
INSERT INTO `items` VALUES ('ball', 'Balle', '-1', '0', '1');
INSERT INTO `items` VALUES ('ball_ammo', 'ball_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('bandage', 'Bandage', '20', '0', '1');
INSERT INTO `items` VALUES ('bankidcard', 'Bank ID', '-1', '0', '1');
INSERT INTO `items` VALUES ('bat', 'Batte de BaseBall', '-1', '0', '1');
INSERT INTO `items` VALUES ('battleaxe', 'Hache', '-1', '0', '1');
INSERT INTO `items` VALUES ('bird_crap_ammo', 'bird_crap_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('bloodsample', 'Blood Sample', '1', '0', '1');
INSERT INTO `items` VALUES ('blowpipe', 'Chalumeaux', '10', '0', '1');
INSERT INTO `items` VALUES ('bolcacahuetes', 'Bol de cacahuètes', '5', '0', '1');
INSERT INTO `items` VALUES ('bolchips', 'Bol de chips', '5', '0', '1');
INSERT INTO `items` VALUES ('bolnoixcajou', 'Bol de noix de cajou', '5', '0', '1');
INSERT INTO `items` VALUES ('bolpistache', 'Bol de pistaches', '5', '0', '1');
INSERT INTO `items` VALUES ('bottle', 'Tesson de Bouteille', '-1', '0', '1');
INSERT INTO `items` VALUES ('bread', 'Pain', '30', '0', '1');
INSERT INTO `items` VALUES ('bulletproof', 'Gilet Pare-Balle', '2', '0', '1');
INSERT INTO `items` VALUES ('bulletsample', 'Bullet Casing', '1', '0', '1');
INSERT INTO `items` VALUES ('bullpuprifle', 'Type 86-S', '-1', '0', '1');
INSERT INTO `items` VALUES ('bullpuprifle_mk2', 'bullpuprifle_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('bullpupshotgun', 'Kel-Tec KSG', '-1', '0', '1');
INSERT INTO `items` VALUES ('bzgas', 'Lacrymogène', '-1', '0', '1');
INSERT INTO `items` VALUES ('carbinerifle', 'M4A1', '-1', '0', '1');
INSERT INTO `items` VALUES ('carbinerifle_mk2', 'carbinerifle_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('carbon', 'Carbone', '-1', '0', '1');
INSERT INTO `items` VALUES ('carokit', 'Kit carosserie', '3', '0', '1');
INSERT INTO `items` VALUES ('carotool', 'outils carosserie', '4', '0', '1');
INSERT INTO `items` VALUES ('cigarette', 'cigarette', '10', '0', '1');
INSERT INTO `items` VALUES ('clip', 'Chargeur', '-1', '0', '1');
INSERT INTO `items` VALUES ('clothe', 'Vêtement', '40', '0', '1');
INSERT INTO `items` VALUES ('coke', 'Coke', '50', '0', '1');
INSERT INTO `items` VALUES ('coke_pooch', 'Pochon de coke', '10', '0', '1');
INSERT INTO `items` VALUES ('combatmg', 'M249E1', '-1', '0', '1');
INSERT INTO `items` VALUES ('combatmg_mk2', 'combatmg_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('combatpdw', 'SIG Sauer MPX', '-1', '0', '1');
INSERT INTO `items` VALUES ('combatpistol', 'Glock 17', '-1', '0', '1');
INSERT INTO `items` VALUES ('compactlauncher', 'M79 GL', '-1', '0', '1');
INSERT INTO `items` VALUES ('compactrifle', 'AK-47U', '-1', '0', '1');
INSERT INTO `items` VALUES ('copper', 'Cuivre', '56', '0', '1');
INSERT INTO `items` VALUES ('crack', 'Crack', '50', '0', '1');
INSERT INTO `items` VALUES ('crack_pooch', 'Pochon de Crack', '10', '0', '1');
INSERT INTO `items` VALUES ('crowbar', 'Pied de Biche', '-1', '0', '1');
INSERT INTO `items` VALUES ('cutted_wood', 'Bois coupé', '20', '0', '1');
INSERT INTO `items` VALUES ('dagger', 'Dague', '-1', '0', '1');
INSERT INTO `items` VALUES ('dbshotgun', 'Zabala short-barreled side-by-side shotgun', '-1', '0', '1');
INSERT INTO `items` VALUES ('diamond', 'Diamant', '50', '0', '1');
INSERT INTO `items` VALUES ('digiscanner', 'digiscanner', '-1', '0', '1');
INSERT INTO `items` VALUES ('dnaanalyzer', 'DNA Analyzer', '1', '0', '1');
INSERT INTO `items` VALUES ('doubleaction', 'doubleaction', '-1', '0', '1');
INSERT INTO `items` VALUES ('douille', 'Boite de douille', '-1', '0', '1');
INSERT INTO `items` VALUES ('drpepper', 'Dr. Pepper', '5', '0', '1');
INSERT INTO `items` VALUES ('ecstasy', 'Ecstasy', '50', '0', '1');
INSERT INTO `items` VALUES ('ecstasy_pooch', 'Pochon decstasy', '10', '0', '1');
INSERT INTO `items` VALUES ('enemy_laser_ammo', 'laser', '100', '0', '1');
INSERT INTO `items` VALUES ('energy', 'Energy Drink', '5', '0', '1');
INSERT INTO `items` VALUES ('esctasy', 'esctasy', '-1', '0', '1');
INSERT INTO `items` VALUES ('essence', 'Essence', '24', '0', '1');
INSERT INTO `items` VALUES ('fabric', 'Tissu', '80', '0', '1');
INSERT INTO `items` VALUES ('fireextinguisher', 'Extincteur', '-1', '0', '1');
INSERT INTO `items` VALUES ('fireextinguisher_ammo', 'poudre d’extincteur', '100', '0', '1');
INSERT INTO `items` VALUES ('firework', 'Firework', '-1', '0', '1');
INSERT INTO `items` VALUES ('fish', 'Poisson', '100', '0', '1');
INSERT INTO `items` VALUES ('fixkit', 'Kit réparation', '5', '0', '1');
INSERT INTO `items` VALUES ('fixtool', 'outils réparation', '6', '0', '1');
INSERT INTO `items` VALUES ('flare', 'Flare', '-1', '0', '1');
INSERT INTO `items` VALUES ('flaregun', 'Pistolet de Detresse', '-1', '0', '1');
INSERT INTO `items` VALUES ('flare_ammo', 'Flares', '100', '0', '1');
INSERT INTO `items` VALUES ('flashlight', 'Lampe', '-1', '0', '1');
INSERT INTO `items` VALUES ('garbagebag', 'garbagebag', '-1', '0', '1');
INSERT INTO `items` VALUES ('gazbottle', 'bouteille de gaz', '11', '0', '1');
INSERT INTO `items` VALUES ('gold', 'Or', '21', '0', '1');
INSERT INTO `items` VALUES ('golem', 'Golem', '5', '0', '1');
INSERT INTO `items` VALUES ('golfclub', 'Club de Golb', '-1', '0', '1');
INSERT INTO `items` VALUES ('gps', 'GPS', '-1', '0', '1');
INSERT INTO `items` VALUES ('grand_cru', 'Grand cru', '-1', '0', '1');
INSERT INTO `items` VALUES ('grapperaisin', 'Grappe de raisin', '5', '0', '1');
INSERT INTO `items` VALUES ('grenade', 'Grenade', '-1', '0', '1');
INSERT INTO `items` VALUES ('grenadelauncher', 'Milkor MGL', '-1', '0', '1');
INSERT INTO `items` VALUES ('grenadelauncher_ammo', 'grenades pour lanceur de grenade', '100', '0', '1');
INSERT INTO `items` VALUES ('grenadelauncher_smoke_ammo', 'grenades fumigène pour lanceur de grenade', '100', '0', '1');
INSERT INTO `items` VALUES ('grip', 'Poignée', '-1', '0', '1');
INSERT INTO `items` VALUES ('gusenberg', 'Gusenberg', '-1', '0', '1');
INSERT INTO `items` VALUES ('gzgas_ammo', 'gaz', '100', '0', '1');
INSERT INTO `items` VALUES ('hammer', 'Marteau', '-1', '0', '1');
INSERT INTO `items` VALUES ('hatchet', 'hachette', '-1', '0', '1');
INSERT INTO `items` VALUES ('heavypistol', 'Colt 1911', '-1', '0', '1');
INSERT INTO `items` VALUES ('heavyshotgun', 'Saiga-12K', '-1', '0', '1');
INSERT INTO `items` VALUES ('heavysniper', 'Barret M82', '-1', '0', '1');
INSERT INTO `items` VALUES ('heavysniper_mk2', 'heavysniper_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('hifi', 'HiFi', '1', '0', '1');
INSERT INTO `items` VALUES ('hominglauncher', 'SA-7 Grail', '-1', '0', '1');
INSERT INTO `items` VALUES ('ice', 'Glaçon', '5', '0', '1');
INSERT INTO `items` VALUES ('icetea', 'Ice Tea', '5', '0', '1');
INSERT INTO `items` VALUES ('iron', 'Fer', '42', '0', '1');
INSERT INTO `items` VALUES ('jager', 'Jägermeister', '5', '0', '1');
INSERT INTO `items` VALUES ('jagerbomb', 'Jägerbomb', '5', '0', '1');
INSERT INTO `items` VALUES ('jagercerbere', 'Jäger Cerbère', '3', '0', '1');
INSERT INTO `items` VALUES ('jusfruit', 'Jus de fruits', '5', '0', '1');
INSERT INTO `items` VALUES ('jus_raisin', 'Jus de raisin', '-1', '0', '1');
INSERT INTO `items` VALUES ('ketamine', 'Ketamine', '50', '0', '1');
INSERT INTO `items` VALUES ('ketamine_pooch', 'Pochon de ketamine', '10', '0', '1');
INSERT INTO `items` VALUES ('knife', 'Knife', '-1', '0', '1');
INSERT INTO `items` VALUES ('knuckle', 'Poing Américain', '-1', '0', '1');
INSERT INTO `items` VALUES ('limonade', 'Limonade', '5', '0', '1');
INSERT INTO `items` VALUES ('lithium', 'Lithium', '50', '0', '1');
INSERT INTO `items` VALUES ('lockpick', 'Lockpick', '10', '0', '1');
INSERT INTO `items` VALUES ('machete', 'Machette', '-1', '0', '1');
INSERT INTO `items` VALUES ('machinepistol', 'TEC-9', '-1', '0', '1');
INSERT INTO `items` VALUES ('marksmanpistol', 'Thompson-Center Contender G2', '-1', '0', '1');
INSERT INTO `items` VALUES ('marksmanrifle', 'M39 EMR', '-1', '0', '1');
INSERT INTO `items` VALUES ('marksmanrifle_mk2', 'marksmanrifle_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('martini', 'Martini blanc', '5', '0', '1');
INSERT INTO `items` VALUES ('medikit', 'Medikit', '5', '0', '1');
INSERT INTO `items` VALUES ('menthe', 'Feuille de menthe', '10', '0', '1');
INSERT INTO `items` VALUES ('meth', 'meth', '50', '0', '1');
INSERT INTO `items` VALUES ('methlab', 'Methilamine', '50', '0', '1');
INSERT INTO `items` VALUES ('meth_pooch', 'Pochon de meth', '10', '0', '1');
INSERT INTO `items` VALUES ('metreshooter', 'Mètre de shooter', '3', '0', '1');
INSERT INTO `items` VALUES ('mg', 'PKP Pecheneg', '-1', '0', '1');
INSERT INTO `items` VALUES ('mg_ammo', 'MG Ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('microsmg', 'Uzi', '-1', '0', '1');
INSERT INTO `items` VALUES ('minigun', 'minigun', '-1', '0', '1');
INSERT INTO `items` VALUES ('minigun_ammo', 'Minigun Ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('minismg', 'Skorpion', '-1', '0', '1');
INSERT INTO `items` VALUES ('mixapero', 'Mix Apéritif', '3', '0', '1');
INSERT INTO `items` VALUES ('mojito', 'Mojito', '5', '0', '1');
INSERT INTO `items` VALUES ('molotov', 'Cocktail Molotov', '-1', '0', '1');
INSERT INTO `items` VALUES ('molotov_ammo', 'cocktail molotov', '100', '0', '1');
INSERT INTO `items` VALUES ('musket', 'Brown Bess', '-1', '0', '1');
INSERT INTO `items` VALUES ('myrte', 'myrte', '-1', '0', '1');
INSERT INTO `items` VALUES ('myrtealcool', 'alcool de myrte', '-1', '0', '1');
INSERT INTO `items` VALUES ('myrte_cargo', 'Cargaison de Myrte', '-1', '0', '1');
INSERT INTO `items` VALUES ('nightstick', 'Matraque', '-1', '0', '1');
INSERT INTO `items` VALUES ('nightvision', 'Night Vision', '-1', '0', '1');
INSERT INTO `items` VALUES ('opium', 'Opium', '50', '0', '1');
INSERT INTO `items` VALUES ('opium_pooch', 'Pochon de opium', '10', '0', '1');
INSERT INTO `items` VALUES ('oxycutter', 'Plasma Torch', '-1', '0', '1');
INSERT INTO `items` VALUES ('pacicficidcard', 'Pacific ID', '-1', '0', '1');
INSERT INTO `items` VALUES ('packaged_chicken', 'Poulet en barquette', '100', '0', '1');
INSERT INTO `items` VALUES ('packaged_plank', 'Paquet de planches', '100', '0', '1');
INSERT INTO `items` VALUES ('parachute', 'Parachute', '-1', '0', '1');
INSERT INTO `items` VALUES ('petrol', 'Pétrole', '24', '0', '1');
INSERT INTO `items` VALUES ('petrolcan', 'Bidon Essence', '-1', '0', '1');
INSERT INTO `items` VALUES ('petrol_raffin', 'Pétrole Raffiné', '24', '0', '1');
INSERT INTO `items` VALUES ('phone', 'Smartphone', '-1', '0', '1');
INSERT INTO `items` VALUES ('pipebomb', 'pipe bomb', '-1', '0', '1');
INSERT INTO `items` VALUES ('pistol', 'Beretta', '-1', '0', '1');
INSERT INTO `items` VALUES ('pistol50', 'Desert Eagle', '-1', '0', '1');
INSERT INTO `items` VALUES ('pistol_ammo', 'munitions pour pistolet', '100', '0', '1');
INSERT INTO `items` VALUES ('pistol_mk2', 'Sig Sauer P226', '-1', '0', '1');
INSERT INTO `items` VALUES ('plane_rocket_ammo', 'plane_rocket_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('player_laser_ammo', 'player_laser_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('poolcue', 'Queue de Billard', '-1', '0', '1');
INSERT INTO `items` VALUES ('poudre', 'Boite de poudre', '-1', '0', '1');
INSERT INTO `items` VALUES ('proxmine', 'Mine de Proximité', '-1', '0', '1');
INSERT INTO `items` VALUES ('pumpshotgun', 'Remington 870', '-1', '0', '1');
INSERT INTO `items` VALUES ('pumpshotgun_mk2', 'pumpshotgun_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('railgun', 'railgun', '-1', '0', '1');
INSERT INTO `items` VALUES ('raisin', 'Raisin', '-1', '0', '1');
INSERT INTO `items` VALUES ('redbull', 'RedBull', '-1', '0', '1');
INSERT INTO `items` VALUES ('redbull_cargo', 'Cargaison de RedBull', '-1', '0', '1');
INSERT INTO `items` VALUES ('remotesniper', 'Dragunov', '-1', '0', '1');
INSERT INTO `items` VALUES ('revolver', 'Raging Bull', '-1', '0', '1');
INSERT INTO `items` VALUES ('revolver_mk2', 'revolver_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('rhum', 'Rhum', '5', '0', '1');
INSERT INTO `items` VALUES ('rhumcoca', 'Rhum-coca', '5', '0', '1');
INSERT INTO `items` VALUES ('rhumfruit', 'Rhum-jus de fruits', '5', '0', '1');
INSERT INTO `items` VALUES ('rifle_ammo', 'Munition pour fusil a fusil d\'assaut', '100', '0', '1');
INSERT INTO `items` VALUES ('rolex', 'Rolex Watch', '-1', '0', '1');
INSERT INTO `items` VALUES ('rpg', 'RPG-7', '-1', '0', '1');
INSERT INTO `items` VALUES ('rpg_ammo', 'RPG Ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('sacbillets', 'Sac de Billets', '1', '0', '1');
INSERT INTO `items` VALUES ('saucisson', 'Saucisson', '5', '0', '1');
INSERT INTO `items` VALUES ('sawnoffshotgun', 'Mossberg 500', '-1', '0', '1');
INSERT INTO `items` VALUES ('shotgun_ammo', 'Munition pour fusil a pompe', '100', '0', '1');
INSERT INTO `items` VALUES ('silencieux', 'Silencieux', '-1', '0', '1');
INSERT INTO `items` VALUES ('sim', 'Carte Sim', '-1', '0', '1');
INSERT INTO `items` VALUES ('slaughtered_chicken', 'Poulet abattu', '20', '0', '1');
INSERT INTO `items` VALUES ('smg', 'MP5', '-1', '0', '1');
INSERT INTO `items` VALUES ('smg_ammo', 'munition pour mitraillette', '100', '0', '1');
INSERT INTO `items` VALUES ('smg_mk2', 'smg_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('smokegrenade', 'Granade Smoke', '-1', '0', '1');
INSERT INTO `items` VALUES ('smokegrenade_ammo', 'grenade gaz', '100', '0', '1');
INSERT INTO `items` VALUES ('sniperrifle', 'PSG-1', '-1', '0', '1');
INSERT INTO `items` VALUES ('sniper_ammo', 'munition pour sniper', '100', '0', '1');
INSERT INTO `items` VALUES ('sniper_remote_ammo', 'Sniper Remote Ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('snowball', 'Boule de Neige', '-1', '0', '1');
INSERT INTO `items` VALUES ('snspistol', 'Pétoire', '-1', '0', '1');
INSERT INTO `items` VALUES ('snspistol_mk2', 'snspistol_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('soda', 'Soda', '5', '0', '1');
INSERT INTO `items` VALUES ('space_rocket_ammo', 'space_rocket_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('specialcarbine', 'G36C', '-1', '0', '1');
INSERT INTO `items` VALUES ('specialcarbine_mk2', 'specialcarbine_mk2', '-1', '0', '1');
INSERT INTO `items` VALUES ('stickybomb', 'Bombe Collante', '-1', '0', '1');
INSERT INTO `items` VALUES ('stickybomb_ammo', 'bombe collante', '100', '0', '1');
INSERT INTO `items` VALUES ('stinger', 'stinger', '-1', '0', '1');
INSERT INTO `items` VALUES ('stinger_ammo', 'stinger_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('stone', 'Pierre', '7', '0', '1');
INSERT INTO `items` VALUES ('stungun', 'Taser', '-1', '0', '1');
INSERT INTO `items` VALUES ('stungun_ammo', 'Stungun Ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('switchblade', 'Crant D arrêt', '-1', '0', '1');
INSERT INTO `items` VALUES ('tank_ammo', 'tank_ammo', '100', '0', '1');
INSERT INTO `items` VALUES ('tel', 'smartphone', '-1', '0', '1');
INSERT INTO `items` VALUES ('teqpaf', 'Teq\'paf', '5', '0', '1');
INSERT INTO `items` VALUES ('tequila', 'Tequila', '5', '0', '1');
INSERT INTO `items` VALUES ('vine', 'Vin', '-1', '0', '1');
INSERT INTO `items` VALUES ('vintagepistol', 'FN Model 1910', '-1', '0', '1');
INSERT INTO `items` VALUES ('vodka', 'Vodka', '5', '0', '1');
INSERT INTO `items` VALUES ('vodkaenergy', 'Vodka-energy', '5', '0', '1');
INSERT INTO `items` VALUES ('vodkafruit', 'Vodka-jus de fruits', '5', '0', '1');
INSERT INTO `items` VALUES ('vodkrb', 'Vodka RedBull', '-1', '0', '1');
INSERT INTO `items` VALUES ('washed_stone', 'Pierre Lavée', '7', '0', '1');
INSERT INTO `items` VALUES ('water', 'Eau', '30', '0', '1');
INSERT INTO `items` VALUES ('weed', 'Weed', '50', '0', '1');
INSERT INTO `items` VALUES ('weed_pooch', 'Pochon de weed', '10', '0', '1');
INSERT INTO `items` VALUES ('whisky', 'Whisky', '5', '0', '1');
INSERT INTO `items` VALUES ('whiskycoc', 'Whisky CorsicaCola', '-1', '0', '1');
INSERT INTO `items` VALUES ('whiskycoca', 'Whisky-coca', '5', '0', '1');
INSERT INTO `items` VALUES ('wood', 'Bois', '20', '0', '1');
INSERT INTO `items` VALUES ('wool', 'Laine', '40', '0', '1');
INSERT INTO `items` VALUES ('wrench', 'Clé à Molette ', '-1', '0', '1');
INSERT INTO `items` VALUES ('yusuf', 'Skin de luxe', '-1', '0', '1');

-- ----------------------------
-- Table structure for `jail`
-- ----------------------------
DROP TABLE IF EXISTS `jail`;
CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `isjailed` tinyint(1) DEFAULT NULL,
  `J_Time` datetime NOT NULL,
  `J_Cell` varchar(20) NOT NULL,
  `Jailer` varchar(100) NOT NULL,
  `Jailer_ID` varchar(100) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jail
-- ----------------------------

-- ----------------------------
-- Table structure for `job_grades`
-- ----------------------------
DROP TABLE IF EXISTS `job_grades`;
CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job_grades
-- ----------------------------
INSERT INTO `job_grades` VALUES ('1', 'unemployed', '0', 'unemployed', 'Chomeur', '100', 'null', 'null');
INSERT INTO `job_grades` VALUES ('2', 'vagos', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('3', 'vagos', '1', 'capo', 'Braqueur', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('4', 'vagos', '2', 'consigliere', 'Bandit', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('5', 'vagos', '3', 'boss', 'Chef du Gang', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('6', 'mafia', '0', 'soldato', 'Ptite-Frappe', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('7', 'mafia', '1', 'capo', 'Capo', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('8', 'mafia', '2', 'consigliere', 'Consigliere', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('9', 'mafia', '3', 'boss', 'Parain', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('10', 'cartel', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('11', 'cartel', '1', 'capo', 'Braqueur', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('12', 'cartel', '2', 'consigliere', 'Mafieux', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('13', 'cartel', '3', 'boss', 'Parrain', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('14', 'biker', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('15', 'biker', '1', 'capo', 'Motard', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('16', 'biker', '2', 'consigliere', 'Bandit', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('17', 'biker', '3', 'boss', 'Chef du Gang', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('18', 'unemployed2', '0', 'unemployed2', 'R.S.A', '100', 'null', 'null');
INSERT INTO `job_grades` VALUES ('19', 'unemployed3', '0', 'unemployed3', 'Organisation', '100', 'null', 'null');
INSERT INTO `job_grades` VALUES ('20', 'lumberjack', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('21', 'fisherman', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('22', 'fueler', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('23', 'reporter', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('24', 'tailor', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('25', 'miner', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('26', 'slaughterer', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job_grades` VALUES ('32', 'taxi', '0', 'recrue', 'Recrue', '25', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}');
INSERT INTO `job_grades` VALUES ('33', 'taxi', '1', 'novice', 'Novice', '50', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}');
INSERT INTO `job_grades` VALUES ('34', 'taxi', '2', 'experimente', 'Experimente', '75', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}');
INSERT INTO `job_grades` VALUES ('35', 'taxi', '3', 'uber', 'Uber', '100', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}');
INSERT INTO `job_grades` VALUES ('36', 'taxi', '4', 'boss', 'Patron', '125', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":29,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":1,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":4,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}');
INSERT INTO `job_grades` VALUES ('37', 'ambulance', '0', 'ambulance', 'Ambulancier', '20', '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES ('38', 'ambulance', '1', 'doctor', 'Medecin', '40', '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES ('39', 'ambulance', '2', 'chief_doctor', 'Medecin-chef', '60', '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES ('40', 'ambulance', '3', 'boss', 'Chirurgien', '80', '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES ('41', 'ammunation', '0', 'stagiaire', 'Stagiaire', '12', '{}', '{}');
INSERT INTO `job_grades` VALUES ('42', 'ammunation', '1', 'employé', 'Employé', '36', '{}', '{}');
INSERT INTO `job_grades` VALUES ('43', 'ammunation', '2', 'professionnel', 'Professionnel', '48', '{}', '{}');
INSERT INTO `job_grades` VALUES ('44', 'ammunation', '3', 'second', 'Second', '48', '{}', '{}');
INSERT INTO `job_grades` VALUES ('45', 'ammunation', '4', 'boss', 'Patron', '150', '{}', '{}');
INSERT INTO `job_grades` VALUES ('46', 'army', '0', 'recruit', 'Soldat', '1200', '{}', '{}');
INSERT INTO `job_grades` VALUES ('47', 'army', '1', 'firstclass', '1er Classe', '1350', '{}', '{}');
INSERT INTO `job_grades` VALUES ('48', 'army', '2', 'capo', 'Caporal', '1500', '{}', '{}');
INSERT INTO `job_grades` VALUES ('49', 'army', '3', 'chiefcapo', 'Caporal-Chef', '1700', '{}', '{}');
INSERT INTO `job_grades` VALUES ('50', 'army', '4', 'sergeant', 'Sergent', '1850', '{}', '{}');
INSERT INTO `job_grades` VALUES ('51', 'army', '5', 'chiefsergeant', 'Sergent-Chef', '2000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('52', 'army', '6', 'adjudant', 'Adjudant', '2100', '{}', '{}');
INSERT INTO `job_grades` VALUES ('53', 'army', '7', 'chiefadjudant', 'Adjudant-Chef', '2250', '{}', '{}');
INSERT INTO `job_grades` VALUES ('54', 'army', '8', 'major', 'Major', '2400', '{}', '{}');
INSERT INTO `job_grades` VALUES ('55', 'army', '9', 'aspirant', 'Aspirant', '2550', '{}', '{}');
INSERT INTO `job_grades` VALUES ('56', 'army', '10', 'souslieutenant', 'Sous-lieutenant', '2700', '{}', '{}');
INSERT INTO `job_grades` VALUES ('57', 'army', '11', 'lieutenant', 'Lieutenant', '2800', '{}', '{}');
INSERT INTO `job_grades` VALUES ('58', 'army', '12', 'captain', 'Capitaine', '2950', '{}', '{}');
INSERT INTO `job_grades` VALUES ('59', 'army', '13', 'commandant', 'Commandant', '3100', '{}', '{}');
INSERT INTO `job_grades` VALUES ('60', 'army', '14', 'lieutenantcolonel', 'Lieutenant-colonel', '3200', '{}', '{}');
INSERT INTO `job_grades` VALUES ('61', 'army', '15', 'colonel', 'Colonel', '3350', '{}', '{}');
INSERT INTO `job_grades` VALUES ('62', 'army', '16', 'brigadiergeneral', 'Général de brigade', '3550', '{}', '{}');
INSERT INTO `job_grades` VALUES ('63', 'army', '17', 'divisiongeneral', 'Général de division', '3750', '{}', '{}');
INSERT INTO `job_grades` VALUES ('64', 'army', '18', 'generalofthearmycorps', 'Général de corps d\'armée', '4000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('65', 'army', '19', 'generalarmy', 'Général d\'armée', '4700', '{}', '{}');
INSERT INTO `job_grades` VALUES ('66', 'army', '20', 'boss', 'Maréchal', '5200', '{}', '{}');
INSERT INTO `job_grades` VALUES ('67', 'avocat', '0', 'recruit', 'Recrue', '20', '{\"tshirt_1\":57,\"torso_1\":55,\"arms\":0,\"pants_1\":35,\"glasses\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":5,\"face\":19,\"glasses_2\":1,\"torso_2\":0,\"shoes\":24,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":8}', '{\"tshirt_1\":34,\"torso_1\":48,\"shoes\":24,\"pants_1\":34,\"torso_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"glasses\":0,\"helmet_2\":0,\"hair_2\":3,\"face\":21,\"decals_1\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"arms\":14,\"hair_color_1\":10,\"tshirt_2\":0,\"helmet_1\":57}');
INSERT INTO `job_grades` VALUES ('68', 'avocat', '1', 'boss', 'Patron', '100', '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":41,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":8,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":21,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"hair_color_1\":10,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"shoes\":24,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}');
INSERT INTO `job_grades` VALUES ('69', 'bahama', '0', 'recrue', 'Recrue', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('70', 'bahama', '1', 'novice', 'Novice', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('71', 'bahama', '2', 'experimente', 'Experimente', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('72', 'bahama', '3', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('73', 'banker', '0', 'advisor', 'Conseiller', '10', '{}', '{}');
INSERT INTO `job_grades` VALUES ('74', 'banker', '1', 'banker', 'Banquier', '20', '{}', '{}');
INSERT INTO `job_grades` VALUES ('75', 'banker', '2', 'business_banker', 'Banquier d\'affaire', '30', '{}', '{}');
INSERT INTO `job_grades` VALUES ('76', 'banker', '3', 'trader', 'Trader', '40', '{}', '{}');
INSERT INTO `job_grades` VALUES ('77', 'banker', '4', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('112', 'fbi', '0', 'agent', 'Agent', '500', '{}', '{}');
INSERT INTO `job_grades` VALUES ('113', 'fbi', '1', 'special', 'Agent Expérimenté', '1000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('114', 'fbi', '2', 'supervisor', 'Superviseur', '1000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('115', 'fbi', '3', 'assistant', 'Assistant du Directeur', '1500', '{}', '{}');
INSERT INTO `job_grades` VALUES ('116', 'fbi', '4', 'boss', 'Directeur', '2000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('117', 'state', '0', 'vicepresident', 'Garde-du Corp', '10000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('118', 'state', '1', 'president', 'Secret-Service', '18000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('119', 'state', '2', 'gouvernment', 'Procureur', '33000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('120', 'state', '3', 'boss', 'Gouverneur', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('121', 'unicorn', '0', 'barman', 'Barman', '300', '{}', '{}');
INSERT INTO `job_grades` VALUES ('122', 'unicorn', '1', 'dancer', 'Danseur', '300', '{}', '{}');
INSERT INTO `job_grades` VALUES ('123', 'unicorn', '2', 'viceboss', 'Co-gérant', '500', '{}', '{}');
INSERT INTO `job_grades` VALUES ('124', 'unicorn', '3', 'boss', 'Gérant', '600', '{}', '{}');
INSERT INTO `job_grades` VALUES ('125', 'vigne', '0', 'recrue', 'Intérimaire', '500', '{\"tshirt_1\":59,\"tshirt_2\":0,\"torso_1\":12,\"torso_2\":5,\"shoes_1\":7,\"shoes_2\":2,\"pants_1\":9, \"pants_2\":7, \"arms\":1, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}', '{\"tshirt_1\":0,\"tshirt_2\":0,\"torso_1\":56,\"torso_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"pants_1\":36, \"pants_2\":0, \"arms\":63, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}');
INSERT INTO `job_grades` VALUES ('126', 'vigne', '1', 'novice', 'Vigneron', '750', '{\"tshirt_1\":57,\"tshirt_2\":0,\"torso_1\":13,\"torso_2\":5,\"shoes_1\":7,\"shoes_2\":2,\"pants_1\":9, \"pants_2\":7, \"arms\":11, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}', '{\"tshirt_1\":0,\"tshirt_2\":0,\"torso_1\":56,\"torso_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"pants_1\":36, \"pants_2\":0, \"arms\":63, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}');
INSERT INTO `job_grades` VALUES ('127', 'vigne', '2', 'cdisenior', 'Chef de chai', '1200', '{\"tshirt_1\":57,\"tshirt_2\":0,\"torso_1\":13,\"torso_2\":5,\"shoes_1\":7,\"shoes_2\":2,\"pants_1\":9, \"pants_2\":7, \"arms\":11, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}', '{\"tshirt_1\":0,\"tshirt_2\":0,\"torso_1\":56,\"torso_2\":0,\"shoes_1\":27,\"shoes_2\":0,\"pants_1\":36, \"pants_2\":0, \"arms\":63, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}');
INSERT INTO `job_grades` VALUES ('128', 'vigne', '3', 'boss', 'Patron', '1600', '{\"tshirt_1\":57,\"tshirt_2\":0,\"torso_1\":13,\"torso_2\":5,\"shoes_1\":7,\"shoes_2\":2,\"pants_1\":9, \"pants_2\":7, \"arms\":11, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}', '{\"tshirt_1\":15,\"tshirt_2\":0,\"torso_1\":14,\"torso_2\":15,\"shoes_1\":12,\"shoes_2\":0,\"pants_1\":9, \"pants_2\":5, \"arms\":1, \"helmet_1\":11, \"helmet_2\":0,\"bags_1\":0,\"bags_2\":0,\"ears_1\":0,\"glasses_1\":0,\"ears_2\":0,\"glasses_2\":0}');
INSERT INTO `job_grades` VALUES ('129', 'police', '0', 'recruit', 'cadet', '200', '{}', '{}');
INSERT INTO `job_grades` VALUES ('130', 'police', '1', 'officer', 'Officier', '400', '{}', '{}');
INSERT INTO `job_grades` VALUES ('131', 'police', '2', 'sergeant', 'Sergent', '600', '{}', '{}');
INSERT INTO `job_grades` VALUES ('132', 'police', '3', 'lieutenant', 'Lieutenant', '850', '{}', '{}');
INSERT INTO `job_grades` VALUES ('133', 'police', '4', 'boss', 'Capitaine', '1000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('134', 'mecano', '0', 'recrue', 'Recrue', '12', '{}', '{}');
INSERT INTO `job_grades` VALUES ('135', 'mecano', '1', 'novice', 'Novice', '24', '{}', '{}');
INSERT INTO `job_grades` VALUES ('136', 'mecano', '2', 'experimente', 'Experimente', '36', '{}', '{}');
INSERT INTO `job_grades` VALUES ('137', 'mecano', '3', 'chief', 'Chef d\'équipe', '48', '{}', '{}');
INSERT INTO `job_grades` VALUES ('138', 'mecano', '4', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('139', 'aircraftdealer', '0', 'recruit', 'Recruit', '10', '{}', '{}');
INSERT INTO `job_grades` VALUES ('140', 'aircraftdealer', '1', 'novice', 'Novice', '25', '{}', '{}');
INSERT INTO `job_grades` VALUES ('141', 'aircraftdealer', '2', 'experienced', 'Expert', '40', '{}', '{}');
INSERT INTO `job_grades` VALUES ('142', 'aircraftdealer', '3', 'boss', 'Boss', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('143', 'boatdealer', '0', 'recruit', 'Recrue', '10', '{}', '{}');
INSERT INTO `job_grades` VALUES ('144', 'boatdealer', '1', 'novice', 'Novice', '25', '{}', '{}');
INSERT INTO `job_grades` VALUES ('145', 'boatdealer', '2', 'experienced', 'Experimente', '40', '{}', '{}');
INSERT INTO `job_grades` VALUES ('146', 'boatdealer', '3', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('147', 'cardealer', '0', 'recruit', 'Recrue', '100', '{}', '{}');
INSERT INTO `job_grades` VALUES ('148', 'cardealer', '1', 'novice', 'Novice', '250', '{}', '{}');
INSERT INTO `job_grades` VALUES ('149', 'cardealer', '2', 'experienced', 'Experimente', '400', '{}', '{}');
INSERT INTO `job_grades` VALUES ('150', 'cardealer', '3', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('151', 'motoshop', '0', 'recruit', 'Recrue', '500', '{}', '{}');
INSERT INTO `job_grades` VALUES ('152', 'motoshop', '1', 'experienced', 'Experimente', '700', '{}', '{}');
INSERT INTO `job_grades` VALUES ('153', 'motoshop', '2', 'boss', 'Patron', '1000', '{}', '{}');
INSERT INTO `job_grades` VALUES ('154', 'vagos', '0', 'soldato', 'Dealer', '200', 'null', 'null');
INSERT INTO `job_grades` VALUES ('155', 'vagos', '1', 'capo', 'Braqueur', '400', 'null', 'null');
INSERT INTO `job_grades` VALUES ('156', 'vagos', '2', 'consigliere', 'Bandit', '600', 'null', 'null');
INSERT INTO `job_grades` VALUES ('157', 'vagos', '3', 'boss', 'Chef du Gang', '1000', 'null', 'null');
INSERT INTO `job_grades` VALUES ('158', 'famillies', '0', 'soldato', 'Dealer', '200', 'null', 'null');
INSERT INTO `job_grades` VALUES ('159', 'famillies', '1', 'capo', 'Braqueur', '400', 'null', 'null');
INSERT INTO `job_grades` VALUES ('160', 'famillies', '2', 'consigliere', 'Bandit', '600', 'null', 'null');
INSERT INTO `job_grades` VALUES ('161', 'famillies', '3', 'boss', 'Chef du Gang', '1000', 'null', 'null');
INSERT INTO `job_grades` VALUES ('162', 'brinks', '0', 'interim', 'Convoyeur de fonds', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('163', 'brinks', '1', 'employee', 'Gérant Convoyeur', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('164', 'brinks', '2', 'chief', 'Banquier', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('165', 'brinks', '3', 'boss', 'Patron', '0', '{}', '{}');
INSERT INTO `job_grades` VALUES ('171', 'realestateagent', '0', 'location', 'Location', '10', '{}', '{}');
INSERT INTO `job_grades` VALUES ('172', 'realestateagent', '1', 'vendeur', 'Vendeur', '25', '{}', '{}');
INSERT INTO `job_grades` VALUES ('173', 'realestateagent', '2', 'gestion', 'Gestion', '40', '{}', '{}');
INSERT INTO `job_grades` VALUES ('174', 'realestateagent', '3', 'boss', 'Patron', '0', '{}', '{}');

-- ----------------------------
-- Table structure for `job2_grades`
-- ----------------------------
DROP TABLE IF EXISTS `job2_grades`;
CREATE TABLE `job2_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job2_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job2_grades
-- ----------------------------
INSERT INTO `job2_grades` VALUES ('1', 'unemployed2', '0', 'unemployed2', 'Civil', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('2', 'lumberjack', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('3', 'fisherman', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('4', 'fueler', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('5', 'reporter', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('6', 'tailor', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('7', 'miner', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('8', 'slaughterer', '0', 'employee', 'Intérimaire', '0', 'null', 'null');
INSERT INTO `job2_grades` VALUES ('9', 'state', '0', 'vicepresident', 'Vice-Président', '10000', '{}', '{}');
INSERT INTO `job2_grades` VALUES ('10', 'state', '1', 'president', 'Président', '18000', '{}', '{}');
INSERT INTO `job2_grades` VALUES ('11', 'state', '2', 'gouvernment', 'Gouvernement', '33000', '{}', '{}');
INSERT INTO `job2_grades` VALUES ('12', 'state', '3', 'boss', 'L\'état', '0', '{}', '{}');

-- ----------------------------
-- Table structure for `job3_grades`
-- ----------------------------
DROP TABLE IF EXISTS `job3_grades`;
CREATE TABLE `job3_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job3_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of job3_grades
-- ----------------------------
INSERT INTO `job3_grades` VALUES ('1', 'unemployed3', '0', 'unemployed3', 'Organisation', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('6', 'mafia', '0', 'soldato', 'Ptite-Frappe', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('7', 'mafia', '1', 'capo', 'Capo', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('8', 'mafia', '2', 'consigliere', 'Consigliere', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('9', 'mafia', '3', 'boss', 'Parain', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('10', 'cartel', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('11', 'cartel', '1', 'capo', 'Braqueur', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('12', 'cartel', '2', 'consigliere', 'Mafieux', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('13', 'cartel', '3', 'boss', 'Parrain', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('14', 'biker', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('15', 'biker', '1', 'capo', 'Motard', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('16', 'biker', '2', 'consigliere', 'Bandit', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('17', 'biker', '3', 'boss', 'Chef du Gang', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('24', 'kano', '0', 'soldato', 'Soldat', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('25', 'kano', '1', 'capo', 'Assassin', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('26', 'kano', '2', 'consigliere', 'Consigliere', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('27', 'kano', '3', 'righthand', 'Bras Droit', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('28', 'kano', '4', 'boss', 'Chef', '0', '{\"tshirt_1\":15,\"torso_1\":42,\"shoes\":8,\"pants_1\":24,\"pants_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":8,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":0,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":-1}', '{}');
INSERT INTO `job3_grades` VALUES ('63', 'lazone', '0', 'soldato', 'Soldat', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('64', 'lazone', '1', 'capo', 'Assassin', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('65', 'lazone', '2', 'consigliere', 'Consigliere', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('66', 'lazone', '3', 'righthand', 'Bras Droit', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('67', 'lazone', '4', 'boss', 'Chef', '0', '{\"tshirt_1\":15,\"torso_1\":42,\"shoes\":8,\"pants_1\":24,\"pants_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":8,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":0,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":-1}', '{}');
INSERT INTO `job3_grades` VALUES ('73', 'mercenaire', '0', 'soldato', 'Soldat', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('74', 'mercenaire', '1', 'capo', 'Assassin', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('75', 'mercenaire', '2', 'consigliere', 'Consigliere', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('76', 'mercenaire', '3', 'righthand', 'Bras Droit', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('77', 'mercenaire', '4', 'boss', 'Chef', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('78', 'syndicat', '0', 'soldato', 'Soldat', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('79', 'syndicat', '1', 'capo', 'Assassin', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('80', 'syndicat', '2', 'consigliere', 'Consigliere', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('81', 'syndicat', '3', 'righthand', 'Bras Droit', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('82', 'syndicat', '4', 'boss', 'Chef', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('83', 'podolskaia', '0', 'soldato', 'Soldat', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('84', 'podolskaia', '1', 'capo', 'Assassin', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('85', 'podolskaia', '2', 'consigliere', 'Consigliere', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('86', 'podolskaia', '3', 'righthand', 'Bras Droit', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('87', 'podolskaia', '4', 'boss', 'Chef', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('88', 'podolskaia', '0', 'recrue', 'Recrue', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('89', 'podolskaia', '1', 'novice', 'Gérant', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('90', 'podolskaia', '2', 'experimente', 'Frère de sang', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('91', 'podolskaia', '3', 'chief', 'El Patron', '0', '{}', '{}');
INSERT INTO `job3_grades` VALUES ('92', 'vagos', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('93', 'vagos', '1', 'capo', 'Braqueur', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('94', 'vagos', '2', 'consigliere', 'Bandit', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('95', 'vagos', '3', 'boss', 'Chef du Gang', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('96', 'famillies', '0', 'soldato', 'Dealer', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('97', 'famillies', '1', 'capo', 'Braqueur', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('98', 'famillies', '2', 'consigliere', 'Bandit', '0', 'null', 'null');
INSERT INTO `job3_grades` VALUES ('99', 'famillies', '3', 'boss', 'Chef du Gang', '0', 'null', 'null');

-- ----------------------------
-- Table structure for `jobrecrutement`
-- ----------------------------
DROP TABLE IF EXISTS `jobrecrutement`;
CREATE TABLE `jobrecrutement` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `job` varchar(100) DEFAULT NULL,
  `liendoc` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jobrecrutement
-- ----------------------------

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('aircraftdealer', 'Airliner', '0');
INSERT INTO `jobs` VALUES ('ambulance', 'Ambulance', '0');
INSERT INTO `jobs` VALUES ('ammunation', 'Ammunation', '0');
INSERT INTO `jobs` VALUES ('army', 'Army', '0');
INSERT INTO `jobs` VALUES ('avocat', 'AVOCAT', '0');
INSERT INTO `jobs` VALUES ('bahama', 'Bahama Mas', '0');
INSERT INTO `jobs` VALUES ('banker', 'Banquier', '0');
INSERT INTO `jobs` VALUES ('biker', 'Biker', '1');
INSERT INTO `jobs` VALUES ('boatdealer', 'Marina', '0');
INSERT INTO `jobs` VALUES ('brinks', 'Union Depository', '0');
INSERT INTO `jobs` VALUES ('cardealer', 'Concessionnaire', '0');
INSERT INTO `jobs` VALUES ('cartel', 'Cartel', '1');
INSERT INTO `jobs` VALUES ('famillies', 'Famillies', '1');
INSERT INTO `jobs` VALUES ('fbi', 'FBI', '1');
INSERT INTO `jobs` VALUES ('fisherman', 'Pêcheur', '0');
INSERT INTO `jobs` VALUES ('fueler', 'Raffineur', '0');
INSERT INTO `jobs` VALUES ('lumberjack', 'Bûcheron', '0');
INSERT INTO `jobs` VALUES ('mafia', 'Mafia', '1');
INSERT INTO `jobs` VALUES ('mecano', 'Mécano', '0');
INSERT INTO `jobs` VALUES ('miner', 'Mineur', '0');
INSERT INTO `jobs` VALUES ('motoshop', 'concessionnaire moto', '1');
INSERT INTO `jobs` VALUES ('police', 'LSPD', '0');
INSERT INTO `jobs` VALUES ('realestateagent', 'Agent immobilier', '0');
INSERT INTO `jobs` VALUES ('reporter', 'Journaliste', '0');
INSERT INTO `jobs` VALUES ('slaughterer', 'Abatteur', '0');
INSERT INTO `jobs` VALUES ('state', 'Etat', '1');
INSERT INTO `jobs` VALUES ('tailor', 'Couturier', '0');
INSERT INTO `jobs` VALUES ('taxi', 'Taxi', '0');
INSERT INTO `jobs` VALUES ('unemployed', 'Job', '0');
INSERT INTO `jobs` VALUES ('unemployed2', 'Job.2', '0');
INSERT INTO `jobs` VALUES ('unemployed3', 'Job.3', '0');
INSERT INTO `jobs` VALUES ('unicorn', 'Unicorn', '1');
INSERT INTO `jobs` VALUES ('vagos', 'Vagos', '1');
INSERT INTO `jobs` VALUES ('vigne', 'Vigneron', '1');

-- ----------------------------
-- Table structure for `jobs2`
-- ----------------------------
DROP TABLE IF EXISTS `jobs2`;
CREATE TABLE `jobs2` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jobs2
-- ----------------------------
INSERT INTO `jobs2` VALUES ('fisherman', 'Pêcheur', '0');
INSERT INTO `jobs2` VALUES ('fueler', 'Raffineur', '0');
INSERT INTO `jobs2` VALUES ('lumberjack', 'Bûcheron', '0');
INSERT INTO `jobs2` VALUES ('miner', 'Mineur', '0');
INSERT INTO `jobs2` VALUES ('reporter', 'Journaliste', '0');
INSERT INTO `jobs2` VALUES ('slaughterer', 'Abatteur', '0');
INSERT INTO `jobs2` VALUES ('state', 'Etat', '1');
INSERT INTO `jobs2` VALUES ('tailor', 'Couturier', '0');
INSERT INTO `jobs2` VALUES ('unemployed2', 'R.S.A', '0');

-- ----------------------------
-- Table structure for `jobs3`
-- ----------------------------
DROP TABLE IF EXISTS `jobs3`;
CREATE TABLE `jobs3` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jobs3
-- ----------------------------
INSERT INTO `jobs3` VALUES ('biker', 'Biker', '1');
INSERT INTO `jobs3` VALUES ('cartel', 'Cartel', '1');
INSERT INTO `jobs3` VALUES ('famillies', 'Famillies', '1');
INSERT INTO `jobs3` VALUES ('kano', 'Clan kano', '0');
INSERT INTO `jobs3` VALUES ('lazone', 'LaZone', '0');
INSERT INTO `jobs3` VALUES ('mafia', 'Mafia', '1');
INSERT INTO `jobs3` VALUES ('mercenaire', 'Mercenaire', '0');
INSERT INTO `jobs3` VALUES ('ms13', 'MS13', '0');
INSERT INTO `jobs3` VALUES ('none', 'Ã‰tat', '0');
INSERT INTO `jobs3` VALUES ('syndicat', 'Syndicat', '0');
INSERT INTO `jobs3` VALUES ('unemployed3', 'Organisation', '0');
INSERT INTO `jobs3` VALUES ('vagos', 'Vagos', '1');

-- ----------------------------
-- Table structure for `licenses`
-- ----------------------------
DROP TABLE IF EXISTS `licenses`;
CREATE TABLE `licenses` (
  `type` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of licenses
-- ----------------------------
INSERT INTO `licenses` VALUES ('aircraft', 'Aircraft License');
INSERT INTO `licenses` VALUES ('dmv', 'Code de la route');
INSERT INTO `licenses` VALUES ('dmv2', 'Permis de avion');
INSERT INTO `licenses` VALUES ('drive', 'Permis de conduire');
INSERT INTO `licenses` VALUES ('drive_bike', 'Permis moto');
INSERT INTO `licenses` VALUES ('drive_truck', 'Permis camion');
INSERT INTO `licenses` VALUES ('weapon', 'Permis de port d\'arme');

-- ----------------------------
-- Table structure for `moto`
-- ----------------------------
DROP TABLE IF EXISTS `moto`;
CREATE TABLE `moto` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of moto
-- ----------------------------
INSERT INTO `moto` VALUES ('Akuma', 'AKUMA', '7500', 'roadster', 'akuma.jpg');
INSERT INTO `moto` VALUES ('Avarus', 'avarus', '18000', 'american', 'avarus.jpg');
INSERT INTO `moto` VALUES ('Bagger', 'bagger', '13500', 'american', 'bagger.jpg');
INSERT INTO `moto` VALUES ('Bati 801', 'bati', '12000', 'sports', 'bati-801.jpg');
INSERT INTO `moto` VALUES ('Bati 801RR', 'bati2', '19000', 'sports', 'bati-801rr.jpg');
INSERT INTO `moto` VALUES ('BF400', 'bf400', '6500', 'cross', 'bf400.jpg');
INSERT INTO `moto` VALUES ('BMX (velo)', 'bmx', '160', 'bicycles', null);
INSERT INTO `moto` VALUES ('Carbon RS', 'carbonrs', '18000', 'sports', 'carbon-rs.jpg');
INSERT INTO `moto` VALUES ('Chimera', 'chimera', '38000', 'american', 'chimera.jpg');
INSERT INTO `moto` VALUES ('Cliffhanger', 'cliffhanger', '9500', 'cross', 'cliffhanger.jpg');
INSERT INTO `moto` VALUES ('Cruiser', 'cruiser', '510', 'bicycles', null);
INSERT INTO `moto` VALUES ('Daemon', 'daemon', '11500', 'american', 'daemon.jpg');
INSERT INTO `moto` VALUES ('Daemon High', 'daemon2', '13500', 'american', 'daemon2.jpg');
INSERT INTO `moto` VALUES ('Defiler', 'defiler', '9800', 'roadster', 'defiler.jpg');
INSERT INTO `moto` VALUES ('Double T', 'double', '28000', 'sports', 'double-t.jpg');
INSERT INTO `moto` VALUES ('Enduro', 'enduro', '5500', 'cross', 'enduro.jpg');
INSERT INTO `moto` VALUES ('Esskey', 'esskey', '4200', 'roadster', null);
INSERT INTO `moto` VALUES ('Faggio', 'faggio', '1900', 'scooters', 'faggio.jpg');
INSERT INTO `moto` VALUES ('Vespa', 'faggio2', '2800', 'scooters', 'faggio-mod.jpg');
INSERT INTO `moto` VALUES ('Fixter', 'fixter', '225', 'bicycles', null);
INSERT INTO `moto` VALUES ('Gargoyle', 'gargoyle', '16500', 'cross', 'gargoyle.jpg');
INSERT INTO `moto` VALUES ('Hakuchou', 'hakuchou', '31000', 'sports', 'hakuchou.jpg');
INSERT INTO `moto` VALUES ('Hakuchou Sport', 'hakuchou2', '55000', 'sports', 'hakuchou-drag-bike.jpg');
INSERT INTO `moto` VALUES ('Hexer', 'hexer', '12000', 'american', 'hexer.jpg');
INSERT INTO `moto` VALUES ('Innovation', 'innovation', '23500', 'american', 'innovation.jpg');
INSERT INTO `moto` VALUES ('Manchez', 'manchez', '5300', 'cross', 'manchez.jpg');
INSERT INTO `moto` VALUES ('Nemesis', 'nemesis', '5800', 'roadster', 'nemesis.jpg');
INSERT INTO `moto` VALUES ('Nightblade', 'nightblade', '35000', 'american', 'nightblade.jpg');
INSERT INTO `moto` VALUES ('PCJ-600', 'pcj', '6200', 'roadster', 'pcj-600.jpg');
INSERT INTO `moto` VALUES ('Ruffian', 'ruffian', '6800', 'roadster', 'ruffian.jpg');
INSERT INTO `moto` VALUES ('Sanchez', 'sanchez', '5300', 'cross', 'sanchez.jpg');
INSERT INTO `moto` VALUES ('Sanchez Sport', 'sanchez2', '5300', 'cross', null);
INSERT INTO `moto` VALUES ('Sanctus', 'sanctus', '25000', 'concepts', 'sanctus.jpg');
INSERT INTO `moto` VALUES ('Scorcher', 'scorcher', '280', 'bicycles', null);
INSERT INTO `moto` VALUES ('Sovereign', 'sovereign', '22000', 'american', 'sovereign.jpg');
INSERT INTO `moto` VALUES ('Shotaro', 'shotaro', '275000', 'concepts', null);
INSERT INTO `moto` VALUES ('Thrust', 'thrust', '24000', 'sports', 'thrust.jpg');
INSERT INTO `moto` VALUES ('Tri bike', 'tribike3', '520', 'bicycles', null);
INSERT INTO `moto` VALUES ('Vader', 'vader', '7200', 'roadster', 'vader.jpg');
INSERT INTO `moto` VALUES ('Vortex', 'vortex', '9800', 'roadster', 'vortex.jpg');
INSERT INTO `moto` VALUES ('Woflsbane', 'wolfsbane', '9000', 'american', 'wolfsbane.jpg');
INSERT INTO `moto` VALUES ('Zombie', 'zombiea', '9500', 'american', 'zombie-chopper.jpg');
INSERT INTO `moto` VALUES ('Zombie Luxuary', 'zombieb', '12000', 'american', 'zombie-bobber.jpg');
INSERT INTO `moto` VALUES ('Deathbike', 'deathbike', '20000', 'concepts', null);

-- ----------------------------
-- Table structure for `moto_categories`
-- ----------------------------
DROP TABLE IF EXISTS `moto_categories`;
CREATE TABLE `moto_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of moto_categories
-- ----------------------------
INSERT INTO `moto_categories` VALUES ('american', 'Americaine');
INSERT INTO `moto_categories` VALUES ('sports', 'Sportives');
INSERT INTO `moto_categories` VALUES ('roadster', 'Roadsters');
INSERT INTO `moto_categories` VALUES ('cross', 'Cross');
INSERT INTO `moto_categories` VALUES ('concepts', 'Concepts');
INSERT INTO `moto_categories` VALUES ('scooters', 'Scooters');
INSERT INTO `moto_categories` VALUES ('bicycles', 'Velo');

-- ----------------------------
-- Table structure for `moto_sold`
-- ----------------------------
DROP TABLE IF EXISTS `moto_sold`;
CREATE TABLE `moto_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of moto_sold
-- ----------------------------
INSERT INTO `moto_sold` VALUES ('Alexrony', 'BF400', 'HDT 976', 'Dan Smith', '2020-05-23 10:48');
INSERT INTO `moto_sold` VALUES ('Alexrony', 'BMX (velo)', 'INI 335', 'Dan Smith', '2020-05-23 10:50');
INSERT INTO `moto_sold` VALUES ('Alexrony', 'Woflsbane', 'VUM 700', 'Dan Smith', '2020-05-23 10:46');

-- ----------------------------
-- Table structure for `motoshop_vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `motoshop_vehicles`;
CREATE TABLE `motoshop_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of motoshop_vehicles
-- ----------------------------

-- ----------------------------
-- Table structure for `owned_aircrafts`
-- ----------------------------
DROP TABLE IF EXISTS `owned_aircrafts`;
CREATE TABLE `owned_aircrafts` (
  `owner` varchar(30) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the aircraft',
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of owned_aircrafts
-- ----------------------------

-- ----------------------------
-- Table structure for `owned_boats`
-- ----------------------------
DROP TABLE IF EXISTS `owned_boats`;
CREATE TABLE `owned_boats` (
  `owner` varchar(30) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the boat',
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of owned_boats
-- ----------------------------
INSERT INTO `owned_boats` VALUES ('steam:110000104c989b7', '1', 'IUN 694', '{\"modGrille\":-1,\"modRoof\":-1,\"modLivery\":-1,\"modFender\":-1,\"windowTint\":-1,\"modExhaust\":-1,\"modTrunk\":-1,\"modTank\":-1,\"modAerials\":-1,\"modPlateHolder\":-1,\"modFrontWheels\":-1,\"modBackWheels\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"dirtLevel\":0.0,\"modRearBumper\":-1,\"modArmor\":-1,\"modSpeakers\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":28,\"modDoorSpeaker\":-1,\"plate\":\"IUN 694\",\"modEngineBlock\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modAirFilter\":-1,\"modHydrolic\":-1,\"color1\":29,\"wheelColor\":156,\"modHorns\":-1,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modSuspension\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"health\":974,\"modArchCover\":-1,\"wheels\":0,\"modEngine\":-1,\"modDial\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"model\":-1030275036,\"tyreSmokeColor\":[255,255,255],\"modSideSkirt\":-1,\"modTrimA\":-1,\"neonColor\":[255,0,255],\"color2\":0,\"extras\":[],\"modFrame\":-1,\"modVanityPlate\":-1,\"modSpoilers\":-1,\"modSeats\":-1,\"modShifterLeavers\":-1,\"modSmokeEnabled\":1,\"modHood\":-1,\"modWindows\":-1}');
INSERT INTO `owned_boats` VALUES ('steam:11000010c2709d9', '1', 'FVA 162', '{\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modRoof\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"modAerials\":-1,\"modHorns\":-1,\"color1\":111,\"modShifterLeavers\":-1,\"pearlescentColor\":111,\"modPlateHolder\":-1,\"modSuspension\":-1,\"modStruts\":-1,\"extras\":[],\"modSpeakers\":-1,\"modSteeringWheel\":-1,\"modDashboard\":-1,\"modTrimB\":-1,\"plateIndex\":4,\"wheelColor\":156,\"modBrakes\":-1,\"modBackWheels\":-1,\"modTank\":-1,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1,\"color2\":32,\"modTrimA\":-1,\"modAPlate\":-1,\"modFrame\":-1,\"modTurbo\":false,\"health\":1000,\"modVanityPlate\":-1,\"modDial\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":-1043459709,\"modFrontBumper\":-1,\"dirtLevel\":0.0,\"modFrontWheels\":-1,\"modHydrolic\":-1,\"modLivery\":-1,\"modSmokeEnabled\":false,\"modSeats\":-1,\"modExhaust\":-1,\"plate\":\"FVA 162\",\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"windowTint\":-1,\"modXenon\":false,\"modEngine\":-1,\"modWindows\":-1,\"modHood\":-1,\"modSideSkirt\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"modArchCover\":-1}');
INSERT INTO `owned_boats` VALUES ('steam:110000104c989b7', '1', 'TTO 082', '{\"windowTint\":-1,\"modTransmission\":-1,\"modXenon\":false,\"modLivery\":-1,\"modShifterLeavers\":-1,\"neonColor\":[255,0,255],\"modVanityPlate\":-1,\"modFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":{\"3\":true,\"4\":false,\"5\":true,\"7\":false},\"model\":-688022864,\"pearlescentColor\":0,\"modSeats\":-1,\"modFrontBumper\":-1,\"modAirFilter\":-1,\"modRearBumper\":-1,\"modSpeakers\":-1,\"modGrille\":-1,\"wheelColor\":0,\"modStruts\":-1,\"wheels\":0,\"modRightFender\":-1,\"modTank\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modDoorSpeaker\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"plate\":\"TTO 082\",\"modSpoilers\":-1,\"color2\":1,\"modDial\":-1,\"modPlateHolder\":-1,\"modWindows\":-1,\"modAPlate\":-1,\"modTrunk\":-1,\"plateIndex\":0,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"modBackWheels\":-1,\"modTurbo\":false,\"modDashboard\":-1,\"modSideSkirt\":-1,\"dirtLevel\":0.0,\"modRoof\":-1,\"modSuspension\":-1,\"modEngine\":-1,\"color1\":3,\"neonEnabled\":[false,false,false,false],\"modAerials\":-1,\"modBrakes\":-1,\"health\":998,\"modHood\":-1,\"modArmor\":-1}');

-- ----------------------------
-- Table structure for `owned_properties`
-- ----------------------------
DROP TABLE IF EXISTS `owned_properties`;
CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of owned_properties
-- ----------------------------
INSERT INTO `owned_properties` VALUES ('34', 'MiltonDrive', '200000', '0', 'steam:11000010c2b4bd6');
INSERT INTO `owned_properties` VALUES ('35', 'Hangman AvenueLuxApartment7', '1315849', '0', 'steam:11000010c2709d9');
INSERT INTO `owned_properties` VALUES ('36', 'North Sheldon AvenueLuxApartment13', '1975282', '0', 'steam:11000010c2709d9');

-- ----------------------------
-- Table structure for `owned_vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `owned_vehicles`;
CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the car',
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of owned_vehicles
-- ----------------------------
INSERT INTO `owned_vehicles` VALUES ('steam:110000104c989b7', '1', 'BJZ 576', '{\"modHorns\":-1,\"plate\":\"BJZ 576\",\"modFrame\":-1,\"model\":1912215274,\"dirtLevel\":1.0,\"modDoorSpeaker\":-1,\"color2\":2,\"modHood\":-1,\"modStruts\":-1,\"modAPlate\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modSpoilers\":-1,\"modRoof\":-1,\"modXenon\":false,\"modHydrolic\":-1,\"modTrimB\":-1,\"pearlescentColor\":0,\"extras\":{\"11\":true,\"2\":true,\"12\":false,\"1\":true},\"windowTint\":-1,\"modGrille\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modFrontBumper\":-1,\"modTrimA\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"modTank\":-1,\"modArchCover\":-1,\"modBrakes\":-1,\"modTransmission\":-1,\"wheels\":0,\"modEngine\":-1,\"modDial\":-1,\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"modAirFilter\":-1,\"color1\":2,\"modDashboard\":-1,\"modEngineBlock\":-1,\"plateIndex\":4,\"modLivery\":1,\"modExhaust\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"health\":1000,\"modSeats\":-1,\"modPlateHolder\":-1,\"modTrunk\":-1,\"modWindows\":-1,\"modTurbo\":false,\"modAerials\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"neonColor\":[255,0,255]}', 'car', 'police', '1');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'BPJ 377', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"BPJ 377\",\"neonEnabled\":[false,false,false,false],\"modLivery\":0,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":{\"11\":false},\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":2,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":-1627000575,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":0,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":2,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":0,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":2.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '1');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'DXR 863', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"DXR 863\",\"neonEnabled\":[false,false,false,false],\"modLivery\":0,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":{\"1\":true},\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":21,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":21,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":-1047278874,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":21,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":21,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":3,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":13.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '0');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'MGP 142', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"MGP 142\",\"neonEnabled\":[false,false,false,false],\"modLivery\":1,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":{\"11\":false,\"2\":true,\"1\":true,\"12\":true},\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":2,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":1912215274,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":0,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":2,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":0,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":0.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '0');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'MZV 275', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"MZV 275\",\"neonEnabled\":[false,false,false,false],\"modLivery\":0,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":[],\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":2,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":908205667,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":0,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":2,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":1,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":0.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '1');
INSERT INTO `owned_vehicles` VALUES ('steam:11000010c2b4bd6', '1', 'OMG 984', '{\"modEngine\":-1,\"dirtLevel\":7.14102268219,\"modRearBumper\":-1,\"modTrimB\":-1,\"pearlescentColor\":4,\"modLivery\":-1,\"modFrontWheels\":-1,\"health\":1000,\"modAirFilter\":-1,\"modSpoilers\":-1,\"modDoorSpeaker\":-1,\"modDashboard\":-1,\"modSuspension\":-1,\"modRightFender\":-1,\"modShifterLeavers\":-1,\"modTank\":-1,\"modWindows\":-1,\"neonEnabled\":[false,false,false,false],\"modRoof\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"plate\":\"OMG 984\",\"modStruts\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"extras\":{\"12\":false,\"10\":false,\"11\":true},\"modFender\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"modPlateHolder\":-1,\"modArmor\":-1,\"wheels\":7,\"modTrimA\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modFrame\":-1,\"modDial\":-1,\"modSeats\":-1,\"plateIndex\":0,\"modSideSkirt\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modExhaust\":-1,\"color1\":2,\"windowTint\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modTransmission\":-1,\"modBrakes\":-1,\"modSteeringWheel\":-1,\"modHorns\":-1,\"modHydrolic\":-1,\"modAerials\":-1,\"color2\":0,\"modGrille\":-1,\"model\":767087018,\"wheelColor\":156,\"modFrontBumper\":-1,\"modBackWheels\":-1}', 'car', null, '0');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'TNO 745', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"TNO 745\",\"neonEnabled\":[false,false,false,false],\"modLivery\":-1,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":{\"11\":false,\"12\":false},\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":-1973172295,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":7,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":1,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":1,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":3.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '0');
INSERT INTO `owned_vehicles` VALUES ('steam:11000010c2b4bd6', '1', 'VBY 747', '{\"modAirFilter\":6,\"plateIndex\":0,\"extras\":[],\"modWindows\":0,\"neonColor\":[0,255,0],\"modDial\":0,\"modFrontBumper\":10,\"modSpoilers\":14,\"modExhaust\":2,\"color1\":64,\"modTrimA\":0,\"windowTint\":1,\"modGrille\":2,\"modTransmission\":-1,\"modShifterLeavers\":-1,\"modPlateHolder\":-1,\"modTurbo\":false,\"modBrakes\":2,\"modRightFender\":-1,\"modDoorSpeaker\":2,\"modVanityPlate\":-1,\"modSteeringWheel\":12,\"plate\":\"VBY 747\",\"modSeats\":2,\"modRearBumper\":1,\"wheels\":0,\"modTrimB\":1,\"modXenon\":1,\"tyreSmokeColor\":[255,255,255],\"modAerials\":1,\"modFrame\":4,\"health\":987,\"neonEnabled\":[1,1,1,1],\"modSpeakers\":-1,\"modSmokeEnabled\":1,\"modTank\":-1,\"modHood\":7,\"wheelColor\":158,\"modLivery\":3,\"modAPlate\":-1,\"modTrunk\":-1,\"modHorns\":-1,\"modBackWheels\":-1,\"modArchCover\":1,\"model\":-295689028,\"pearlescentColor\":91,\"dirtLevel\":7.2329149246216,\"modSideSkirt\":3,\"modFrontWheels\":-1,\"modArmor\":4,\"modSuspension\":3,\"color2\":117,\"modEngineBlock\":1,\"modStruts\":7,\"modRoof\":0,\"modOrnaments\":25,\"modHydrolic\":-1,\"modEngine\":-1,\"modFender\":2,\"modDashboard\":3}', 'car', null, '0');
INSERT INTO `owned_vehicles` VALUES ('steam:110000117fd8bdf', '1', 'VCX 716', '{\"modSideSkirt\":-1,\"modTank\":-1,\"plate\":\"VCX 716\",\"neonEnabled\":[false,false,false,false],\"modLivery\":2,\"modAPlate\":-1,\"modSpoilers\":-1,\"extras\":{\"1\":false},\"modSuspension\":-1,\"modRoof\":-1,\"modDial\":-1,\"windowTint\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"color1\":2,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modBackWheels\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modFender\":-1,\"modXenon\":false,\"tyreSmokeColor\":[255,255,255],\"model\":-1051914684,\"modTrimB\":-1,\"modTurbo\":false,\"pearlescentColor\":0,\"modHood\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"color2\":2,\"modEngine\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"plateIndex\":4,\"modHorns\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"wheels\":3,\"modAerials\":-1,\"modWindows\":-1,\"modStruts\":-1,\"dirtLevel\":12.0,\"modGrille\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modOrnaments\":-1}', 'car', 'police', '1');

-- ----------------------------
-- Table structure for `phone_app_chat`
-- ----------------------------
DROP TABLE IF EXISTS `phone_app_chat`;
CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_app_chat
-- ----------------------------
INSERT INTO `phone_app_chat` VALUES ('10', 'aaaa', 'ff', '2020-05-23 15:04:10');
INSERT INTO `phone_app_chat` VALUES ('11', 'vagos', 'V - G', '2020-05-23 15:04:10');
INSERT INTO `phone_app_chat` VALUES ('12', 'a', 'test', '2020-05-23 15:04:12');
INSERT INTO `phone_app_chat` VALUES ('13', 'vagos', 'Test', '2020-05-23 15:04:20');
INSERT INTO `phone_app_chat` VALUES ('14', 'aaaa', 'test', '2020-05-23 15:04:34');
INSERT INTO `phone_app_chat` VALUES ('15', 'vagos', 'test', '2020-05-23 15:04:48');

-- ----------------------------
-- Table structure for `phone_calls`
-- ----------------------------
DROP TABLE IF EXISTS `phone_calls`;
CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1178 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_calls
-- ----------------------------
INSERT INTO `phone_calls` VALUES ('1148', '555-2463', '555-1842', '0', '2020-05-23 12:11:36', '1');
INSERT INTO `phone_calls` VALUES ('1149', '555-1842', '555-2463', '1', '2020-05-23 12:11:36', '1');
INSERT INTO `phone_calls` VALUES ('1150', '555-2463', '555-1842', '0', '2020-05-23 13:11:27', '1');
INSERT INTO `phone_calls` VALUES ('1151', '555-1842', '555-2463', '1', '2020-05-23 13:11:27', '1');
INSERT INTO `phone_calls` VALUES ('1152', '555-1842', '555-2463', '1', '2020-05-23 13:13:06', '1');
INSERT INTO `phone_calls` VALUES ('1153', '555-2463', '555-1842', '0', '2020-05-23 13:13:06', '1');
INSERT INTO `phone_calls` VALUES ('1154', '555-2463', '555-1842', '0', '2020-05-23 14:37:20', '1');
INSERT INTO `phone_calls` VALUES ('1155', '555-1842', '555-2463', '1', '2020-05-23 14:37:20', '1');
INSERT INTO `phone_calls` VALUES ('1156', '555-2463', '555-1842', '0', '2020-05-23 14:37:45', '1');
INSERT INTO `phone_calls` VALUES ('1157', '555-1842', '555-2463', '1', '2020-05-23 14:37:45', '1');
INSERT INTO `phone_calls` VALUES ('1158', '555-1842', '555-2463', '1', '2020-05-23 14:53:37', '0');
INSERT INTO `phone_calls` VALUES ('1159', '555-2463', '555-1842', '0', '2020-05-23 14:53:37', '0');
INSERT INTO `phone_calls` VALUES ('1160', '555-2463', '555-1842', '0', '2020-05-23 14:59:28', '1');
INSERT INTO `phone_calls` VALUES ('1161', '555-1842', '555-2463', '1', '2020-05-23 14:59:28', '1');
INSERT INTO `phone_calls` VALUES ('1162', '555-1842', '555-2463', '0', '2020-05-23 15:00:58', '0');
INSERT INTO `phone_calls` VALUES ('1163', '555-2463', '555-1842', '1', '2020-05-23 15:00:58', '0');
INSERT INTO `phone_calls` VALUES ('1164', '555-1842', '555-2072', '1', '2020-05-25 15:22:01', '1');
INSERT INTO `phone_calls` VALUES ('1165', '555-2072', '555-1842', '0', '2020-05-25 15:22:01', '1');
INSERT INTO `phone_calls` VALUES ('1166', '555-2072', '555-1842', '1', '2020-05-25 15:24:05', '0');
INSERT INTO `phone_calls` VALUES ('1167', '555-1842', '555-2072', '0', '2020-05-25 15:24:05', '0');
INSERT INTO `phone_calls` VALUES ('1168', '555-1842', '555-2072', '0', '2020-05-25 15:24:12', '1');
INSERT INTO `phone_calls` VALUES ('1169', '555-2072', '555-1842', '1', '2020-05-25 15:24:12', '1');
INSERT INTO `phone_calls` VALUES ('1170', '555-1842', '555-2072', '1', '2020-05-25 15:25:28', '0');
INSERT INTO `phone_calls` VALUES ('1171', '555-2072', '555-1842', '0', '2020-05-25 15:25:28', '0');
INSERT INTO `phone_calls` VALUES ('1172', '555-2072', '555-1842', '1', '2020-05-25 15:25:51', '1');
INSERT INTO `phone_calls` VALUES ('1173', '555-1842', '555-2072', '0', '2020-05-25 15:25:51', '1');
INSERT INTO `phone_calls` VALUES ('1174', '555-2072', '555-1842', '1', '2020-05-25 15:25:52', '1');
INSERT INTO `phone_calls` VALUES ('1175', '555-1842', '555-2072', '0', '2020-05-25 15:25:52', '1');
INSERT INTO `phone_calls` VALUES ('1176', '555-1842', '555-2072', '0', '2020-05-25 15:25:52', '1');
INSERT INTO `phone_calls` VALUES ('1177', '555-2072', '555-1842', '1', '2020-05-25 15:25:52', '1');

-- ----------------------------
-- Table structure for `phone_messages`
-- ----------------------------
DROP TABLE IF EXISTS `phone_messages`;
CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2214 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_messages
-- ----------------------------
INSERT INTO `phone_messages` VALUES ('2185', 'police', '555-2463', 'Bande de puceaux venez me chercher !', '2020-05-23 12:11:09', '1', '1');
INSERT INTO `phone_messages` VALUES ('2186', 'police', '555-1842', 'De #555-2463 : Bande de puceaux venez me chercher !', '2020-05-23 12:11:09', '1', '0');
INSERT INTO `phone_messages` VALUES ('2187', '5552463', '555-1842', 'test', '2020-05-23 12:12:22', '1', '1');
INSERT INTO `phone_messages` VALUES ('2188', '5552463', '555-1842', 'tu recois?', '2020-05-23 12:12:36', '1', '1');
INSERT INTO `phone_messages` VALUES ('2189', 'ambulance', '555-1842', 'De #555-2463 : Medical attention required: unconscious citizen! 979.86071777344, -142.31703186036', '2020-05-23 12:32:58', '1', '0');
INSERT INTO `phone_messages` VALUES ('2190', '555-1842', '555-2463', 'test', '2020-05-23 13:12:48', '1', '0');
INSERT INTO `phone_messages` VALUES ('2191', '555-2463', '555-1842', 'test', '2020-05-23 13:12:49', '1', '1');
INSERT INTO `phone_messages` VALUES ('2192', '555-1842', '555-2463', 'test', '2020-05-23 13:12:50', '1', '0');
INSERT INTO `phone_messages` VALUES ('2193', '555-2463', '555-1842', 'test', '2020-05-23 13:12:50', '1', '1');
INSERT INTO `phone_messages` VALUES ('2194', '555-1842', '555-2463', 'test', '2020-05-23 13:12:51', '1', '0');
INSERT INTO `phone_messages` VALUES ('2195', '555-2463', '555-1842', 'test', '2020-05-23 13:12:51', '1', '1');
INSERT INTO `phone_messages` VALUES ('2196', '555-1842', '555-2463', 'GPS: -851.16625976563, 299.73950195313', '2020-05-23 13:12:57', '1', '0');
INSERT INTO `phone_messages` VALUES ('2197', '555-2463', '555-1842', 'GPS: -851.16625976563, 299.73950195313', '2020-05-23 13:12:57', '1', '1');
INSERT INTO `phone_messages` VALUES ('2198', '555-2463', '555-1842', 'puceau', '2020-05-23 13:13:04', '1', '0');
INSERT INTO `phone_messages` VALUES ('2199', '555-1842', '555-2463', 'puceau', '2020-05-23 13:13:04', '1', '1');
INSERT INTO `phone_messages` VALUES ('2200', '555-1842', '555-2463', 'yo', '2020-05-23 15:00:32', '1', '0');
INSERT INTO `phone_messages` VALUES ('2201', '555-2463', '555-1842', 'yo', '2020-05-23 15:00:32', '1', '1');
INSERT INTO `phone_messages` VALUES ('2202', '555-2463', '555-1842', '123', '2020-05-23 15:00:42', '1', '0');
INSERT INTO `phone_messages` VALUES ('2203', '555-1842', '555-2463', '123', '2020-05-23 15:00:43', '1', '1');
INSERT INTO `phone_messages` VALUES ('2204', '5552463', '555-1842', '5552463', '2020-05-23 15:34:09', '1', '1');
INSERT INTO `phone_messages` VALUES ('2205', '5552463', '555-1842', '555-2463', '2020-05-23 15:34:52', '1', '1');
INSERT INTO `phone_messages` VALUES ('2206', '555-1842', '555-2463', '555-2448', '2020-05-23 15:35:16', '1', '0');
INSERT INTO `phone_messages` VALUES ('2207', '555-2463', '555-1842', '555-2448', '2020-05-23 15:35:17', '1', '1');
INSERT INTO `phone_messages` VALUES ('2208', '555-1842', '555-2072', 'hey', '2020-05-25 15:23:36', '1', '0');
INSERT INTO `phone_messages` VALUES ('2209', '555-2072', '555-1842', 'hey', '2020-05-25 15:23:36', '1', '1');
INSERT INTO `phone_messages` VALUES ('2210', '555-2072', '555-1842', 'GPS: -342.59887695313, -1149.6141357422', '2020-05-25 15:23:42', '1', '0');
INSERT INTO `phone_messages` VALUES ('2211', '555-1842', '555-2072', 'GPS: -342.59887695313, -1149.6141357422', '2020-05-25 15:23:42', '1', '1');
INSERT INTO `phone_messages` VALUES ('2212', '555-2072', '555-1842', 'dsfsd', '2020-05-25 15:26:03', '0', '0');
INSERT INTO `phone_messages` VALUES ('2213', '555-1842', '555-2072', 'dsfsd', '2020-05-25 15:26:03', '1', '1');

-- ----------------------------
-- Table structure for `phone_users_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `phone_users_contacts`;
CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_users_contacts
-- ----------------------------
INSERT INTO `phone_users_contacts` VALUES ('100', 'steam:11000010c2709d9', '555-2463', 'New contact');
INSERT INTO `phone_users_contacts` VALUES ('101', 'steam:11000010c2709d9', '555-2072', 'fzafaz');

-- ----------------------------
-- Table structure for `properties`
-- ----------------------------
DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of properties
-- ----------------------------
INSERT INTO `properties` VALUES ('1', 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', null, '1', '1', '0', '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', '1500000');
INSERT INTO `properties` VALUES ('2', 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', null, '1', '1', '0', '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', '1500000');
INSERT INTO `properties` VALUES ('3', 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":38.9667,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', null, '1', '1', '0', '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', '1700000');
INSERT INTO `properties` VALUES ('4', 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', null, '1', '1', '0', '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', '1500000');
INSERT INTO `properties` VALUES ('5', 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', null, '1', '1', '0', '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', '1500000');
INSERT INTO `properties` VALUES ('6', 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', null, '1', '1', '0', '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', '1500000');
INSERT INTO `properties` VALUES ('7', 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":29.3989,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', null, '1', '1', '0', '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '562500');
INSERT INTO `properties` VALUES ('8', 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', null, '1', '1', '0', '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', '1500000');
INSERT INTO `properties` VALUES ('9', 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', null, '1', '1', '0', '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', '1500000');
INSERT INTO `properties` VALUES ('10', 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', null, '1', '1', '0', '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', '1500000');
INSERT INTO `properties` VALUES ('11', 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', null, '1', '1', '0', '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', '1700000');
INSERT INTO `properties` VALUES ('12', 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":85.6936}', null, null, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', null, '0', '0', '1', null, '0');
INSERT INTO `properties` VALUES ('13', 'Modern1Apartment', 'Appartement Moderne 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('14', 'Modern2Apartment', 'Appartement Moderne 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('15', 'Modern3Apartment', 'Appartement Moderne 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', '1300000');
INSERT INTO `properties` VALUES ('16', 'Mody1Apartment', 'Appartement Mode 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('17', 'Mody2Apartment', 'Appartement Mode 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('18', 'Mody3Apartment', 'Appartement Mode 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', '1300000');
INSERT INTO `properties` VALUES ('19', 'Vibrant1Apartment', 'Appartement Vibrant 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('20', 'Vibrant2Apartment', 'Appartement Vibrant 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('21', 'Vibrant3Apartment', 'Appartement Vibrant 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', '1300000');
INSERT INTO `properties` VALUES ('22', 'Sharp1Apartment', 'Appartement Persan 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('23', 'Sharp2Apartment', 'Appartement Persan 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('24', 'Sharp3Apartment', 'Appartement Persan 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', '1300000');
INSERT INTO `properties` VALUES ('25', 'Monochrome1Apartment', 'Appartement Monochrome 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('26', 'Monochrome2Apartment', 'Appartement Monochrome 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('27', 'Monochrome3Apartment', 'Appartement Monochrome 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', '1300000');
INSERT INTO `properties` VALUES ('28', 'Seductive1Apartment', 'Appartement Séduisant 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('29', 'Seductive2Apartment', 'Appartement Séduisant 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('30', 'Seductive3Apartment', 'Appartement Séduisant 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', '1300000');
INSERT INTO `properties` VALUES ('31', 'Regal1Apartment', 'Appartement Régal 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('32', 'Regal2Apartment', 'Appartement Régal 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('33', 'Regal3Apartment', 'Appartement Régal 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', '1300000');
INSERT INTO `properties` VALUES ('34', 'Aqua1Apartment', 'Appartement Aqua 1', null, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', null, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', '1300000');
INSERT INTO `properties` VALUES ('35', 'Aqua2Apartment', 'Appartement Aqua 2', null, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', null, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', '1300000');
INSERT INTO `properties` VALUES ('36', 'Aqua3Apartment', 'Appartement Aqua 3', null, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', null, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', '0', '1', '0', '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', '1300000');
INSERT INTO `properties` VALUES ('37', 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":37.958}', null, null, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', null, '0', '0', '1', null, '0');
INSERT INTO `properties` VALUES ('38', 'IntegrityWay28', '4 Integrity Way - Apt 28', null, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', null, '[]', 'IntegrityWay', '0', '1', '0', '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', '1700000');
INSERT INTO `properties` VALUES ('39', 'IntegrityWay30', '4 Integrity Way - Apt 30', null, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', null, '[]', 'IntegrityWay', '0', '1', '0', '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', '1700000');
INSERT INTO `properties` VALUES ('40', 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":34.7444}', null, null, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', null, '0', '0', '1', null, '0');
INSERT INTO `properties` VALUES ('41', 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', null, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', null, '[]', 'DellPerroHeights', '0', '1', '0', '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', '1700000');
INSERT INTO `properties` VALUES ('42', 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', null, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', null, '[]', 'DellPerroHeights', '0', '1', '0', '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', '1700000');
INSERT INTO `properties` VALUES ('107', 'Roy Lowenstein BoulevardLowApartment19', 'Roy Lowenstein Boulevard', '{\"x\":249.03553771973,\"y\":-1933.7664794922,\"z\":24.349781036377}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":249.03553771973,\"y\":-1933.7664794922,\"z\":24.349781036377}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '99505');
INSERT INTO `properties` VALUES ('108', 'Roy Lowenstein BoulevardLowApartment20', 'Roy Lowenstein Boulevard', '{\"x\":257.51184082031,\"y\":-1927.5687255859,\"z\":25.444780349731}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":257.51184082031,\"y\":-1927.5687255859,\"z\":25.444780349731}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '90960');
INSERT INTO `properties` VALUES ('109', 'Roy Lowenstein BoulevardLowApartment21', 'Roy Lowenstein Boulevard', '{\"x\":268.31359863281,\"y\":-1915.6882324219,\"z\":25.84966468811}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":268.31359863281,\"y\":-1915.6882324219,\"z\":25.84966468811}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '95039');
INSERT INTO `properties` VALUES ('110', 'Roy Lowenstein BoulevardLowApartment22', 'Roy Lowenstein Boulevard', '{\"x\":281.27899169922,\"y\":-1897.9656982422,\"z\":26.885105133057}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":281.27899169922,\"y\":-1897.9656982422,\"z\":26.885105133057}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89695');
INSERT INTO `properties` VALUES ('117', 'Roy Lowenstein BoulevardLowApartment29', 'Roy Lowenstein Boulevard', '{\"x\":318.58779907227,\"y\":-1852.9735107422,\"z\":27.120948791504}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":318.58779907227,\"y\":-1852.9735107422,\"z\":27.120948791504}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '91412');
INSERT INTO `properties` VALUES ('118', 'Roy Lowenstein BoulevardLowApartment30', 'Roy Lowenstein Boulevard', '{\"x\":327.20092773438,\"y\":-1843.7722167969,\"z\":27.302562713623}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":327.20092773438,\"y\":-1843.7722167969,\"z\":27.302562713623}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '96259');
INSERT INTO `properties` VALUES ('119', 'Roy Lowenstein BoulevardLowApartment31', 'Roy Lowenstein Boulevard', '{\"x\":340.52871704102,\"y\":-1827.5593261719,\"z\":27.947919845581}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":340.52871704102,\"y\":-1827.5593261719,\"z\":27.947919845581}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '87143');
INSERT INTO `properties` VALUES ('120', 'Roy Lowenstein BoulevardLowApartment32', 'Roy Lowenstein Boulevard', '{\"x\":349.14263916016,\"y\":-1820.0362548828,\"z\":28.894121170044}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":349.14263916016,\"y\":-1820.0362548828,\"z\":28.894121170044}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '96135');
INSERT INTO `properties` VALUES ('127', 'Roy Lowenstein BoulevardLowApartment1', 'Roy Lowenstein Boulevard', '{\"x\":403.60928344727,\"y\":-1749.7593994141,\"z\":29.342887878418}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":403.60928344727,\"y\":-1749.7593994141,\"z\":29.342887878418}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '88149');
INSERT INTO `properties` VALUES ('128', 'Roy Lowenstein BoulevardLowApartment2', 'Roy Lowenstein Boulevard', '{\"x\":418.10855102539,\"y\":-1735.5811767578,\"z\":29.607698440552}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":418.10855102539,\"y\":-1735.5811767578,\"z\":29.607698440552}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '85099');
INSERT INTO `properties` VALUES ('129', 'Roy Lowenstein BoulevardLowApartment3', 'Roy Lowenstein Boulevard', '{\"x\":428.95733642578,\"y\":-1723.9631347656,\"z\":29.229053497314}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":428.95733642578,\"y\":-1723.9631347656,\"z\":29.229053497314}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '85276');
INSERT INTO `properties` VALUES ('130', 'Roy Lowenstein BoulevardLowApartment4', 'Roy Lowenstein Boulevard', '{\"x\":441.82684326172,\"y\":-1706.0318603516,\"z\":29.34734916687}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":441.82684326172,\"y\":-1706.0318603516,\"z\":29.34734916687}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '97500');
INSERT INTO `properties` VALUES ('140', 'Roy Lowenstein BoulevardLowApartment14', 'Roy Lowenstein Boulevard', '{\"x\":290.98867797852,\"y\":-1793.9849853516,\"z\":27.701089859009}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":290.98867797852,\"y\":-1793.9849853516,\"z\":27.701089859009}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '87337');
INSERT INTO `properties` VALUES ('141', 'Roy Lowenstein BoulevardLowApartment15', 'Roy Lowenstein Boulevard', '{\"x\":299.70458984375,\"y\":-1784.9112548828,\"z\":28.438661575317}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":299.70458984375,\"y\":-1784.9112548828,\"z\":28.438661575317}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '86127');
INSERT INTO `properties` VALUES ('142', 'Roy Lowenstein BoulevardLowApartment16', 'Roy Lowenstein Boulevard', '{\"x\":306.1071472168,\"y\":-1776.8776855469,\"z\":28.66404914856}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":306.1071472168,\"y\":-1776.8776855469,\"z\":28.66404914856}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '104334');
INSERT INTO `properties` VALUES ('143', 'Roy Lowenstein BoulevardLowApartment17', 'Roy Lowenstein Boulevard', '{\"x\":322.22161865234,\"y\":-1759.3278808594,\"z\":29.315031051636}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":322.22161865234,\"y\":-1759.3278808594,\"z\":29.315031051636}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '98168');
INSERT INTO `properties` VALUES ('144', 'Roy Lowenstein BoulevardLowApartment18', 'Roy Lowenstein Boulevard', '{\"x\":332.83636474609,\"y\":-1742.3994140625,\"z\":29.730527877808}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":332.83636474609,\"y\":-1742.3994140625,\"z\":29.730527877808}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '92396');
INSERT INTO `properties` VALUES ('145', 'Forum DriveLowApartment1', 'Forum Drive', '{\"x\":-46.506820678711,\"y\":-1446.1826171875,\"z\":32.429595947266}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":-46.506820678711,\"y\":-1446.1826171875,\"z\":32.429595947266}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '102297');
INSERT INTO `properties` VALUES ('146', 'Forum DriveLowApartment2', 'Forum Drive', '{\"x\":-35.16219329834,\"y\":-1446.5322265625,\"z\":31.493417739868}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":-35.16219329834,\"y\":-1446.5322265625,\"z\":31.493417739868}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '87271');
INSERT INTO `properties` VALUES ('147', 'Forum DriveLowApartment3', 'Forum Drive', '{\"x\":-1.6476721763611,\"y\":-1444.0211181641,\"z\":30.552778244019}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":-1.6476721763611,\"y\":-1444.0211181641,\"z\":30.552778244019}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '101835');
INSERT INTO `properties` VALUES ('148', 'Forum DriveLowApartment4', 'Forum Drive', '{\"x\":15.82791519165,\"y\":-1445.5560302734,\"z\":30.541540145874}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":15.82791519165,\"y\":-1445.5560302734,\"z\":30.541540145874}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89921');
INSERT INTO `properties` VALUES ('149', 'Brouge AvenueLowApartment1', 'Brouge Avenue', '{\"x\":247.88203430176,\"y\":-1729.4180908203,\"z\":29.356981277466}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":247.88203430176,\"y\":-1729.4180908203,\"z\":29.356981277466}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '88897');
INSERT INTO `properties` VALUES ('150', 'Brouge AvenueLowApartment2', 'Brouge Avenue', '{\"x\":217.72830200195,\"y\":-1716.7276611328,\"z\":29.291751861572}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":217.72830200195,\"y\":-1716.7276611328,\"z\":29.291751861572}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '98231');
INSERT INTO `properties` VALUES ('151', 'Brouge AvenueLowApartment3', 'Brouge Avenue', '{\"x\":224.15005493164,\"y\":-1704.2561035156,\"z\":29.303884506226}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":224.15005493164,\"y\":-1704.2561035156,\"z\":29.303884506226}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89338');
INSERT INTO `properties` VALUES ('152', 'Brouge AvenueLowApartment4', 'Brouge Avenue', '{\"x\":256.3913269043,\"y\":-1723.4542236328,\"z\":29.65412902832}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":256.3913269043,\"y\":-1723.4542236328,\"z\":29.65412902832}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '90526');
INSERT INTO `properties` VALUES ('153', 'Brouge AvenueLowApartment5', 'Brouge Avenue', '{\"x\":242.43644714355,\"y\":-1688.8393554688,\"z\":29.282918930054}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":242.43644714355,\"y\":-1688.8393554688,\"z\":29.282918930054}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '85424');
INSERT INTO `properties` VALUES ('154', 'Brouge AvenueLowApartment6', 'Brouge Avenue', '{\"x\":267.30023193359,\"y\":-1711.484375,\"z\":29.376234054565}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":267.30023193359,\"y\":-1711.484375,\"z\":29.376234054565}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '91327');
INSERT INTO `properties` VALUES ('155', 'MacDonald StreetLowApartment7', 'MacDonald Street', '{\"x\":252.58195495605,\"y\":-1671.6158447266,\"z\":29.66316986084}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":252.58195495605,\"y\":-1671.6158447266,\"z\":29.66316986084}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '94259');
INSERT INTO `properties` VALUES ('156', 'MacDonald StreetLowApartment8', 'MacDonald Street', '{\"x\":280.57510375977,\"y\":-1693.5810546875,\"z\":29.266786575317}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":280.57510375977,\"y\":-1693.5810546875,\"z\":29.266786575317}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '86193');
INSERT INTO `properties` VALUES ('157', 'West Mirror DriveLowApartment9', 'West Mirror Drive', '{\"x\":997.30786132813,\"y\":-729.08630371094,\"z\":57.815551757813}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":997.30786132813,\"y\":-729.08630371094,\"z\":57.815551757813}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '104042');
INSERT INTO `properties` VALUES ('158', 'West Mirror DriveLowApartment10', 'West Mirror Drive', '{\"x\":980.76599121094,\"y\":-714.68322753906,\"z\":57.766952514648}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":980.76599121094,\"y\":-714.68322753906,\"z\":57.766952514648}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '99824');
INSERT INTO `properties` VALUES ('159', 'West Mirror DriveLowApartment11', 'West Mirror Drive', '{\"x\":970.87756347656,\"y\":-700.78363037109,\"z\":58.481956481934}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":970.87756347656,\"y\":-700.78363037109,\"z\":58.481956481934}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89231');
INSERT INTO `properties` VALUES ('160', 'West Mirror DriveLuxApartment1', 'West Mirror Drive', '{\"x\":960.69580078125,\"y\":-669.50451660156,\"z\":58.449771881104}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":960.69580078125,\"y\":-669.50451660156,\"z\":58.449771881104}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', '1396017');
INSERT INTO `properties` VALUES ('161', 'West Mirror DriveLowApartment2', 'West Mirror Drive', '{\"x\":943.88000488281,\"y\":-653.74456787109,\"z\":58.428718566895}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":943.88000488281,\"y\":-653.74456787109,\"z\":58.428718566895}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '94220');
INSERT INTO `properties` VALUES ('162', 'West Mirror DriveLowApartment3', 'West Mirror Drive', '{\"x\":930.80364990234,\"y\":-637.83612060547,\"z\":57.859703063965}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":930.80364990234,\"y\":-637.83612060547,\"z\":57.859703063965}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '91674');
INSERT INTO `properties` VALUES ('163', 'West Mirror DriveLowApartment4', 'West Mirror Drive', '{\"x\":905.05169677734,\"y\":-614.53497314453,\"z\":58.048969268799}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":905.05169677734,\"y\":-614.53497314453,\"z\":58.048969268799}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '92306');
INSERT INTO `properties` VALUES ('164', 'West Mirror DriveLowApartment5', 'West Mirror Drive', '{\"x\":888.05657958984,\"y\":-606.83062744141,\"z\":58.220500946045}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":888.05657958984,\"y\":-606.83062744141,\"z\":58.220500946045}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '88473');
INSERT INTO `properties` VALUES ('165', 'West Mirror DriveLowApartment6', 'West Mirror Drive', '{\"x\":861.68499755859,\"y\":-582.40612792969,\"z\":58.156494140625}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":861.68499755859,\"y\":-582.40612792969,\"z\":58.156494140625}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89541');
INSERT INTO `properties` VALUES ('166', 'West Mirror DriveLowApartment7', 'West Mirror Drive', '{\"x\":844.93389892578,\"y\":-565.01568603516,\"z\":57.707931518555}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":844.93389892578,\"y\":-565.01568603516,\"z\":57.707931518555}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '93395');
INSERT INTO `properties` VALUES ('167', 'West Mirror DriveLowApartment8', 'West Mirror Drive', '{\"x\":851.12780761719,\"y\":-532.33752441406,\"z\":57.925201416016}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":851.12780761719,\"y\":-532.33752441406,\"z\":57.925201416016}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '93093');
INSERT INTO `properties` VALUES ('168', 'West Mirror DriveLowApartment9', 'West Mirror Drive', '{\"x\":863.12628173828,\"y\":-510.39077758789,\"z\":57.328956604004}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":863.12628173828,\"y\":-510.39077758789,\"z\":57.328956604004}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '87699');
INSERT INTO `properties` VALUES ('169', 'West Mirror DriveLowApartment10', 'West Mirror Drive', '{\"x\":879.65759277344,\"y\":-498.93240356445,\"z\":57.875160217285}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":879.65759277344,\"y\":-498.93240356445,\"z\":57.875160217285}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '95742');
INSERT INTO `properties` VALUES ('170', 'West Mirror DriveLuxApartment11', 'West Mirror Drive', '{\"x\":906.94549560547,\"y\":-490.7135925293,\"z\":59.436214447021}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":906.94549560547,\"y\":-490.7135925293,\"z\":59.436214447021}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', '1095404');
INSERT INTO `properties` VALUES ('171', 'West Mirror DriveLowApartment12', 'West Mirror Drive', '{\"x\":922.76696777344,\"y\":-480.61740112305,\"z\":60.699756622314}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":922.76696777344,\"y\":-480.61740112305,\"z\":60.699756622314}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '103794');
INSERT INTO `properties` VALUES ('172', 'West Mirror DriveLowApartment13', 'West Mirror Drive', '{\"x\":942.56396484375,\"y\":-464.65219116211,\"z\":61.252323150635}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":942.56396484375,\"y\":-464.65219116211,\"z\":61.252323150635}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '92715');
INSERT INTO `properties` VALUES ('173', 'West Mirror DriveLowApartment14', 'West Mirror Drive', '{\"x\":968.5234375,\"y\":-453.16781616211,\"z\":62.402824401855}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":968.5234375,\"y\":-453.16781616211,\"z\":62.402824401855}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '94264');
INSERT INTO `properties` VALUES ('174', 'West Mirror DriveLowApartment15', 'West Mirror Drive', '{\"x\":989.00720214844,\"y\":-434.70355224609,\"z\":63.737648010254}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":989.00720214844,\"y\":-434.70355224609,\"z\":63.737648010254}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '88172');
INSERT INTO `properties` VALUES ('175', 'West Mirror DriveLowApartment16', 'West Mirror Drive', '{\"x\":1011.7874145508,\"y\":-422.57360839844,\"z\":64.952728271484}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1011.7874145508,\"y\":-422.57360839844,\"z\":64.952728271484}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '102976');
INSERT INTO `properties` VALUES ('176', 'West Mirror DriveLowApartment17', 'West Mirror Drive', '{\"x\":1030.8032226563,\"y\":-410.89944458008,\"z\":65.949295043945}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1030.8032226563,\"y\":-410.89944458008,\"z\":65.949295043945}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '97824');
INSERT INTO `properties` VALUES ('177', 'Bridge StreetLowApartment18', 'Bridge Street', '{\"x\":1062.7053222656,\"y\":-380.07598876953,\"z\":67.848403930664}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1062.7053222656,\"y\":-380.07598876953,\"z\":67.848403930664}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '95219');
INSERT INTO `properties` VALUES ('178', 'West Mirror DriveLowApartment19', 'West Mirror Drive', '{\"x\":1112.9084472656,\"y\":-390.88705444336,\"z\":68.733512878418}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1112.9084472656,\"y\":-390.88705444336,\"z\":68.733512878418}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '86631');
INSERT INTO `properties` VALUES ('179', 'Bridge StreetLuxApartment20', 'Bridge Street', '{\"x\":1100.1086425781,\"y\":-411.32711791992,\"z\":67.555236816406}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":1100.1086425781,\"y\":-411.32711791992,\"z\":67.555236816406}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', '931339');
INSERT INTO `properties` VALUES ('180', 'Bridge StreetLowApartment21', 'Bridge Street', '{\"x\":1099.7180175781,\"y\":-436.58966064453,\"z\":67.396293640137}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1099.7180175781,\"y\":-436.58966064453,\"z\":67.396293640137}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '92679');
INSERT INTO `properties` VALUES ('181', 'Bridge StreetLowApartment22', 'Bridge Street', '{\"x\":1097.9696044922,\"y\":-465.15158081055,\"z\":67.319412231445}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1097.9696044922,\"y\":-465.15158081055,\"z\":67.319412231445}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '103768');
INSERT INTO `properties` VALUES ('182', 'Bridge StreetLowApartment23', 'Bridge Street', '{\"x\":1089.4764404297,\"y\":-484.38037109375,\"z\":65.660247802734}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1089.4764404297,\"y\":-484.38037109375,\"z\":65.660247802734}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '100093');
INSERT INTO `properties` VALUES ('183', 'Bridge StreetLowApartment24', 'Bridge Street', '{\"x\":1056.51171875,\"y\":-448.26446533203,\"z\":66.257484436035}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1056.51171875,\"y\":-448.26446533203,\"z\":66.257484436035}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '86049');
INSERT INTO `properties` VALUES ('184', 'Bridge StreetLowApartment25', 'Bridge Street', '{\"x\":1052.5235595703,\"y\":-470.77896118164,\"z\":63.898918151855}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1052.5235595703,\"y\":-470.77896118164,\"z\":63.898918151855}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '101768');
INSERT INTO `properties` VALUES ('185', 'Bridge StreetLowApartment26', 'Bridge Street', '{\"x\":1046.4715576172,\"y\":-497.48861694336,\"z\":64.079322814941}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1046.4715576172,\"y\":-497.48861694336,\"z\":64.079322814941}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '90333');
INSERT INTO `properties` VALUES ('186', 'Nikola AvenueLowApartment27', 'Nikola Avenue', '{\"x\":1004.5206298828,\"y\":-512.43347167969,\"z\":60.693511962891}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1004.5206298828,\"y\":-512.43347167969,\"z\":60.693511962891}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '105365');
INSERT INTO `properties` VALUES ('187', 'Nikola AvenueLowApartment28', 'Nikola Avenue', '{\"x\":988.31677246094,\"y\":-526.54357910156,\"z\":60.476100921631}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":988.31677246094,\"y\":-526.54357910156,\"z\":60.476100921631}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '89796');
INSERT INTO `properties` VALUES ('188', 'Nikola AvenueLowApartment29', 'Nikola Avenue', '{\"x\":965.83898925781,\"y\":-543.04620361328,\"z\":59.359092712402}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":965.83898925781,\"y\":-543.04620361328,\"z\":59.359092712402}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '99292');
INSERT INTO `properties` VALUES ('189', 'Nikola AvenueLowApartment30', 'Nikola Avenue', '{\"x\":963.65783691406,\"y\":-595.93206787109,\"z\":59.90270614624}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":963.65783691406,\"y\":-595.93206787109,\"z\":59.90270614624}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '102714');
INSERT INTO `properties` VALUES ('190', 'Nikola AvenueLowApartment31', 'Nikola Avenue', '{\"x\":975.796875,\"y\":-579.63757324219,\"z\":59.635547637939}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":975.796875,\"y\":-579.63757324219,\"z\":59.635547637939}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '102008');
INSERT INTO `properties` VALUES ('191', 'Mirror PlaceLowApartment32', 'Mirror Place', '{\"x\":1010.8396606445,\"y\":-572.66796875,\"z\":60.594429016113}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-100.008}', '{\"x\":1010.8396606445,\"y\":-572.66796875,\"z\":60.594429016113}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '92336');
INSERT INTO `properties` VALUES ('192', 'Mirror PlaceLowApartment33', 'Mirror Place', '{\"x\":1001.264465332,\"y\":-594.24005126953,\"z\":59.232467651367}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-101.008}', '{\"x\":265.307,\"y\":-1002.802,\"z\":-101.008}', '{\"x\":1001.264465332,\"y\":-594.24005126953,\"z\":59.232467651367}', '[]', null, '1', '0', null, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', '105474');
INSERT INTO `properties` VALUES ('193', 'West Mirror DriveLuxApartment34', 'West Mirror Drive', '{\"x\":979.28515625,\"y\":-627.31146240234,\"z\":59.235836029053}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":979.28515625,\"y\":-627.31146240234,\"z\":59.235836029053}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', '916439');
INSERT INTO `properties` VALUES ('194', 'Ace Jones DriveLuxApartment1', 'Ace Jones Drive', '{\"x\":-1540.1517333984,\"y\":421.90277099609,\"z\":110.01378631592}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1540.1517333984,\"y\":421.90277099609,\"z\":110.01378631592}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1825893');
INSERT INTO `properties` VALUES ('195', 'Ace Jones DriveLuxApartment2', 'Ace Jones Drive', '{\"x\":-1496.3151855469,\"y\":437.80752563477,\"z\":112.49752807617}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1496.3151855469,\"y\":437.80752563477,\"z\":112.49752807617}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1413305');
INSERT INTO `properties` VALUES ('196', 'Ace Jones DriveLuxApartment3', 'Ace Jones Drive', '{\"x\":-1454.1470947266,\"y\":513.10931396484,\"z\":117.64394378662}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1454.1470947266,\"y\":513.10931396484,\"z\":117.64394378662}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1322627');
INSERT INTO `properties` VALUES ('197', 'Ace Jones DriveLuxApartment4', 'Ace Jones Drive', '{\"x\":-1500.3343505859,\"y\":522.37237548828,\"z\":118.27212524414}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1500.3343505859,\"y\":522.37237548828,\"z\":118.27212524414}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1406898');
INSERT INTO `properties` VALUES ('198', 'Ace Jones DriveLuxApartment5', 'Ace Jones Drive', '{\"x\":-1451.9193115234,\"y\":545.38983154297,\"z\":120.79940795898}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1451.9193115234,\"y\":545.38983154297,\"z\":120.79940795898}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1363970');
INSERT INTO `properties` VALUES ('199', 'Hangman AvenueLuxApartment6', 'Hangman Avenue', '{\"x\":-1405.9122314453,\"y\":526.81701660156,\"z\":123.83125305176}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1405.9122314453,\"y\":526.81701660156,\"z\":123.83125305176}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1331091');
INSERT INTO `properties` VALUES ('200', 'Hangman AvenueLuxApartment7', 'Hangman Avenue', '{\"x\":-1405.2404785156,\"y\":561.19598388672,\"z\":125.40619659424}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1405.2404785156,\"y\":561.19598388672,\"z\":125.40619659424}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1315849');
INSERT INTO `properties` VALUES ('201', 'Hangman AvenueLuxApartment8', 'Hangman Avenue', '{\"x\":-1366.46875,\"y\":611.55004882813,\"z\":133.92456054688}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1366.46875,\"y\":611.55004882813,\"z\":133.92456054688}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2288019');
INSERT INTO `properties` VALUES ('202', 'Hangman AvenueLuxApartment9', 'Hangman Avenue', '{\"x\":-1338.1193847656,\"y\":606.13049316406,\"z\":134.37992858887}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1338.1193847656,\"y\":606.13049316406,\"z\":134.37992858887}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1758483');
INSERT INTO `properties` VALUES ('203', 'Hangman AvenueLuxApartment10', 'Hangman Avenue', '{\"x\":-1363.8488769531,\"y\":569.89282226563,\"z\":134.97283935547}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1363.8488769531,\"y\":569.89282226563,\"z\":134.97283935547}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2225161');
INSERT INTO `properties` VALUES ('204', 'Hangman AvenueLuxApartment11', 'Hangman Avenue', '{\"x\":-1346.9027099609,\"y\":560.84869384766,\"z\":130.53153991699}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1346.9027099609,\"y\":560.84869384766,\"z\":130.53153991699}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1940278');
INSERT INTO `properties` VALUES ('205', 'North Sheldon AvenueLuxApartment12', 'North Sheldon Avenue', '{\"x\":-1291.3852539063,\"y\":649.32806396484,\"z\":141.50144958496}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1291.3852539063,\"y\":649.32806396484,\"z\":141.50144958496}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1432190');
INSERT INTO `properties` VALUES ('206', 'North Sheldon AvenueLuxApartment13', 'North Sheldon Avenue', '{\"x\":-1277.7301025391,\"y\":629.96185302734,\"z\":143.19226074219}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1277.7301025391,\"y\":629.96185302734,\"z\":143.19226074219}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1975282');
INSERT INTO `properties` VALUES ('207', 'North Sheldon AvenueLuxApartment14', 'North Sheldon Avenue', '{\"x\":-1248.2115478516,\"y\":643.39379882813,\"z\":142.65231323242}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1248.2115478516,\"y\":643.39379882813,\"z\":142.65231323242}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1633645');
INSERT INTO `properties` VALUES ('208', 'North Sheldon AvenueLuxApartment15', 'North Sheldon Avenue', '{\"x\":-1241.6223144531,\"y\":673.69683837891,\"z\":142.81593322754}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1241.6223144531,\"y\":673.69683837891,\"z\":142.81593322754}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1927464');
INSERT INTO `properties` VALUES ('209', 'North Sheldon AvenueLuxApartment16', 'North Sheldon Avenue', '{\"x\":-1219.4108886719,\"y\":665.17425537109,\"z\":144.53370666504}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1219.4108886719,\"y\":665.17425537109,\"z\":144.53370666504}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1483548');
INSERT INTO `properties` VALUES ('210', 'North Sheldon AvenueLuxApartment17', 'North Sheldon Avenue', '{\"x\":-1197.5023193359,\"y\":693.49377441406,\"z\":147.40734863281}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1197.5023193359,\"y\":693.49377441406,\"z\":147.40734863281}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1898058');
INSERT INTO `properties` VALUES ('211', 'North Sheldon AvenueLuxApartment18', 'North Sheldon Avenue', '{\"x\":-1165.4506835938,\"y\":727.36590576172,\"z\":155.60679626465}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1165.4506835938,\"y\":727.36590576172,\"z\":155.60679626465}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1935051');
INSERT INTO `properties` VALUES ('212', 'North Sheldon AvenueLuxApartment19', 'North Sheldon Avenue', '{\"x\":-1118.1479492188,\"y\":762.20751953125,\"z\":164.28875732422}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1118.1479492188,\"y\":762.20751953125,\"z\":164.28875732422}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1654822');
INSERT INTO `properties` VALUES ('213', 'North Sheldon AvenueLuxApartment20', 'North Sheldon Avenue', '{\"x\":-1129.8489990234,\"y\":783.94305419922,\"z\":163.88703918457}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1129.8489990234,\"y\":783.94305419922,\"z\":163.88703918457}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2235649');
INSERT INTO `properties` VALUES ('214', 'North Sheldon AvenueLuxApartment21', 'North Sheldon Avenue', '{\"x\":-1100.5798339844,\"y\":797.28149414063,\"z\":167.25796508789}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1100.5798339844,\"y\":797.28149414063,\"z\":167.25796508789}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1516494');
INSERT INTO `properties` VALUES ('215', 'North Sheldon AvenueLuxApartment22', 'North Sheldon Avenue', '{\"x\":-1067.6361083984,\"y\":795.04876708984,\"z\":166.92190551758}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1067.6361083984,\"y\":795.04876708984,\"z\":166.92190551758}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1624034');
INSERT INTO `properties` VALUES ('216', 'Hillcrest AvenueLuxApartment23', 'Hillcrest Avenue', '{\"x\":-1056.0628662109,\"y\":761.55041503906,\"z\":167.31658935547}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1056.0628662109,\"y\":761.55041503906,\"z\":167.31658935547}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2125176');
INSERT INTO `properties` VALUES ('217', 'Hillcrest AvenueLuxApartment24', 'Hillcrest Avenue', '{\"x\":-1065.3902587891,\"y\":727.29650878906,\"z\":165.47462463379}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1065.3902587891,\"y\":727.29650878906,\"z\":165.47462463379}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1784011');
INSERT INTO `properties` VALUES ('218', 'Hillcrest AvenueLuxApartment25', 'Hillcrest Avenue', '{\"x\":-1002.4211425781,\"y\":684.62292480469,\"z\":160.90394592285}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-1002.4211425781,\"y\":684.62292480469,\"z\":160.90394592285}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2228432');
INSERT INTO `properties` VALUES ('219', 'Hillcrest AvenueLuxApartment26', 'Hillcrest Avenue', '{\"x\":-931.75317382813,\"y\":691.40087890625,\"z\":153.46670532227}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-931.75317382813,\"y\":691.40087890625,\"z\":153.46670532227}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1843597');
INSERT INTO `properties` VALUES ('220', 'Hillcrest AvenueLuxApartment27', 'Hillcrest Avenue', '{\"x\":-908.76416015625,\"y\":694.35162353516,\"z\":151.43374633789}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-908.76416015625,\"y\":694.35162353516,\"z\":151.43374633789}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1838944');
INSERT INTO `properties` VALUES ('221', 'Hillcrest AvenueLuxApartment28', 'Hillcrest Avenue', '{\"x\":-885.44946289063,\"y\":699.21716308594,\"z\":151.27056884766}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-885.44946289063,\"y\":699.21716308594,\"z\":151.27056884766}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1447297');
INSERT INTO `properties` VALUES ('222', 'Hillcrest AvenueLuxApartment29', 'Hillcrest Avenue', '{\"x\":-853.17449951172,\"y\":696.08850097656,\"z\":148.78552246094}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-853.17449951172,\"y\":696.08850097656,\"z\":148.78552246094}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1751874');
INSERT INTO `properties` VALUES ('223', 'Hillcrest AvenueLuxApartment30', 'Hillcrest Avenue', '{\"x\":-819.52648925781,\"y\":697.07574462891,\"z\":148.10981750488}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-819.52648925781,\"y\":697.07574462891,\"z\":148.10981750488}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2005935');
INSERT INTO `properties` VALUES ('224', 'Hillcrest AvenueLuxApartment31', 'Hillcrest Avenue', '{\"x\":-764.53857421875,\"y\":651.15399169922,\"z\":145.50137329102}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-764.53857421875,\"y\":651.15399169922,\"z\":145.50137329102}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1534266');
INSERT INTO `properties` VALUES ('225', 'Hillcrest AvenueLuxApartment32', 'Hillcrest Avenue', '{\"x\":-751.33044433594,\"y\":621.18103027344,\"z\":142.24934387207}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-751.33044433594,\"y\":621.18103027344,\"z\":142.24934387207}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1641906');
INSERT INTO `properties` VALUES ('226', 'Hillcrest AvenueLuxApartment33', 'Hillcrest Avenue', '{\"x\":-732.18029785156,\"y\":595.59002685547,\"z\":141.82688903809}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-732.18029785156,\"y\":595.59002685547,\"z\":141.82688903809}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1690095');
INSERT INTO `properties` VALUES ('227', 'Hillcrest AvenueLuxApartment34', 'Hillcrest Avenue', '{\"x\":-704.45471191406,\"y\":589.56817626953,\"z\":141.9292755127}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-704.45471191406,\"y\":589.56817626953,\"z\":141.9292755127}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2111721');
INSERT INTO `properties` VALUES ('228', 'Hillcrest AvenueLuxApartment35', 'Hillcrest Avenue', '{\"x\":-686.38201904297,\"y\":596.75897216797,\"z\":143.6420135498}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-686.38201904297,\"y\":596.75897216797,\"z\":143.6420135498}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1302158');
INSERT INTO `properties` VALUES ('229', 'Hillcrest AvenueLuxApartment36', 'Hillcrest Avenue', '{\"x\":-700.82513427734,\"y\":648.19500732422,\"z\":155.17527770996}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-700.82513427734,\"y\":648.19500732422,\"z\":155.17527770996}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1627035');
INSERT INTO `properties` VALUES ('230', 'Normandy DriveLuxApartment37', 'Normandy Drive', '{\"x\":-564.24957275391,\"y\":683.94000244141,\"z\":146.20091247559}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-564.24957275391,\"y\":683.94000244141,\"z\":146.20091247559}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2369862');
INSERT INTO `properties` VALUES ('231', 'Normandy DriveLuxApartment38', 'Normandy Drive', '{\"x\":-559.19543457031,\"y\":664.49560546875,\"z\":145.45433044434}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-559.19543457031,\"y\":664.49560546875,\"z\":145.45433044434}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2132089');
INSERT INTO `properties` VALUES ('232', 'North Sheldon AvenueLuxApartment1', 'North Sheldon Avenue', '{\"x\":-998.62060546875,\"y\":816.19018554688,\"z\":173.04978942871}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-998.62060546875,\"y\":816.19018554688,\"z\":173.04978942871}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1568831');
INSERT INTO `properties` VALUES ('233', 'North Sheldon AvenueLuxApartment2', 'North Sheldon Avenue', '{\"x\":-998.00158691406,\"y\":769.24133300781,\"z\":171.42100524902}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-998.00158691406,\"y\":769.24133300781,\"z\":171.42100524902}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1618234');
INSERT INTO `properties` VALUES ('234', 'North Sheldon AvenueLuxApartment3', 'North Sheldon Avenue', '{\"x\":-962.67352294922,\"y\":812.99340820313,\"z\":177.56622314453}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-962.67352294922,\"y\":812.99340820313,\"z\":177.56622314453}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1683351');
INSERT INTO `properties` VALUES ('235', 'North Sheldon AvenueLuxApartment4', 'North Sheldon Avenue', '{\"x\":-912.38861083984,\"y\":778.07110595703,\"z\":187.01139831543}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-912.38861083984,\"y\":778.07110595703,\"z\":187.01139831543}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2309230');
INSERT INTO `properties` VALUES ('236', 'North Sheldon AvenueLuxApartment5', 'North Sheldon Avenue', '{\"x\":-921.05853271484,\"y\":813.16076660156,\"z\":184.33616638184}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-921.05853271484,\"y\":813.16076660156,\"z\":184.33616638184}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1395534');
INSERT INTO `properties` VALUES ('237', 'North Sheldon AvenueLuxApartment6', 'North Sheldon Avenue', '{\"x\":-867.65472412109,\"y\":786.28570556641,\"z\":191.93307495117}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-867.65472412109,\"y\":786.28570556641,\"z\":191.93307495117}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2044176');
INSERT INTO `properties` VALUES ('238', 'North Sheldon AvenueLuxApartment7', 'North Sheldon Avenue', '{\"x\":-824.13452148438,\"y\":806.2744140625,\"z\":202.78399658203}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-824.13452148438,\"y\":806.2744140625,\"z\":202.78399658203}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2218586');
INSERT INTO `properties` VALUES ('239', 'Normandy DriveLuxApartment8', 'Normandy Drive', '{\"x\":-747.03479003906,\"y\":808.23846435547,\"z\":215.02998352051}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-747.03479003906,\"y\":808.23846435547,\"z\":215.02998352051}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2139339');
INSERT INTO `properties` VALUES ('240', 'Normandy DriveLuxApartment9', 'Normandy Drive', '{\"x\":-655.19738769531,\"y\":803.77398681641,\"z\":198.9920501709}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-655.19738769531,\"y\":803.77398681641,\"z\":198.9920501709}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1687735');
INSERT INTO `properties` VALUES ('241', 'Normandy DriveLuxApartment10', 'Normandy Drive', '{\"x\":-587.37933349609,\"y\":806.12823486328,\"z\":191.2474822998}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-587.37933349609,\"y\":806.12823486328,\"z\":191.2474822998}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2356003');
INSERT INTO `properties` VALUES ('242', 'Normandy DriveLuxApartment11', 'Normandy Drive', '{\"x\":-595.34600830078,\"y\":781.00415039063,\"z\":189.10961914063}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-595.34600830078,\"y\":781.00415039063,\"z\":189.10961914063}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2337523');
INSERT INTO `properties` VALUES ('243', 'Normandy DriveLuxApartment12', 'Normandy Drive', '{\"x\":-596.98510742188,\"y\":763.32183837891,\"z\":189.12007141113}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-596.98510742188,\"y\":763.32183837891,\"z\":189.12007141113}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1690635');
INSERT INTO `properties` VALUES ('244', 'Normandy DriveLuxApartment13', 'Normandy Drive', '{\"x\":-579.89562988281,\"y\":733.75048828125,\"z\":184.20704650879}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-579.89562988281,\"y\":733.75048828125,\"z\":184.20704650879}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '2255983');
INSERT INTO `properties` VALUES ('245', 'Normandy DriveLuxApartment14', 'Normandy Drive', '{\"x\":-699.21026611328,\"y\":706.07305908203,\"z\":158.0075378418}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-680.124,\"y\":590.608,\"z\":144.392}', '{\"x\":-699.21026611328,\"y\":706.07305908203,\"z\":158.0075378418}', '[]', null, '1', '0', null, '{\"x\":-680.46,\"y\":588.6,\"z\":135.769}', '1638838');

-- ----------------------------
-- Table structure for `racket_organisation`
-- ----------------------------
DROP TABLE IF EXISTS `racket_organisation`;
CREATE TABLE `racket_organisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of racket_organisation
-- ----------------------------
INSERT INTO `racket_organisation` VALUES ('1', 'Raket', '3000', '0');
INSERT INTO `racket_organisation` VALUES ('2', 'Raket', '5000', '0');
INSERT INTO `racket_organisation` VALUES ('3', 'Raket', '10000', '1');
INSERT INTO `racket_organisation` VALUES ('4', 'Raket', '20000', '1');
INSERT INTO `racket_organisation` VALUES ('5', 'Raket', '50000', '2');
INSERT INTO `racket_organisation` VALUES ('6', 'Raket', '150000', '3');
INSERT INTO `racket_organisation` VALUES ('7', 'Raket', '350000', '3');

-- ----------------------------
-- Table structure for `rented_aircrafts`
-- ----------------------------
DROP TABLE IF EXISTS `rented_aircrafts`;
CREATE TABLE `rented_aircrafts` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(30) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rented_aircrafts
-- ----------------------------

-- ----------------------------
-- Table structure for `rented_boats`
-- ----------------------------
DROP TABLE IF EXISTS `rented_boats`;
CREATE TABLE `rented_boats` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(30) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rented_boats
-- ----------------------------

-- ----------------------------
-- Table structure for `rented_vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `rented_vehicles`;
CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rented_vehicles
-- ----------------------------

-- ----------------------------
-- Table structure for `rented_vehiclesmoto`
-- ----------------------------
DROP TABLE IF EXISTS `rented_vehiclesmoto`;
CREATE TABLE `rented_vehiclesmoto` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rented_vehiclesmoto
-- ----------------------------

-- ----------------------------
-- Table structure for `shops`
-- ----------------------------
DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shops
-- ----------------------------
INSERT INTO `shops` VALUES ('1', 'TwentyFourSeven', 'bread', '35');
INSERT INTO `shops` VALUES ('2', 'TwentyFourSeven', 'water', '22');
INSERT INTO `shops` VALUES ('3', 'RobsLiquor', 'bread', '32');
INSERT INTO `shops` VALUES ('4', 'RobsLiquor', 'water', '20');
INSERT INTO `shops` VALUES ('5', 'LTDgasoline', 'bread', '30');
INSERT INTO `shops` VALUES ('6', 'LTDgasoline', 'water', '15');
INSERT INTO `shops` VALUES ('16', 'TwentyFourSeven', 'cigarette', '20');
INSERT INTO `shops` VALUES ('22', 'TwentyFourSeven', 'lockpick', '2000');
INSERT INTO `shops` VALUES ('23', 'LTDgasoline', 'lockpick', '2000');
INSERT INTO `shops` VALUES ('24', 'RobsLiquor', 'lockpick', '2000');

-- ----------------------------
-- Table structure for `shops2`
-- ----------------------------
DROP TABLE IF EXISTS `shops2`;
CREATE TABLE `shops2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of shops2
-- ----------------------------
INSERT INTO `shops2` VALUES ('2', 'narekshop', 'clip', '150');
INSERT INTO `shops2` VALUES ('3', 'narekshop', 'yusuf', '300');
INSERT INTO `shops2` VALUES ('4', 'narekshop', 'grip', '300');
INSERT INTO `shops2` VALUES ('5', 'narekshop', 'flashlight', '300');
INSERT INTO `shops2` VALUES ('6', 'narekshop', 'silencieux', '150');
INSERT INTO `shops2` VALUES ('7', 'narekshop', 'bulletproof', '1000');
INSERT INTO `shops2` VALUES ('11', 'narekshop', 'smg_ammo', '1');
INSERT INTO `shops2` VALUES ('12', 'narekshop', 'pistol_ammo', '1');
INSERT INTO `shops2` VALUES ('13', 'narekshop', 'rifle_ammo', '1');
INSERT INTO `shops2` VALUES ('14', 'narekshop', 'shotgun_ammo', '1');

-- ----------------------------
-- Table structure for `sim_card`
-- ----------------------------
DROP TABLE IF EXISTS `sim_card`;
CREATE TABLE `sim_card` (
  `owner` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'Insert Sim',
  PRIMARY KEY (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sim_card
-- ----------------------------

-- ----------------------------
-- Table structure for `truck_inventory`
-- ----------------------------
DROP TABLE IF EXISTS `truck_inventory`;
CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) NOT NULL,
  `itemt` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `owned` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item` (`item`,`plate`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of truck_inventory
-- ----------------------------

-- ----------------------------
-- Table structure for `twitter_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `twitter_accounts`;
CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of twitter_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for `twitter_likes`
-- ----------------------------
DROP TABLE IF EXISTS `twitter_likes`;
CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  KEY `FK_twitter_likes_twitter_tweets` (`tweetId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of twitter_likes
-- ----------------------------
INSERT INTO `twitter_likes` VALUES ('1', '3', '10');
INSERT INTO `twitter_likes` VALUES ('2', '3', '9');
INSERT INTO `twitter_likes` VALUES ('3', '3', '1');
INSERT INTO `twitter_likes` VALUES ('4', '7', '33');
INSERT INTO `twitter_likes` VALUES ('5', '13', '39');
INSERT INTO `twitter_likes` VALUES ('6', '13', '46');
INSERT INTO `twitter_likes` VALUES ('7', '15', '51');
INSERT INTO `twitter_likes` VALUES ('8', '15', '46');
INSERT INTO `twitter_likes` VALUES ('13', '23', '154');
INSERT INTO `twitter_likes` VALUES ('14', '20', '154');
INSERT INTO `twitter_likes` VALUES ('15', '13', '160');
INSERT INTO `twitter_likes` VALUES ('16', '19', '160');
INSERT INTO `twitter_likes` VALUES ('17', '20', '161');
INSERT INTO `twitter_likes` VALUES ('18', '13', '161');
INSERT INTO `twitter_likes` VALUES ('21', '20', '189');
INSERT INTO `twitter_likes` VALUES ('22', '15', '191');
INSERT INTO `twitter_likes` VALUES ('23', '16', '211');
INSERT INTO `twitter_likes` VALUES ('24', '16', '233');

-- ----------------------------
-- Table structure for `twitter_tweets`
-- ----------------------------
DROP TABLE IF EXISTS `twitter_tweets`;
CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_tweets_twitter_accounts` (`authorId`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of twitter_tweets
-- ----------------------------

-- ----------------------------
-- Table structure for `user_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) NOT NULL,
  `name` varchar(50) NOT NULL,
  `money` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_accounts
-- ----------------------------
INSERT INTO `user_accounts` VALUES ('44', 'steam:11000010c2709d9', 'black_money', '52585');
INSERT INTO `user_accounts` VALUES ('45', 'steam:110000104c989b7', 'black_money', '0');
INSERT INTO `user_accounts` VALUES ('46', 'steam:11000010c2b4bd6', 'black_money', '940');
INSERT INTO `user_accounts` VALUES ('47', 'steam:110000117fd8bdf', 'black_money', '0');
INSERT INTO `user_accounts` VALUES ('48', 'steam:11000010f9264c0', 'black_money', '0');
INSERT INTO `user_accounts` VALUES ('49', 'steam:110000105e314d0', 'black_money', '0');

-- ----------------------------
-- Table structure for `user_inventory`
-- ----------------------------
DROP TABLE IF EXISTS `user_inventory`;
CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9761 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_inventory
-- ----------------------------
INSERT INTO `user_inventory` VALUES ('8351', 'steam:11000010c2709d9', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('8352', 'steam:11000010c2709d9', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('8353', 'steam:11000010c2709d9', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('8354', 'steam:11000010c2709d9', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('8355', 'steam:11000010c2709d9', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('8356', 'steam:11000010c2709d9', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('8357', 'steam:11000010c2709d9', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('8358', 'steam:11000010c2709d9', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('8359', 'steam:11000010c2709d9', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8360', 'steam:11000010c2709d9', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8361', 'steam:11000010c2709d9', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('8362', 'steam:11000010c2709d9', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('8363', 'steam:11000010c2709d9', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('8364', 'steam:11000010c2709d9', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('8365', 'steam:11000010c2709d9', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('8366', 'steam:11000010c2709d9', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('8367', 'steam:11000010c2709d9', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8368', 'steam:11000010c2709d9', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('8369', 'steam:11000010c2709d9', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('8370', 'steam:11000010c2709d9', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('8371', 'steam:11000010c2709d9', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('8372', 'steam:11000010c2709d9', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8373', 'steam:11000010c2709d9', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('8374', 'steam:11000010c2709d9', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('8375', 'steam:11000010c2709d9', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('8376', 'steam:11000010c2709d9', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('8377', 'steam:11000010c2709d9', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('8378', 'steam:11000010c2709d9', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('8379', 'steam:11000010c2709d9', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('8380', 'steam:11000010c2709d9', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('8381', 'steam:11000010c2709d9', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('8382', 'steam:11000010c2709d9', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('8383', 'steam:11000010c2709d9', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('8384', 'steam:11000010c2709d9', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8385', 'steam:11000010c2709d9', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('8386', 'steam:11000010c2709d9', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('8387', 'steam:11000010c2709d9', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('8388', 'steam:11000010c2709d9', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8389', 'steam:11000010c2709d9', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('8390', 'steam:11000010c2709d9', 'fixtool', '1');
INSERT INTO `user_inventory` VALUES ('8391', 'steam:11000010c2709d9', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('8392', 'steam:11000010c2709d9', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('8393', 'steam:11000010c2709d9', 'weed', '0');
INSERT INTO `user_inventory` VALUES ('8394', 'steam:11000010c2709d9', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('8395', 'steam:11000010c2709d9', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('8396', 'steam:11000010c2709d9', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('8397', 'steam:11000010c2709d9', 'bat', '0');
INSERT INTO `user_inventory` VALUES ('8398', 'steam:11000010c2709d9', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('8399', 'steam:11000010c2709d9', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('8400', 'steam:11000010c2709d9', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('8401', 'steam:11000010c2709d9', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('8402', 'steam:11000010c2709d9', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('8403', 'steam:11000010c2709d9', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('8404', 'steam:11000010c2709d9', 'rifle_ammo', '-1');
INSERT INTO `user_inventory` VALUES ('8405', 'steam:11000010c2709d9', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8406', 'steam:11000010c2709d9', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('8407', 'steam:11000010c2709d9', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('8408', 'steam:11000010c2709d9', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('8409', 'steam:11000010c2709d9', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('8410', 'steam:11000010c2709d9', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('8411', 'steam:11000010c2709d9', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('8412', 'steam:11000010c2709d9', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8413', 'steam:11000010c2709d9', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('8414', 'steam:11000010c2709d9', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('8415', 'steam:11000010c2709d9', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('8416', 'steam:11000010c2709d9', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('8417', 'steam:11000010c2709d9', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('8418', 'steam:11000010c2709d9', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('8419', 'steam:11000010c2709d9', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('8420', 'steam:11000010c2709d9', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8421', 'steam:11000010c2709d9', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('8422', 'steam:11000010c2709d9', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('8423', 'steam:11000010c2709d9', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('8424', 'steam:11000010c2709d9', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8425', 'steam:11000010c2709d9', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8426', 'steam:11000010c2709d9', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('8427', 'steam:11000010c2709d9', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8428', 'steam:11000010c2709d9', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8429', 'steam:11000010c2709d9', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('8430', 'steam:11000010c2709d9', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('8431', 'steam:11000010c2709d9', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('8432', 'steam:11000010c2709d9', 'water', '0');
INSERT INTO `user_inventory` VALUES ('8433', 'steam:11000010c2709d9', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8434', 'steam:11000010c2709d9', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('8435', 'steam:11000010c2709d9', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('8436', 'steam:11000010c2709d9', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('8437', 'steam:11000010c2709d9', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8438', 'steam:11000010c2709d9', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8439', 'steam:11000010c2709d9', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8440', 'steam:11000010c2709d9', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8441', 'steam:11000010c2709d9', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('8442', 'steam:11000010c2709d9', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8443', 'steam:11000010c2709d9', 'lockpick', '10');
INSERT INTO `user_inventory` VALUES ('8444', 'steam:11000010c2709d9', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('8445', 'steam:11000010c2709d9', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('8446', 'steam:11000010c2709d9', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8447', 'steam:11000010c2709d9', 'medikit', '2');
INSERT INTO `user_inventory` VALUES ('8448', 'steam:11000010c2709d9', 'coke', '0');
INSERT INTO `user_inventory` VALUES ('8449', 'steam:11000010c2709d9', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('8450', 'steam:11000010c2709d9', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8451', 'steam:11000010c2709d9', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('8452', 'steam:11000010c2709d9', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('8453', 'steam:11000010c2709d9', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('8454', 'steam:11000010c2709d9', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8455', 'steam:11000010c2709d9', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8456', 'steam:11000010c2709d9', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('8457', 'steam:11000010c2709d9', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('8458', 'steam:11000010c2709d9', 'crack', '0');
INSERT INTO `user_inventory` VALUES ('8459', 'steam:11000010c2709d9', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('8460', 'steam:11000010c2709d9', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('8461', 'steam:11000010c2709d9', 'acier', '2');
INSERT INTO `user_inventory` VALUES ('8462', 'steam:11000010c2709d9', 'coke_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8463', 'steam:11000010c2709d9', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8464', 'steam:11000010c2709d9', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('8465', 'steam:11000010c2709d9', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('8466', 'steam:11000010c2709d9', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('8467', 'steam:11000010c2709d9', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('8468', 'steam:11000010c2709d9', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('8469', 'steam:11000010c2709d9', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8470', 'steam:11000010c2709d9', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8471', 'steam:11000010c2709d9', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('8472', 'steam:11000010c2709d9', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('8473', 'steam:11000010c2709d9', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('8474', 'steam:11000010c2709d9', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8475', 'steam:11000010c2709d9', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('8476', 'steam:11000010c2709d9', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('8477', 'steam:11000010c2709d9', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('8478', 'steam:11000010c2709d9', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('8479', 'steam:11000010c2709d9', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8480', 'steam:11000010c2709d9', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('8481', 'steam:11000010c2709d9', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('8482', 'steam:11000010c2709d9', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('8483', 'steam:11000010c2709d9', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('8484', 'steam:11000010c2709d9', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8485', 'steam:11000010c2709d9', 'poudre', '53');
INSERT INTO `user_inventory` VALUES ('8486', 'steam:11000010c2709d9', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8487', 'steam:11000010c2709d9', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8488', 'steam:11000010c2709d9', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('8489', 'steam:11000010c2709d9', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8490', 'steam:11000010c2709d9', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('8491', 'steam:11000010c2709d9', 'knife', '0');
INSERT INTO `user_inventory` VALUES ('8492', 'steam:11000010c2709d9', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8493', 'steam:11000010c2709d9', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8494', 'steam:11000010c2709d9', 'bread', '0');
INSERT INTO `user_inventory` VALUES ('8495', 'steam:11000010c2709d9', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8496', 'steam:11000010c2709d9', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8497', 'steam:11000010c2709d9', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8498', 'steam:11000010c2709d9', 'pistol_ammo', '-1');
INSERT INTO `user_inventory` VALUES ('8499', 'steam:11000010c2709d9', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8500', 'steam:11000010c2709d9', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('8501', 'steam:11000010c2709d9', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('8502', 'steam:11000010c2709d9', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('8503', 'steam:11000010c2709d9', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('8504', 'steam:11000010c2709d9', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('8505', 'steam:11000010c2709d9', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('8506', 'steam:11000010c2709d9', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('8507', 'steam:11000010c2709d9', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8508', 'steam:11000010c2709d9', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('8509', 'steam:11000010c2709d9', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('8510', 'steam:11000010c2709d9', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('8511', 'steam:11000010c2709d9', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('8512', 'steam:11000010c2709d9', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8513', 'steam:11000010c2709d9', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8514', 'steam:11000010c2709d9', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('8515', 'steam:11000010c2709d9', 'lithium', '0');
INSERT INTO `user_inventory` VALUES ('8516', 'steam:11000010c2709d9', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8517', 'steam:11000010c2709d9', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('8518', 'steam:11000010c2709d9', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8519', 'steam:11000010c2709d9', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('8520', 'steam:11000010c2709d9', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('8521', 'steam:11000010c2709d9', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8522', 'steam:11000010c2709d9', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8523', 'steam:11000010c2709d9', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('8524', 'steam:11000010c2709d9', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('8525', 'steam:11000010c2709d9', 'clip', '2');
INSERT INTO `user_inventory` VALUES ('8526', 'steam:11000010c2709d9', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('8527', 'steam:11000010c2709d9', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('8528', 'steam:11000010c2709d9', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('8529', 'steam:11000010c2709d9', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('8530', 'steam:11000010c2709d9', 'carbon', '16');
INSERT INTO `user_inventory` VALUES ('8531', 'steam:11000010c2709d9', 'acetone', '0');
INSERT INTO `user_inventory` VALUES ('8532', 'steam:11000010c2709d9', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('8533', 'steam:11000010c2709d9', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('8534', 'steam:11000010c2709d9', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8535', 'steam:11000010c2709d9', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('8536', 'steam:11000010c2709d9', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('8537', 'steam:11000010c2709d9', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('8538', 'steam:11000010c2709d9', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('8539', 'steam:11000010c2709d9', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('8540', 'steam:11000010c2709d9', 'meth_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8541', 'steam:11000010c2709d9', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('8542', 'steam:11000010c2709d9', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('8543', 'steam:11000010c2709d9', 'methlab', '0');
INSERT INTO `user_inventory` VALUES ('8544', 'steam:11000010c2709d9', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('8545', 'steam:11000010c2709d9', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('8546', 'steam:11000010c2709d9', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('8547', 'steam:11000010c2709d9', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8548', 'steam:11000010c2709d9', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('8549', 'steam:11000010c2709d9', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('8550', 'steam:11000010c2709d9', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8551', 'steam:11000010c2709d9', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('8552', 'steam:11000010c2709d9', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('8553', 'steam:11000010c2709d9', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('8554', 'steam:11000010c2709d9', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('8555', 'steam:11000010c2709d9', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('8556', 'steam:11000010c2709d9', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('8557', 'steam:11000010c2709d9', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8558', 'steam:11000010c2709d9', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8559', 'steam:11000010c2709d9', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('8560', 'steam:11000010c2709d9', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('8561', 'steam:11000010c2709d9', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('8562', 'steam:11000010c2709d9', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('8563', 'steam:11000010c2709d9', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('8564', 'steam:11000010c2709d9', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8565', 'steam:11000010c2709d9', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('8566', 'steam:11000010c2709d9', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8567', 'steam:11000010c2709d9', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('8568', 'steam:11000010c2709d9', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('8569', 'steam:11000010c2709d9', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('8570', 'steam:11000010c2709d9', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('8571', 'steam:11000010c2709d9', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('8572', 'steam:11000010c2709d9', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('8573', 'steam:11000010c2709d9', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('8574', 'steam:11000010c2709d9', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('8575', 'steam:11000010c2709d9', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('8576', 'steam:11000010c2709d9', 'sim', '1');
INSERT INTO `user_inventory` VALUES ('8577', 'steam:11000010c2709d9', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('8578', 'steam:11000010c2709d9', 'weed_pooch', '35');
INSERT INTO `user_inventory` VALUES ('8579', 'steam:11000010c2709d9', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('8580', 'steam:11000010c2709d9', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('8581', 'steam:11000010c2709d9', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('8582', 'steam:11000010c2709d9', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('8583', 'steam:11000010c2709d9', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8584', 'steam:11000010c2709d9', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('8585', 'steam:11000010c2709d9', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('8586', 'steam:110000104c989b7', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('8587', 'steam:110000104c989b7', 'coke', '0');
INSERT INTO `user_inventory` VALUES ('8588', 'steam:110000104c989b7', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('8589', 'steam:110000104c989b7', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('8590', 'steam:110000104c989b7', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('8591', 'steam:110000104c989b7', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('8592', 'steam:110000104c989b7', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8593', 'steam:110000104c989b7', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8594', 'steam:110000104c989b7', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('8595', 'steam:110000104c989b7', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8596', 'steam:110000104c989b7', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('8597', 'steam:110000104c989b7', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('8598', 'steam:110000104c989b7', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8599', 'steam:110000104c989b7', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('8600', 'steam:110000104c989b7', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('8601', 'steam:110000104c989b7', 'acier', '0');
INSERT INTO `user_inventory` VALUES ('8602', 'steam:110000104c989b7', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8603', 'steam:110000104c989b7', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('8604', 'steam:110000104c989b7', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('8605', 'steam:110000104c989b7', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('8606', 'steam:110000104c989b7', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8607', 'steam:110000104c989b7', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('8608', 'steam:110000104c989b7', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('8609', 'steam:110000104c989b7', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('8610', 'steam:110000104c989b7', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('8611', 'steam:110000104c989b7', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('8612', 'steam:110000104c989b7', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('8613', 'steam:110000104c989b7', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('8614', 'steam:110000104c989b7', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('8615', 'steam:110000104c989b7', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('8616', 'steam:110000104c989b7', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('8617', 'steam:110000104c989b7', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('8618', 'steam:110000104c989b7', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('8619', 'steam:110000104c989b7', 'lockpick', '1');
INSERT INTO `user_inventory` VALUES ('8620', 'steam:110000104c989b7', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('8621', 'steam:110000104c989b7', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8622', 'steam:110000104c989b7', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('8623', 'steam:110000104c989b7', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('8624', 'steam:110000104c989b7', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('8625', 'steam:110000104c989b7', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('8626', 'steam:110000104c989b7', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('8627', 'steam:110000104c989b7', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('8628', 'steam:110000104c989b7', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('8629', 'steam:110000104c989b7', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8630', 'steam:110000104c989b7', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('8631', 'steam:110000104c989b7', 'rifle_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8632', 'steam:110000104c989b7', 'carbon', '0');
INSERT INTO `user_inventory` VALUES ('8633', 'steam:110000104c989b7', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8634', 'steam:110000104c989b7', 'bat', '0');
INSERT INTO `user_inventory` VALUES ('8635', 'steam:110000104c989b7', 'knife', '0');
INSERT INTO `user_inventory` VALUES ('8636', 'steam:110000104c989b7', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('8637', 'steam:110000104c989b7', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('8638', 'steam:110000104c989b7', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('8639', 'steam:110000104c989b7', 'poudre', '0');
INSERT INTO `user_inventory` VALUES ('8640', 'steam:110000104c989b7', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('8641', 'steam:110000104c989b7', 'water', '0');
INSERT INTO `user_inventory` VALUES ('8642', 'steam:110000104c989b7', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('8643', 'steam:110000104c989b7', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8644', 'steam:110000104c989b7', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8645', 'steam:110000104c989b7', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('8646', 'steam:110000104c989b7', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('8647', 'steam:110000104c989b7', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8648', 'steam:110000104c989b7', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('8649', 'steam:110000104c989b7', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('8650', 'steam:110000104c989b7', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('8651', 'steam:110000104c989b7', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8652', 'steam:110000104c989b7', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('8653', 'steam:110000104c989b7', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('8654', 'steam:110000104c989b7', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('8655', 'steam:110000104c989b7', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8656', 'steam:110000104c989b7', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('8657', 'steam:110000104c989b7', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('8658', 'steam:110000104c989b7', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8659', 'steam:110000104c989b7', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8660', 'steam:110000104c989b7', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('8661', 'steam:110000104c989b7', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('8662', 'steam:110000104c989b7', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8663', 'steam:110000104c989b7', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('8664', 'steam:110000104c989b7', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('8665', 'steam:110000104c989b7', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8666', 'steam:110000104c989b7', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('8667', 'steam:110000104c989b7', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('8668', 'steam:110000104c989b7', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('8669', 'steam:110000104c989b7', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('8670', 'steam:110000104c989b7', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('8671', 'steam:110000104c989b7', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('8672', 'steam:110000104c989b7', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('8673', 'steam:110000104c989b7', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8674', 'steam:110000104c989b7', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('8675', 'steam:110000104c989b7', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('8676', 'steam:110000104c989b7', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('8677', 'steam:110000104c989b7', 'coke_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8678', 'steam:110000104c989b7', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('8679', 'steam:110000104c989b7', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8680', 'steam:110000104c989b7', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('8681', 'steam:110000104c989b7', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('8682', 'steam:110000104c989b7', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('8683', 'steam:110000104c989b7', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('8684', 'steam:110000104c989b7', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('8685', 'steam:110000104c989b7', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('8686', 'steam:110000104c989b7', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('8687', 'steam:110000104c989b7', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('8688', 'steam:110000104c989b7', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('8689', 'steam:110000104c989b7', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('8690', 'steam:110000104c989b7', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('8691', 'steam:110000104c989b7', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8692', 'steam:110000104c989b7', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('8693', 'steam:110000104c989b7', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8694', 'steam:110000104c989b7', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('8695', 'steam:110000104c989b7', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('8696', 'steam:110000104c989b7', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8697', 'steam:110000104c989b7', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('8698', 'steam:110000104c989b7', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('8699', 'steam:110000104c989b7', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('8700', 'steam:110000104c989b7', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('8701', 'steam:110000104c989b7', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8702', 'steam:110000104c989b7', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8703', 'steam:110000104c989b7', 'sim', '0');
INSERT INTO `user_inventory` VALUES ('8704', 'steam:110000104c989b7', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8705', 'steam:110000104c989b7', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('8706', 'steam:110000104c989b7', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8707', 'steam:110000104c989b7', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8708', 'steam:110000104c989b7', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8709', 'steam:110000104c989b7', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('8710', 'steam:110000104c989b7', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('8711', 'steam:110000104c989b7', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('8712', 'steam:110000104c989b7', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8713', 'steam:110000104c989b7', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('8714', 'steam:110000104c989b7', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('8715', 'steam:110000104c989b7', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('8716', 'steam:110000104c989b7', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('8717', 'steam:110000104c989b7', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8718', 'steam:110000104c989b7', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('8719', 'steam:110000104c989b7', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('8720', 'steam:110000104c989b7', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('8721', 'steam:110000104c989b7', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('8722', 'steam:110000104c989b7', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('8723', 'steam:110000104c989b7', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('8724', 'steam:110000104c989b7', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8725', 'steam:110000104c989b7', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8726', 'steam:110000104c989b7', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('8727', 'steam:110000104c989b7', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('8728', 'steam:110000104c989b7', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('8729', 'steam:110000104c989b7', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8730', 'steam:110000104c989b7', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('8731', 'steam:110000104c989b7', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('8732', 'steam:110000104c989b7', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('8733', 'steam:110000104c989b7', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8734', 'steam:110000104c989b7', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8735', 'steam:110000104c989b7', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8736', 'steam:110000104c989b7', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('8737', 'steam:110000104c989b7', 'pistol_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8738', 'steam:110000104c989b7', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8739', 'steam:110000104c989b7', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('8740', 'steam:110000104c989b7', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('8741', 'steam:110000104c989b7', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('8742', 'steam:110000104c989b7', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('8743', 'steam:110000104c989b7', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('8744', 'steam:110000104c989b7', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('8745', 'steam:110000104c989b7', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('8746', 'steam:110000104c989b7', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('8747', 'steam:110000104c989b7', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('8748', 'steam:110000104c989b7', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8749', 'steam:110000104c989b7', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8750', 'steam:110000104c989b7', 'acetone', '0');
INSERT INTO `user_inventory` VALUES ('8751', 'steam:110000104c989b7', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('8752', 'steam:110000104c989b7', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('8753', 'steam:110000104c989b7', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('8754', 'steam:110000104c989b7', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('8755', 'steam:110000104c989b7', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('8756', 'steam:110000104c989b7', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8757', 'steam:110000104c989b7', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('8758', 'steam:110000104c989b7', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('8759', 'steam:110000104c989b7', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('8760', 'steam:110000104c989b7', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8761', 'steam:110000104c989b7', 'fixtool', '0');
INSERT INTO `user_inventory` VALUES ('8762', 'steam:110000104c989b7', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('8763', 'steam:110000104c989b7', 'weed', '0');
INSERT INTO `user_inventory` VALUES ('8764', 'steam:110000104c989b7', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('8765', 'steam:110000104c989b7', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('8766', 'steam:110000104c989b7', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('8767', 'steam:110000104c989b7', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8768', 'steam:110000104c989b7', 'bread', '0');
INSERT INTO `user_inventory` VALUES ('8769', 'steam:110000104c989b7', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('8770', 'steam:110000104c989b7', 'meth_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8771', 'steam:110000104c989b7', 'methlab', '0');
INSERT INTO `user_inventory` VALUES ('8772', 'steam:110000104c989b7', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('8773', 'steam:110000104c989b7', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('8774', 'steam:110000104c989b7', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8775', 'steam:110000104c989b7', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('8776', 'steam:110000104c989b7', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('8777', 'steam:110000104c989b7', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('8778', 'steam:110000104c989b7', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('8779', 'steam:110000104c989b7', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('8780', 'steam:110000104c989b7', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('8781', 'steam:110000104c989b7', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('8782', 'steam:110000104c989b7', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('8783', 'steam:110000104c989b7', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('8784', 'steam:110000104c989b7', 'medikit', '0');
INSERT INTO `user_inventory` VALUES ('8785', 'steam:110000104c989b7', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('8786', 'steam:110000104c989b7', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('8787', 'steam:110000104c989b7', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('8788', 'steam:110000104c989b7', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8789', 'steam:110000104c989b7', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('8790', 'steam:110000104c989b7', 'weed_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8791', 'steam:110000104c989b7', 'crack', '0');
INSERT INTO `user_inventory` VALUES ('8792', 'steam:110000104c989b7', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('8793', 'steam:110000104c989b7', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('8794', 'steam:110000104c989b7', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('8795', 'steam:110000104c989b7', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('8796', 'steam:110000104c989b7', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('8797', 'steam:110000104c989b7', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('8798', 'steam:110000104c989b7', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8799', 'steam:110000104c989b7', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('8800', 'steam:110000104c989b7', 'lithium', '0');
INSERT INTO `user_inventory` VALUES ('8801', 'steam:110000104c989b7', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8802', 'steam:110000104c989b7', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('8803', 'steam:110000104c989b7', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('8804', 'steam:110000104c989b7', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8805', 'steam:110000104c989b7', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('8806', 'steam:110000104c989b7', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8807', 'steam:110000104c989b7', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('8808', 'steam:110000104c989b7', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('8809', 'steam:110000104c989b7', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('8810', 'steam:110000104c989b7', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8811', 'steam:110000104c989b7', 'clip', '0');
INSERT INTO `user_inventory` VALUES ('8812', 'steam:110000104c989b7', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8813', 'steam:110000104c989b7', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8814', 'steam:110000104c989b7', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8815', 'steam:110000104c989b7', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('8816', 'steam:110000104c989b7', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('8817', 'steam:110000104c989b7', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('8818', 'steam:110000104c989b7', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('8819', 'steam:110000104c989b7', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('8820', 'steam:110000104c989b7', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('8821', 'steam:11000010c2b4bd6', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('8822', 'steam:11000010c2b4bd6', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('8823', 'steam:11000010c2b4bd6', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8824', 'steam:11000010c2b4bd6', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8825', 'steam:11000010c2b4bd6', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8826', 'steam:11000010c2b4bd6', 'carbon', '0');
INSERT INTO `user_inventory` VALUES ('8827', 'steam:11000010c2b4bd6', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('8828', 'steam:11000010c2b4bd6', 'fixtool', '0');
INSERT INTO `user_inventory` VALUES ('8829', 'steam:11000010c2b4bd6', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('8830', 'steam:11000010c2b4bd6', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('8831', 'steam:11000010c2b4bd6', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('8832', 'steam:11000010c2b4bd6', 'weed', '2');
INSERT INTO `user_inventory` VALUES ('8833', 'steam:11000010c2b4bd6', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8834', 'steam:11000010c2b4bd6', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('8835', 'steam:11000010c2b4bd6', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('8836', 'steam:11000010c2b4bd6', 'lithium', '20');
INSERT INTO `user_inventory` VALUES ('8837', 'steam:11000010c2b4bd6', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('8838', 'steam:11000010c2b4bd6', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('8839', 'steam:11000010c2b4bd6', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('8840', 'steam:11000010c2b4bd6', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('8841', 'steam:11000010c2b4bd6', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('8842', 'steam:11000010c2b4bd6', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('8843', 'steam:11000010c2b4bd6', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('8844', 'steam:11000010c2b4bd6', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('8845', 'steam:11000010c2b4bd6', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('8846', 'steam:11000010c2b4bd6', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8847', 'steam:11000010c2b4bd6', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('8848', 'steam:11000010c2b4bd6', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('8849', 'steam:11000010c2b4bd6', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('8850', 'steam:11000010c2b4bd6', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('8851', 'steam:11000010c2b4bd6', 'bat', '1');
INSERT INTO `user_inventory` VALUES ('8852', 'steam:11000010c2b4bd6', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('8853', 'steam:11000010c2b4bd6', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('8854', 'steam:11000010c2b4bd6', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('8855', 'steam:11000010c2b4bd6', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8856', 'steam:11000010c2b4bd6', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('8857', 'steam:11000010c2b4bd6', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('8858', 'steam:11000010c2b4bd6', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('8859', 'steam:11000010c2b4bd6', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8860', 'steam:11000010c2b4bd6', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('8861', 'steam:11000010c2b4bd6', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('8862', 'steam:11000010c2b4bd6', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('8863', 'steam:11000010c2b4bd6', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('8864', 'steam:11000010c2b4bd6', 'coke_pooch', '9');
INSERT INTO `user_inventory` VALUES ('8865', 'steam:11000010c2b4bd6', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('8866', 'steam:11000010c2b4bd6', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('8867', 'steam:11000010c2b4bd6', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('8868', 'steam:11000010c2b4bd6', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('8869', 'steam:11000010c2b4bd6', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('8870', 'steam:11000010c2b4bd6', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8871', 'steam:11000010c2b4bd6', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('8872', 'steam:11000010c2b4bd6', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('8873', 'steam:11000010c2b4bd6', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('8874', 'steam:11000010c2b4bd6', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('8875', 'steam:11000010c2b4bd6', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('8876', 'steam:11000010c2b4bd6', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('8877', 'steam:11000010c2b4bd6', 'crack', '-1');
INSERT INTO `user_inventory` VALUES ('8878', 'steam:11000010c2b4bd6', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8879', 'steam:11000010c2b4bd6', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('8880', 'steam:11000010c2b4bd6', 'water', '0');
INSERT INTO `user_inventory` VALUES ('8881', 'steam:11000010c2b4bd6', 'knife', '1');
INSERT INTO `user_inventory` VALUES ('8882', 'steam:11000010c2b4bd6', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('8883', 'steam:11000010c2b4bd6', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('8884', 'steam:11000010c2b4bd6', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('8885', 'steam:11000010c2b4bd6', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('8886', 'steam:11000010c2b4bd6', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('8887', 'steam:11000010c2b4bd6', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('8888', 'steam:11000010c2b4bd6', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('8889', 'steam:11000010c2b4bd6', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('8890', 'steam:11000010c2b4bd6', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8891', 'steam:11000010c2b4bd6', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('8892', 'steam:11000010c2b4bd6', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('8893', 'steam:11000010c2b4bd6', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('8894', 'steam:11000010c2b4bd6', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('8895', 'steam:11000010c2b4bd6', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8896', 'steam:11000010c2b4bd6', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8897', 'steam:11000010c2b4bd6', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8898', 'steam:11000010c2b4bd6', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8899', 'steam:11000010c2b4bd6', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('8900', 'steam:11000010c2b4bd6', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('8901', 'steam:11000010c2b4bd6', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8902', 'steam:11000010c2b4bd6', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('8903', 'steam:11000010c2b4bd6', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('8904', 'steam:11000010c2b4bd6', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8905', 'steam:11000010c2b4bd6', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8906', 'steam:11000010c2b4bd6', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('8907', 'steam:11000010c2b4bd6', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('8908', 'steam:11000010c2b4bd6', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8909', 'steam:11000010c2b4bd6', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('8910', 'steam:11000010c2b4bd6', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8911', 'steam:11000010c2b4bd6', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('8912', 'steam:11000010c2b4bd6', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8913', 'steam:11000010c2b4bd6', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8914', 'steam:11000010c2b4bd6', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8915', 'steam:11000010c2b4bd6', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('8916', 'steam:11000010c2b4bd6', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('8917', 'steam:11000010c2b4bd6', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('8918', 'steam:11000010c2b4bd6', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8919', 'steam:11000010c2b4bd6', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8920', 'steam:11000010c2b4bd6', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('8921', 'steam:11000010c2b4bd6', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('8922', 'steam:11000010c2b4bd6', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('8923', 'steam:11000010c2b4bd6', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('8924', 'steam:11000010c2b4bd6', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('8925', 'steam:11000010c2b4bd6', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('8926', 'steam:11000010c2b4bd6', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8927', 'steam:11000010c2b4bd6', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('8928', 'steam:11000010c2b4bd6', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8929', 'steam:11000010c2b4bd6', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('8930', 'steam:11000010c2b4bd6', 'medikit', '0');
INSERT INTO `user_inventory` VALUES ('8931', 'steam:11000010c2b4bd6', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8932', 'steam:11000010c2b4bd6', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('8933', 'steam:11000010c2b4bd6', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('8934', 'steam:11000010c2b4bd6', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8935', 'steam:11000010c2b4bd6', 'methlab', '20');
INSERT INTO `user_inventory` VALUES ('8936', 'steam:11000010c2b4bd6', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8937', 'steam:11000010c2b4bd6', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('8938', 'steam:11000010c2b4bd6', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('8939', 'steam:11000010c2b4bd6', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('8940', 'steam:11000010c2b4bd6', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('8941', 'steam:11000010c2b4bd6', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('8942', 'steam:11000010c2b4bd6', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('8943', 'steam:11000010c2b4bd6', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8944', 'steam:11000010c2b4bd6', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('8945', 'steam:11000010c2b4bd6', 'sim', '0');
INSERT INTO `user_inventory` VALUES ('8946', 'steam:11000010c2b4bd6', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8947', 'steam:11000010c2b4bd6', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('8948', 'steam:11000010c2b4bd6', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('8949', 'steam:11000010c2b4bd6', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('8950', 'steam:11000010c2b4bd6', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('8951', 'steam:11000010c2b4bd6', 'rifle_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8952', 'steam:11000010c2b4bd6', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('8953', 'steam:11000010c2b4bd6', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('8954', 'steam:11000010c2b4bd6', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('8955', 'steam:11000010c2b4bd6', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('8956', 'steam:11000010c2b4bd6', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('8957', 'steam:11000010c2b4bd6', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('8958', 'steam:11000010c2b4bd6', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('8959', 'steam:11000010c2b4bd6', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('8960', 'steam:11000010c2b4bd6', 'bread', '0');
INSERT INTO `user_inventory` VALUES ('8961', 'steam:11000010c2b4bd6', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('8962', 'steam:11000010c2b4bd6', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8963', 'steam:11000010c2b4bd6', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('8964', 'steam:11000010c2b4bd6', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('8965', 'steam:11000010c2b4bd6', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8966', 'steam:11000010c2b4bd6', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('8967', 'steam:11000010c2b4bd6', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8968', 'steam:11000010c2b4bd6', 'acier', '0');
INSERT INTO `user_inventory` VALUES ('8969', 'steam:11000010c2b4bd6', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('8970', 'steam:11000010c2b4bd6', 'poudre', '0');
INSERT INTO `user_inventory` VALUES ('8971', 'steam:11000010c2b4bd6', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('8972', 'steam:11000010c2b4bd6', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('8973', 'steam:11000010c2b4bd6', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('8974', 'steam:11000010c2b4bd6', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('8975', 'steam:11000010c2b4bd6', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8976', 'steam:11000010c2b4bd6', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('8977', 'steam:11000010c2b4bd6', 'meth_pooch', '6');
INSERT INTO `user_inventory` VALUES ('8978', 'steam:11000010c2b4bd6', 'lockpick', '0');
INSERT INTO `user_inventory` VALUES ('8979', 'steam:11000010c2b4bd6', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('8980', 'steam:11000010c2b4bd6', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('8981', 'steam:11000010c2b4bd6', 'pistol_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8982', 'steam:11000010c2b4bd6', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('8983', 'steam:11000010c2b4bd6', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('8984', 'steam:11000010c2b4bd6', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('8985', 'steam:11000010c2b4bd6', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('8986', 'steam:11000010c2b4bd6', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('8987', 'steam:11000010c2b4bd6', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('8988', 'steam:11000010c2b4bd6', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('8989', 'steam:11000010c2b4bd6', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('8990', 'steam:11000010c2b4bd6', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('8991', 'steam:11000010c2b4bd6', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('8992', 'steam:11000010c2b4bd6', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8993', 'steam:11000010c2b4bd6', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('8994', 'steam:11000010c2b4bd6', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('8995', 'steam:11000010c2b4bd6', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('8996', 'steam:11000010c2b4bd6', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('8997', 'steam:11000010c2b4bd6', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('8998', 'steam:11000010c2b4bd6', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('8999', 'steam:11000010c2b4bd6', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9000', 'steam:11000010c2b4bd6', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('9001', 'steam:11000010c2b4bd6', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('9002', 'steam:11000010c2b4bd6', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('9003', 'steam:11000010c2b4bd6', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('9004', 'steam:11000010c2b4bd6', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('9005', 'steam:11000010c2b4bd6', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('9006', 'steam:11000010c2b4bd6', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9007', 'steam:11000010c2b4bd6', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('9008', 'steam:11000010c2b4bd6', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('9009', 'steam:11000010c2b4bd6', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('9010', 'steam:11000010c2b4bd6', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('9011', 'steam:11000010c2b4bd6', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('9012', 'steam:11000010c2b4bd6', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('9013', 'steam:11000010c2b4bd6', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('9014', 'steam:11000010c2b4bd6', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('9015', 'steam:11000010c2b4bd6', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9016', 'steam:11000010c2b4bd6', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('9017', 'steam:11000010c2b4bd6', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9018', 'steam:11000010c2b4bd6', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('9019', 'steam:11000010c2b4bd6', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('9020', 'steam:11000010c2b4bd6', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('9021', 'steam:11000010c2b4bd6', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('9022', 'steam:11000010c2b4bd6', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('9023', 'steam:11000010c2b4bd6', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('9024', 'steam:11000010c2b4bd6', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('9025', 'steam:11000010c2b4bd6', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9026', 'steam:11000010c2b4bd6', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('9027', 'steam:11000010c2b4bd6', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('9028', 'steam:11000010c2b4bd6', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9029', 'steam:11000010c2b4bd6', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('9030', 'steam:11000010c2b4bd6', 'coke', '18');
INSERT INTO `user_inventory` VALUES ('9031', 'steam:11000010c2b4bd6', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('9032', 'steam:11000010c2b4bd6', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('9033', 'steam:11000010c2b4bd6', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('9034', 'steam:11000010c2b4bd6', 'clip', '0');
INSERT INTO `user_inventory` VALUES ('9035', 'steam:11000010c2b4bd6', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('9036', 'steam:11000010c2b4bd6', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('9037', 'steam:11000010c2b4bd6', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('9038', 'steam:11000010c2b4bd6', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9039', 'steam:11000010c2b4bd6', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('9040', 'steam:11000010c2b4bd6', 'acetone', '22');
INSERT INTO `user_inventory` VALUES ('9041', 'steam:11000010c2b4bd6', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9042', 'steam:11000010c2b4bd6', 'weed_pooch', '1');
INSERT INTO `user_inventory` VALUES ('9043', 'steam:11000010c2b4bd6', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('9044', 'steam:11000010c2b4bd6', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('9045', 'steam:11000010c2b4bd6', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('9046', 'steam:11000010c2b4bd6', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('9047', 'steam:11000010c2b4bd6', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('9048', 'steam:11000010c2b4bd6', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9049', 'steam:11000010c2b4bd6', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('9050', 'steam:11000010c2b4bd6', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9051', 'steam:11000010c2b4bd6', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('9052', 'steam:11000010c2b4bd6', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('9053', 'steam:11000010c2b4bd6', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9054', 'steam:11000010c2b4bd6', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9055', 'steam:11000010c2b4bd6', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('9056', 'steam:110000117fd8bdf', 'lithium', '0');
INSERT INTO `user_inventory` VALUES ('9057', 'steam:110000117fd8bdf', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('9058', 'steam:110000117fd8bdf', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('9059', 'steam:110000117fd8bdf', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('9060', 'steam:110000117fd8bdf', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9061', 'steam:110000117fd8bdf', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9062', 'steam:110000117fd8bdf', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('9063', 'steam:110000117fd8bdf', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('9064', 'steam:110000117fd8bdf', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('9065', 'steam:110000117fd8bdf', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('9066', 'steam:110000117fd8bdf', 'knife', '0');
INSERT INTO `user_inventory` VALUES ('9067', 'steam:110000117fd8bdf', 'coke', '0');
INSERT INTO `user_inventory` VALUES ('9068', 'steam:110000117fd8bdf', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('9069', 'steam:110000117fd8bdf', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('9070', 'steam:110000117fd8bdf', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('9071', 'steam:110000117fd8bdf', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('9072', 'steam:110000117fd8bdf', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('9073', 'steam:110000117fd8bdf', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('9074', 'steam:110000117fd8bdf', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('9075', 'steam:110000117fd8bdf', 'crack', '0');
INSERT INTO `user_inventory` VALUES ('9076', 'steam:110000117fd8bdf', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('9077', 'steam:110000117fd8bdf', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('9078', 'steam:110000117fd8bdf', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('9079', 'steam:110000117fd8bdf', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9080', 'steam:110000117fd8bdf', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('9081', 'steam:110000117fd8bdf', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('9082', 'steam:110000117fd8bdf', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('9083', 'steam:110000117fd8bdf', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('9084', 'steam:110000117fd8bdf', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('9085', 'steam:110000117fd8bdf', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('9086', 'steam:110000117fd8bdf', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('9087', 'steam:110000117fd8bdf', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9088', 'steam:110000117fd8bdf', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('9089', 'steam:110000117fd8bdf', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9090', 'steam:110000117fd8bdf', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('9091', 'steam:110000117fd8bdf', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('9092', 'steam:110000117fd8bdf', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('9093', 'steam:110000117fd8bdf', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('9094', 'steam:110000117fd8bdf', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9095', 'steam:110000117fd8bdf', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9096', 'steam:110000117fd8bdf', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('9097', 'steam:110000117fd8bdf', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('9098', 'steam:110000117fd8bdf', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('9099', 'steam:110000117fd8bdf', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('9100', 'steam:110000117fd8bdf', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9101', 'steam:110000117fd8bdf', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('9102', 'steam:110000117fd8bdf', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('9103', 'steam:110000117fd8bdf', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('9104', 'steam:110000117fd8bdf', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('9105', 'steam:110000117fd8bdf', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('9106', 'steam:110000117fd8bdf', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('9107', 'steam:110000117fd8bdf', 'coke_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9108', 'steam:110000117fd8bdf', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9109', 'steam:110000117fd8bdf', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('9110', 'steam:110000117fd8bdf', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('9111', 'steam:110000117fd8bdf', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('9112', 'steam:110000117fd8bdf', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('9113', 'steam:110000117fd8bdf', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9114', 'steam:110000117fd8bdf', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('9115', 'steam:110000117fd8bdf', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9116', 'steam:110000117fd8bdf', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9117', 'steam:110000117fd8bdf', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('9118', 'steam:110000117fd8bdf', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('9119', 'steam:110000117fd8bdf', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('9120', 'steam:110000117fd8bdf', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('9121', 'steam:110000117fd8bdf', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9122', 'steam:110000117fd8bdf', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('9123', 'steam:110000117fd8bdf', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('9124', 'steam:110000117fd8bdf', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('9125', 'steam:110000117fd8bdf', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('9126', 'steam:110000117fd8bdf', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9127', 'steam:110000117fd8bdf', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('9128', 'steam:110000117fd8bdf', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9129', 'steam:110000117fd8bdf', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('9130', 'steam:110000117fd8bdf', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('9131', 'steam:110000117fd8bdf', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9132', 'steam:110000117fd8bdf', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('9133', 'steam:110000117fd8bdf', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9134', 'steam:110000117fd8bdf', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('9135', 'steam:110000117fd8bdf', 'water', '15');
INSERT INTO `user_inventory` VALUES ('9136', 'steam:110000117fd8bdf', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('9137', 'steam:110000117fd8bdf', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('9138', 'steam:110000117fd8bdf', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('9139', 'steam:110000117fd8bdf', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('9140', 'steam:110000117fd8bdf', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9141', 'steam:110000117fd8bdf', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('9142', 'steam:110000117fd8bdf', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9143', 'steam:110000117fd8bdf', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9144', 'steam:110000117fd8bdf', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9145', 'steam:110000117fd8bdf', 'meth_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9146', 'steam:110000117fd8bdf', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('9147', 'steam:110000117fd8bdf', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('9148', 'steam:110000117fd8bdf', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9149', 'steam:110000117fd8bdf', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('9150', 'steam:110000117fd8bdf', 'clip', '0');
INSERT INTO `user_inventory` VALUES ('9151', 'steam:110000117fd8bdf', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('9152', 'steam:110000117fd8bdf', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('9153', 'steam:110000117fd8bdf', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9154', 'steam:110000117fd8bdf', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('9155', 'steam:110000117fd8bdf', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('9156', 'steam:110000117fd8bdf', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('9157', 'steam:110000117fd8bdf', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9158', 'steam:110000117fd8bdf', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9159', 'steam:110000117fd8bdf', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9160', 'steam:110000117fd8bdf', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('9161', 'steam:110000117fd8bdf', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('9162', 'steam:110000117fd8bdf', 'acetone', '0');
INSERT INTO `user_inventory` VALUES ('9163', 'steam:110000117fd8bdf', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9164', 'steam:110000117fd8bdf', 'weed', '0');
INSERT INTO `user_inventory` VALUES ('9165', 'steam:110000117fd8bdf', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('9166', 'steam:110000117fd8bdf', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('9167', 'steam:110000117fd8bdf', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9168', 'steam:110000117fd8bdf', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('9169', 'steam:110000117fd8bdf', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('9170', 'steam:110000117fd8bdf', 'weed_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9171', 'steam:110000117fd8bdf', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('9172', 'steam:110000117fd8bdf', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('9173', 'steam:110000117fd8bdf', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('9174', 'steam:110000117fd8bdf', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('9175', 'steam:110000117fd8bdf', 'rifle_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9176', 'steam:110000117fd8bdf', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('9177', 'steam:110000117fd8bdf', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9178', 'steam:110000117fd8bdf', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('9179', 'steam:110000117fd8bdf', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('9180', 'steam:110000117fd8bdf', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('9181', 'steam:110000117fd8bdf', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('9182', 'steam:110000117fd8bdf', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('9183', 'steam:110000117fd8bdf', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9184', 'steam:110000117fd8bdf', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9185', 'steam:110000117fd8bdf', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9186', 'steam:110000117fd8bdf', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('9187', 'steam:110000117fd8bdf', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('9188', 'steam:110000117fd8bdf', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('9189', 'steam:110000117fd8bdf', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9190', 'steam:110000117fd8bdf', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('9191', 'steam:110000117fd8bdf', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('9192', 'steam:110000117fd8bdf', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('9193', 'steam:110000117fd8bdf', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('9194', 'steam:110000117fd8bdf', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('9195', 'steam:110000117fd8bdf', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('9196', 'steam:110000117fd8bdf', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9197', 'steam:110000117fd8bdf', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('9198', 'steam:110000117fd8bdf', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9199', 'steam:110000117fd8bdf', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('9200', 'steam:110000117fd8bdf', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('9201', 'steam:110000117fd8bdf', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('9202', 'steam:110000117fd8bdf', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('9203', 'steam:110000117fd8bdf', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('9204', 'steam:110000117fd8bdf', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('9205', 'steam:110000117fd8bdf', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('9206', 'steam:110000117fd8bdf', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9207', 'steam:110000117fd8bdf', 'carbon', '0');
INSERT INTO `user_inventory` VALUES ('9208', 'steam:110000117fd8bdf', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('9209', 'steam:110000117fd8bdf', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('9210', 'steam:110000117fd8bdf', 'lockpick', '0');
INSERT INTO `user_inventory` VALUES ('9211', 'steam:110000117fd8bdf', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('9212', 'steam:110000117fd8bdf', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('9213', 'steam:110000117fd8bdf', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('9214', 'steam:110000117fd8bdf', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('9215', 'steam:110000117fd8bdf', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9216', 'steam:110000117fd8bdf', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9217', 'steam:110000117fd8bdf', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9218', 'steam:110000117fd8bdf', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('9219', 'steam:110000117fd8bdf', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('9220', 'steam:110000117fd8bdf', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('9221', 'steam:110000117fd8bdf', 'pistol_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9222', 'steam:110000117fd8bdf', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9223', 'steam:110000117fd8bdf', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('9224', 'steam:110000117fd8bdf', 'sim', '0');
INSERT INTO `user_inventory` VALUES ('9225', 'steam:110000117fd8bdf', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('9226', 'steam:110000117fd8bdf', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('9227', 'steam:110000117fd8bdf', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('9228', 'steam:110000117fd8bdf', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9229', 'steam:110000117fd8bdf', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('9230', 'steam:110000117fd8bdf', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('9231', 'steam:110000117fd8bdf', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('9232', 'steam:110000117fd8bdf', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('9233', 'steam:110000117fd8bdf', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9234', 'steam:110000117fd8bdf', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('9235', 'steam:110000117fd8bdf', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('9236', 'steam:110000117fd8bdf', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('9237', 'steam:110000117fd8bdf', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9238', 'steam:110000117fd8bdf', 'poudre', '0');
INSERT INTO `user_inventory` VALUES ('9239', 'steam:110000117fd8bdf', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9240', 'steam:110000117fd8bdf', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('9241', 'steam:110000117fd8bdf', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9242', 'steam:110000117fd8bdf', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9243', 'steam:110000117fd8bdf', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('9244', 'steam:110000117fd8bdf', 'medikit', '0');
INSERT INTO `user_inventory` VALUES ('9245', 'steam:110000117fd8bdf', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('9246', 'steam:110000117fd8bdf', 'bread', '4');
INSERT INTO `user_inventory` VALUES ('9247', 'steam:110000117fd8bdf', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('9248', 'steam:110000117fd8bdf', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('9249', 'steam:110000117fd8bdf', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('9250', 'steam:110000117fd8bdf', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9251', 'steam:110000117fd8bdf', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('9252', 'steam:110000117fd8bdf', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9253', 'steam:110000117fd8bdf', 'fixtool', '0');
INSERT INTO `user_inventory` VALUES ('9254', 'steam:110000117fd8bdf', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('9255', 'steam:110000117fd8bdf', 'acier', '0');
INSERT INTO `user_inventory` VALUES ('9256', 'steam:110000117fd8bdf', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('9257', 'steam:110000117fd8bdf', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('9258', 'steam:110000117fd8bdf', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('9259', 'steam:110000117fd8bdf', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('9260', 'steam:110000117fd8bdf', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('9261', 'steam:110000117fd8bdf', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9262', 'steam:110000117fd8bdf', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('9263', 'steam:110000117fd8bdf', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('9264', 'steam:110000117fd8bdf', 'bat', '0');
INSERT INTO `user_inventory` VALUES ('9265', 'steam:110000117fd8bdf', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('9266', 'steam:110000117fd8bdf', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('9267', 'steam:110000117fd8bdf', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('9268', 'steam:110000117fd8bdf', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('9269', 'steam:110000117fd8bdf', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('9270', 'steam:110000117fd8bdf', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('9271', 'steam:110000117fd8bdf', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('9272', 'steam:110000117fd8bdf', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('9273', 'steam:110000117fd8bdf', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('9274', 'steam:110000117fd8bdf', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('9275', 'steam:110000117fd8bdf', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('9276', 'steam:110000117fd8bdf', 'methlab', '0');
INSERT INTO `user_inventory` VALUES ('9277', 'steam:110000117fd8bdf', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('9278', 'steam:110000117fd8bdf', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('9279', 'steam:110000117fd8bdf', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9280', 'steam:110000117fd8bdf', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('9281', 'steam:110000117fd8bdf', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('9282', 'steam:110000117fd8bdf', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9283', 'steam:110000117fd8bdf', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9284', 'steam:110000117fd8bdf', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('9285', 'steam:110000117fd8bdf', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9286', 'steam:110000117fd8bdf', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9287', 'steam:110000117fd8bdf', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('9288', 'steam:110000117fd8bdf', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9289', 'steam:110000117fd8bdf', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('9290', 'steam:110000117fd8bdf', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('9291', 'steam:11000010f9264c0', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('9292', 'steam:11000010f9264c0', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('9293', 'steam:11000010f9264c0', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9294', 'steam:11000010f9264c0', 'coke_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9295', 'steam:11000010f9264c0', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('9296', 'steam:11000010f9264c0', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('9297', 'steam:11000010f9264c0', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9298', 'steam:11000010f9264c0', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('9299', 'steam:11000010f9264c0', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('9300', 'steam:11000010f9264c0', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('9301', 'steam:11000010f9264c0', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9302', 'steam:11000010f9264c0', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9303', 'steam:11000010f9264c0', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9304', 'steam:11000010f9264c0', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('9305', 'steam:11000010f9264c0', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('9306', 'steam:11000010f9264c0', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9307', 'steam:11000010f9264c0', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9308', 'steam:11000010f9264c0', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('9309', 'steam:11000010f9264c0', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('9310', 'steam:11000010f9264c0', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('9311', 'steam:11000010f9264c0', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('9312', 'steam:11000010f9264c0', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9313', 'steam:11000010f9264c0', 'fixtool', '0');
INSERT INTO `user_inventory` VALUES ('9314', 'steam:11000010f9264c0', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('9315', 'steam:11000010f9264c0', 'crack', '0');
INSERT INTO `user_inventory` VALUES ('9316', 'steam:11000010f9264c0', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9317', 'steam:11000010f9264c0', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('9318', 'steam:11000010f9264c0', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('9319', 'steam:11000010f9264c0', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9320', 'steam:11000010f9264c0', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('9321', 'steam:11000010f9264c0', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('9322', 'steam:11000010f9264c0', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('9323', 'steam:11000010f9264c0', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('9324', 'steam:11000010f9264c0', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9325', 'steam:11000010f9264c0', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('9326', 'steam:11000010f9264c0', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('9327', 'steam:11000010f9264c0', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('9328', 'steam:11000010f9264c0', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('9329', 'steam:11000010f9264c0', 'weed_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9330', 'steam:11000010f9264c0', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('9331', 'steam:11000010f9264c0', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9332', 'steam:11000010f9264c0', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9333', 'steam:11000010f9264c0', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('9334', 'steam:11000010f9264c0', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('9335', 'steam:11000010f9264c0', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('9336', 'steam:11000010f9264c0', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9337', 'steam:11000010f9264c0', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9338', 'steam:11000010f9264c0', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('9339', 'steam:11000010f9264c0', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('9340', 'steam:11000010f9264c0', 'bat', '0');
INSERT INTO `user_inventory` VALUES ('9341', 'steam:11000010f9264c0', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('9342', 'steam:11000010f9264c0', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9343', 'steam:11000010f9264c0', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('9344', 'steam:11000010f9264c0', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9345', 'steam:11000010f9264c0', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('9346', 'steam:11000010f9264c0', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('9347', 'steam:11000010f9264c0', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9348', 'steam:11000010f9264c0', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('9349', 'steam:11000010f9264c0', 'lithium', '0');
INSERT INTO `user_inventory` VALUES ('9350', 'steam:11000010f9264c0', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('9351', 'steam:11000010f9264c0', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9352', 'steam:11000010f9264c0', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9353', 'steam:11000010f9264c0', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('9354', 'steam:11000010f9264c0', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('9355', 'steam:11000010f9264c0', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9356', 'steam:11000010f9264c0', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9357', 'steam:11000010f9264c0', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('9358', 'steam:11000010f9264c0', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('9359', 'steam:11000010f9264c0', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('9360', 'steam:11000010f9264c0', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('9361', 'steam:11000010f9264c0', 'medikit', '0');
INSERT INTO `user_inventory` VALUES ('9362', 'steam:11000010f9264c0', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9363', 'steam:11000010f9264c0', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('9364', 'steam:11000010f9264c0', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('9365', 'steam:11000010f9264c0', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('9366', 'steam:11000010f9264c0', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9367', 'steam:11000010f9264c0', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('9368', 'steam:11000010f9264c0', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('9369', 'steam:11000010f9264c0', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('9370', 'steam:11000010f9264c0', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9371', 'steam:11000010f9264c0', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9372', 'steam:11000010f9264c0', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9373', 'steam:11000010f9264c0', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9374', 'steam:11000010f9264c0', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9375', 'steam:11000010f9264c0', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9376', 'steam:11000010f9264c0', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('9377', 'steam:11000010f9264c0', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('9378', 'steam:11000010f9264c0', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('9379', 'steam:11000010f9264c0', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('9380', 'steam:11000010f9264c0', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('9381', 'steam:11000010f9264c0', 'rifle_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9382', 'steam:11000010f9264c0', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('9383', 'steam:11000010f9264c0', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('9384', 'steam:11000010f9264c0', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('9385', 'steam:11000010f9264c0', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('9386', 'steam:11000010f9264c0', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('9387', 'steam:11000010f9264c0', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9388', 'steam:11000010f9264c0', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9389', 'steam:11000010f9264c0', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9390', 'steam:11000010f9264c0', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('9391', 'steam:11000010f9264c0', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9392', 'steam:11000010f9264c0', 'coke', '0');
INSERT INTO `user_inventory` VALUES ('9393', 'steam:11000010f9264c0', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('9394', 'steam:11000010f9264c0', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('9395', 'steam:11000010f9264c0', 'sim', '0');
INSERT INTO `user_inventory` VALUES ('9396', 'steam:11000010f9264c0', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('9397', 'steam:11000010f9264c0', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('9398', 'steam:11000010f9264c0', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('9399', 'steam:11000010f9264c0', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('9400', 'steam:11000010f9264c0', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('9401', 'steam:11000010f9264c0', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9402', 'steam:11000010f9264c0', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('9403', 'steam:11000010f9264c0', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('9404', 'steam:11000010f9264c0', 'pistol_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9405', 'steam:11000010f9264c0', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('9406', 'steam:11000010f9264c0', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('9407', 'steam:11000010f9264c0', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9408', 'steam:11000010f9264c0', 'acetone', '0');
INSERT INTO `user_inventory` VALUES ('9409', 'steam:11000010f9264c0', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('9410', 'steam:11000010f9264c0', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('9411', 'steam:11000010f9264c0', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('9412', 'steam:11000010f9264c0', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('9413', 'steam:11000010f9264c0', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('9414', 'steam:11000010f9264c0', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('9415', 'steam:11000010f9264c0', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('9416', 'steam:11000010f9264c0', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9417', 'steam:11000010f9264c0', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9418', 'steam:11000010f9264c0', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('9419', 'steam:11000010f9264c0', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('9420', 'steam:11000010f9264c0', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9421', 'steam:11000010f9264c0', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('9422', 'steam:11000010f9264c0', 'meth_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9423', 'steam:11000010f9264c0', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('9424', 'steam:11000010f9264c0', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('9425', 'steam:11000010f9264c0', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9426', 'steam:11000010f9264c0', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('9427', 'steam:11000010f9264c0', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('9428', 'steam:11000010f9264c0', 'poudre', '0');
INSERT INTO `user_inventory` VALUES ('9429', 'steam:11000010f9264c0', 'water', '0');
INSERT INTO `user_inventory` VALUES ('9430', 'steam:11000010f9264c0', 'methlab', '0');
INSERT INTO `user_inventory` VALUES ('9431', 'steam:11000010f9264c0', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('9432', 'steam:11000010f9264c0', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9433', 'steam:11000010f9264c0', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9434', 'steam:11000010f9264c0', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('9435', 'steam:11000010f9264c0', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('9436', 'steam:11000010f9264c0', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('9437', 'steam:11000010f9264c0', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9438', 'steam:11000010f9264c0', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('9439', 'steam:11000010f9264c0', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('9440', 'steam:11000010f9264c0', 'bread', '0');
INSERT INTO `user_inventory` VALUES ('9441', 'steam:11000010f9264c0', 'clip', '0');
INSERT INTO `user_inventory` VALUES ('9442', 'steam:11000010f9264c0', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('9443', 'steam:11000010f9264c0', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9444', 'steam:11000010f9264c0', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('9445', 'steam:11000010f9264c0', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('9446', 'steam:11000010f9264c0', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('9447', 'steam:11000010f9264c0', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('9448', 'steam:11000010f9264c0', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('9449', 'steam:11000010f9264c0', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('9450', 'steam:11000010f9264c0', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('9451', 'steam:11000010f9264c0', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('9452', 'steam:11000010f9264c0', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('9453', 'steam:11000010f9264c0', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('9454', 'steam:11000010f9264c0', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9455', 'steam:11000010f9264c0', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('9456', 'steam:11000010f9264c0', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('9457', 'steam:11000010f9264c0', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('9458', 'steam:11000010f9264c0', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('9459', 'steam:11000010f9264c0', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9460', 'steam:11000010f9264c0', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('9461', 'steam:11000010f9264c0', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('9462', 'steam:11000010f9264c0', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('9463', 'steam:11000010f9264c0', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('9464', 'steam:11000010f9264c0', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('9465', 'steam:11000010f9264c0', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('9466', 'steam:11000010f9264c0', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('9467', 'steam:11000010f9264c0', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('9468', 'steam:11000010f9264c0', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('9469', 'steam:11000010f9264c0', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('9470', 'steam:11000010f9264c0', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('9471', 'steam:11000010f9264c0', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('9472', 'steam:11000010f9264c0', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('9473', 'steam:11000010f9264c0', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9474', 'steam:11000010f9264c0', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('9475', 'steam:11000010f9264c0', 'weed', '0');
INSERT INTO `user_inventory` VALUES ('9476', 'steam:11000010f9264c0', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('9477', 'steam:11000010f9264c0', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('9478', 'steam:11000010f9264c0', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('9479', 'steam:11000010f9264c0', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('9480', 'steam:11000010f9264c0', 'carbon', '0');
INSERT INTO `user_inventory` VALUES ('9481', 'steam:11000010f9264c0', 'lockpick', '0');
INSERT INTO `user_inventory` VALUES ('9482', 'steam:11000010f9264c0', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('9483', 'steam:11000010f9264c0', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('9484', 'steam:11000010f9264c0', 'knife', '0');
INSERT INTO `user_inventory` VALUES ('9485', 'steam:11000010f9264c0', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9486', 'steam:11000010f9264c0', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('9487', 'steam:11000010f9264c0', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('9488', 'steam:11000010f9264c0', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('9489', 'steam:11000010f9264c0', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('9490', 'steam:11000010f9264c0', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('9491', 'steam:11000010f9264c0', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9492', 'steam:11000010f9264c0', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('9493', 'steam:11000010f9264c0', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('9494', 'steam:11000010f9264c0', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('9495', 'steam:11000010f9264c0', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('9496', 'steam:11000010f9264c0', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('9497', 'steam:11000010f9264c0', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('9498', 'steam:11000010f9264c0', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('9499', 'steam:11000010f9264c0', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9500', 'steam:11000010f9264c0', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('9501', 'steam:11000010f9264c0', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('9502', 'steam:11000010f9264c0', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('9503', 'steam:11000010f9264c0', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('9504', 'steam:11000010f9264c0', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('9505', 'steam:11000010f9264c0', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9506', 'steam:11000010f9264c0', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('9507', 'steam:11000010f9264c0', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('9508', 'steam:11000010f9264c0', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('9509', 'steam:11000010f9264c0', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('9510', 'steam:11000010f9264c0', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('9511', 'steam:11000010f9264c0', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('9512', 'steam:11000010f9264c0', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9513', 'steam:11000010f9264c0', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('9514', 'steam:11000010f9264c0', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('9515', 'steam:11000010f9264c0', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('9516', 'steam:11000010f9264c0', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('9517', 'steam:11000010f9264c0', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('9518', 'steam:11000010f9264c0', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9519', 'steam:11000010f9264c0', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('9520', 'steam:11000010f9264c0', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9521', 'steam:11000010f9264c0', 'acier', '0');
INSERT INTO `user_inventory` VALUES ('9522', 'steam:11000010f9264c0', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('9523', 'steam:11000010f9264c0', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('9524', 'steam:11000010f9264c0', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9525', 'steam:11000010f9264c0', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('9526', 'steam:110000105e314d0', 'tel', '0');
INSERT INTO `user_inventory` VALUES ('9527', 'steam:110000105e314d0', 'bankidcard', '0');
INSERT INTO `user_inventory` VALUES ('9528', 'steam:110000105e314d0', 'smg', '0');
INSERT INTO `user_inventory` VALUES ('9529', 'steam:110000105e314d0', 'grand_cru', '0');
INSERT INTO `user_inventory` VALUES ('9530', 'steam:110000105e314d0', 'bandage', '0');
INSERT INTO `user_inventory` VALUES ('9531', 'steam:110000105e314d0', 'meth_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9532', 'steam:110000105e314d0', 'dnaanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9533', 'steam:110000105e314d0', 'esctasy', '0');
INSERT INTO `user_inventory` VALUES ('9534', 'steam:110000105e314d0', 'sniperrifle', '0');
INSERT INTO `user_inventory` VALUES ('9535', 'steam:110000105e314d0', 'nightvision', '0');
INSERT INTO `user_inventory` VALUES ('9536', 'steam:110000105e314d0', 'flaregun', '0');
INSERT INTO `user_inventory` VALUES ('9537', 'steam:110000105e314d0', 'heavypistol', '0');
INSERT INTO `user_inventory` VALUES ('9538', 'steam:110000105e314d0', 'carbinerifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9539', 'steam:110000105e314d0', 'wood', '0');
INSERT INTO `user_inventory` VALUES ('9540', 'steam:110000105e314d0', 'drpepper', '0');
INSERT INTO `user_inventory` VALUES ('9541', 'steam:110000105e314d0', 'pistol_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9542', 'steam:110000105e314d0', 'bolcacahuetes', '0');
INSERT INTO `user_inventory` VALUES ('9543', 'steam:110000105e314d0', 'knife', '0');
INSERT INTO `user_inventory` VALUES ('9544', 'steam:110000105e314d0', 'assaultrifle', '0');
INSERT INTO `user_inventory` VALUES ('9545', 'steam:110000105e314d0', 'autoshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9546', 'steam:110000105e314d0', 'lithium', '0');
INSERT INTO `user_inventory` VALUES ('9547', 'steam:110000105e314d0', 'blowpipe', '0');
INSERT INTO `user_inventory` VALUES ('9548', 'steam:110000105e314d0', 'crack_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9549', 'steam:110000105e314d0', 'grenadelauncher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9550', 'steam:110000105e314d0', 'grenadelauncher_smoke_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9551', 'steam:110000105e314d0', 'dagger', '0');
INSERT INTO `user_inventory` VALUES ('9552', 'steam:110000105e314d0', 'hammer', '0');
INSERT INTO `user_inventory` VALUES ('9553', 'steam:110000105e314d0', 'wrench', '0');
INSERT INTO `user_inventory` VALUES ('9554', 'steam:110000105e314d0', 'grenadelauncher', '0');
INSERT INTO `user_inventory` VALUES ('9555', 'steam:110000105e314d0', 'whisky', '0');
INSERT INTO `user_inventory` VALUES ('9556', 'steam:110000105e314d0', 'yusuf', '0');
INSERT INTO `user_inventory` VALUES ('9557', 'steam:110000105e314d0', 'acier', '0');
INSERT INTO `user_inventory` VALUES ('9558', 'steam:110000105e314d0', 'fish', '0');
INSERT INTO `user_inventory` VALUES ('9559', 'steam:110000105e314d0', 'bolnoixcajou', '0');
INSERT INTO `user_inventory` VALUES ('9560', 'steam:110000105e314d0', 'grip', '0');
INSERT INTO `user_inventory` VALUES ('9561', 'steam:110000105e314d0', 'wool', '0');
INSERT INTO `user_inventory` VALUES ('9562', 'steam:110000105e314d0', 'snspistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9563', 'steam:110000105e314d0', 'bullpuprifle', '0');
INSERT INTO `user_inventory` VALUES ('9564', 'steam:110000105e314d0', 'bolpistache', '0');
INSERT INTO `user_inventory` VALUES ('9565', 'steam:110000105e314d0', 'sacbillets', '0');
INSERT INTO `user_inventory` VALUES ('9566', 'steam:110000105e314d0', 'hifi', '0');
INSERT INTO `user_inventory` VALUES ('9567', 'steam:110000105e314d0', 'medikit', '0');
INSERT INTO `user_inventory` VALUES ('9568', 'steam:110000105e314d0', 'alcool_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9569', 'steam:110000105e314d0', 'ecstasy', '0');
INSERT INTO `user_inventory` VALUES ('9570', 'steam:110000105e314d0', 'ice', '0');
INSERT INTO `user_inventory` VALUES ('9571', 'steam:110000105e314d0', 'minismg', '0');
INSERT INTO `user_inventory` VALUES ('9572', 'steam:110000105e314d0', 'whiskycoca', '0');
INSERT INTO `user_inventory` VALUES ('9573', 'steam:110000105e314d0', 'myrtealcool', '0');
INSERT INTO `user_inventory` VALUES ('9574', 'steam:110000105e314d0', 'cutted_wood', '0');
INSERT INTO `user_inventory` VALUES ('9575', 'steam:110000105e314d0', 'jagercerbere', '0');
INSERT INTO `user_inventory` VALUES ('9576', 'steam:110000105e314d0', 'minigun', '0');
INSERT INTO `user_inventory` VALUES ('9577', 'steam:110000105e314d0', 'redbull', '0');
INSERT INTO `user_inventory` VALUES ('9578', 'steam:110000105e314d0', 'gzgas_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9579', 'steam:110000105e314d0', 'iron', '0');
INSERT INTO `user_inventory` VALUES ('9580', 'steam:110000105e314d0', 'whiskycoc', '0');
INSERT INTO `user_inventory` VALUES ('9581', 'steam:110000105e314d0', 'rpg', '0');
INSERT INTO `user_inventory` VALUES ('9582', 'steam:110000105e314d0', 'washed_stone', '0');
INSERT INTO `user_inventory` VALUES ('9583', 'steam:110000105e314d0', 'weed', '0');
INSERT INTO `user_inventory` VALUES ('9584', 'steam:110000105e314d0', 'combatmg', '0');
INSERT INTO `user_inventory` VALUES ('9585', 'steam:110000105e314d0', 'water', '0');
INSERT INTO `user_inventory` VALUES ('9586', 'steam:110000105e314d0', 'weed_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9587', 'steam:110000105e314d0', 'vodkafruit', '0');
INSERT INTO `user_inventory` VALUES ('9588', 'steam:110000105e314d0', 'machete', '0');
INSERT INTO `user_inventory` VALUES ('9589', 'steam:110000105e314d0', 'hatchet', '0');
INSERT INTO `user_inventory` VALUES ('9590', 'steam:110000105e314d0', 'slaughtered_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9591', 'steam:110000105e314d0', 'meth', '0');
INSERT INTO `user_inventory` VALUES ('9592', 'steam:110000105e314d0', 'marksmanrifle', '0');
INSERT INTO `user_inventory` VALUES ('9593', 'steam:110000105e314d0', 'bread', '0');
INSERT INTO `user_inventory` VALUES ('9594', 'steam:110000105e314d0', 'vodka', '0');
INSERT INTO `user_inventory` VALUES ('9595', 'steam:110000105e314d0', 'alive_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9596', 'steam:110000105e314d0', 'bzgas', '0');
INSERT INTO `user_inventory` VALUES ('9597', 'steam:110000105e314d0', 'vintagepistol', '0');
INSERT INTO `user_inventory` VALUES ('9598', 'steam:110000105e314d0', 'tequila', '0');
INSERT INTO `user_inventory` VALUES ('9599', 'steam:110000105e314d0', 'bullpuprifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9600', 'steam:110000105e314d0', 'heavysniper', '0');
INSERT INTO `user_inventory` VALUES ('9601', 'steam:110000105e314d0', 'bulletsample', '0');
INSERT INTO `user_inventory` VALUES ('9602', 'steam:110000105e314d0', 'crowbar', '0');
INSERT INTO `user_inventory` VALUES ('9603', 'steam:110000105e314d0', 'remotesniper', '0');
INSERT INTO `user_inventory` VALUES ('9604', 'steam:110000105e314d0', 'doubleaction', '0');
INSERT INTO `user_inventory` VALUES ('9605', 'steam:110000105e314d0', 'martini', '0');
INSERT INTO `user_inventory` VALUES ('9606', 'steam:110000105e314d0', 'golem', '0');
INSERT INTO `user_inventory` VALUES ('9607', 'steam:110000105e314d0', 'assaultshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9608', 'steam:110000105e314d0', 'combatmg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9609', 'steam:110000105e314d0', 'raisin', '0');
INSERT INTO `user_inventory` VALUES ('9610', 'steam:110000105e314d0', 'fixkit', '0');
INSERT INTO `user_inventory` VALUES ('9611', 'steam:110000105e314d0', 'stungun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9612', 'steam:110000105e314d0', 'pumpshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9613', 'steam:110000105e314d0', 'limonade', '0');
INSERT INTO `user_inventory` VALUES ('9614', 'steam:110000105e314d0', 'stungun', '0');
INSERT INTO `user_inventory` VALUES ('9615', 'steam:110000105e314d0', 'switchblade', '0');
INSERT INTO `user_inventory` VALUES ('9616', 'steam:110000105e314d0', 'douille', '0');
INSERT INTO `user_inventory` VALUES ('9617', 'steam:110000105e314d0', 'mojito', '0');
INSERT INTO `user_inventory` VALUES ('9618', 'steam:110000105e314d0', 'bolchips', '0');
INSERT INTO `user_inventory` VALUES ('9619', 'steam:110000105e314d0', 'proxmine', '0');
INSERT INTO `user_inventory` VALUES ('9620', 'steam:110000105e314d0', 'pacicficidcard', '0');
INSERT INTO `user_inventory` VALUES ('9621', 'steam:110000105e314d0', 'redbull_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9622', 'steam:110000105e314d0', 'golfclub', '0');
INSERT INTO `user_inventory` VALUES ('9623', 'steam:110000105e314d0', 'heavysniper_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9624', 'steam:110000105e314d0', 'gazbottle', '0');
INSERT INTO `user_inventory` VALUES ('9625', 'steam:110000105e314d0', 'stinger', '0');
INSERT INTO `user_inventory` VALUES ('9626', 'steam:110000105e314d0', 'packaged_chicken', '0');
INSERT INTO `user_inventory` VALUES ('9627', 'steam:110000105e314d0', 'specialcarbine_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9628', 'steam:110000105e314d0', 'stickybomb_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9629', 'steam:110000105e314d0', 'snowball', '0');
INSERT INTO `user_inventory` VALUES ('9630', 'steam:110000105e314d0', 'musket', '0');
INSERT INTO `user_inventory` VALUES ('9631', 'steam:110000105e314d0', 'copper', '0');
INSERT INTO `user_inventory` VALUES ('9632', 'steam:110000105e314d0', 'sim', '0');
INSERT INTO `user_inventory` VALUES ('9633', 'steam:110000105e314d0', 'alcool', '0');
INSERT INTO `user_inventory` VALUES ('9634', 'steam:110000105e314d0', 'revolver', '0');
INSERT INTO `user_inventory` VALUES ('9635', 'steam:110000105e314d0', 'ball', '0');
INSERT INTO `user_inventory` VALUES ('9636', 'steam:110000105e314d0', 'bloodsample', '0');
INSERT INTO `user_inventory` VALUES ('9637', 'steam:110000105e314d0', 'smokegrenade_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9638', 'steam:110000105e314d0', 'teqpaf', '0');
INSERT INTO `user_inventory` VALUES ('9639', 'steam:110000105e314d0', 'sniper_remote_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9640', 'steam:110000105e314d0', 'flashlight', '0');
INSERT INTO `user_inventory` VALUES ('9641', 'steam:110000105e314d0', 'smg_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9642', 'steam:110000105e314d0', 'mg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9643', 'steam:110000105e314d0', 'bird_crap_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9644', 'steam:110000105e314d0', 'smokegrenade', '0');
INSERT INTO `user_inventory` VALUES ('9645', 'steam:110000105e314d0', 'myrte', '0');
INSERT INTO `user_inventory` VALUES ('9646', 'steam:110000105e314d0', 'machinepistol', '0');
INSERT INTO `user_inventory` VALUES ('9647', 'steam:110000105e314d0', 'grapperaisin', '0');
INSERT INTO `user_inventory` VALUES ('9648', 'steam:110000105e314d0', 'jager', '0');
INSERT INTO `user_inventory` VALUES ('9649', 'steam:110000105e314d0', 'cigarette', '0');
INSERT INTO `user_inventory` VALUES ('9650', 'steam:110000105e314d0', 'ketamine', '0');
INSERT INTO `user_inventory` VALUES ('9651', 'steam:110000105e314d0', 'silencieux', '0');
INSERT INTO `user_inventory` VALUES ('9652', 'steam:110000105e314d0', 'combatpdw', '0');
INSERT INTO `user_inventory` VALUES ('9653', 'steam:110000105e314d0', 'smg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9654', 'steam:110000105e314d0', 'space_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9655', 'steam:110000105e314d0', 'carbon', '0');
INSERT INTO `user_inventory` VALUES ('9656', 'steam:110000105e314d0', 'ammoanalyzer', '0');
INSERT INTO `user_inventory` VALUES ('9657', 'steam:110000105e314d0', 'gold', '0');
INSERT INTO `user_inventory` VALUES ('9658', 'steam:110000105e314d0', 'rhumfruit', '0');
INSERT INTO `user_inventory` VALUES ('9659', 'steam:110000105e314d0', 'rifle_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9660', 'steam:110000105e314d0', 'shotgun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9661', 'steam:110000105e314d0', 'jus_raisin', '0');
INSERT INTO `user_inventory` VALUES ('9662', 'steam:110000105e314d0', 'fireextinguisher', '0');
INSERT INTO `user_inventory` VALUES ('9663', 'steam:110000105e314d0', 'plane_rocket_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9664', 'steam:110000105e314d0', 'saucisson', '0');
INSERT INTO `user_inventory` VALUES ('9665', 'steam:110000105e314d0', 'jagerbomb', '0');
INSERT INTO `user_inventory` VALUES ('9666', 'steam:110000105e314d0', 'carokit', '0');
INSERT INTO `user_inventory` VALUES ('9667', 'steam:110000105e314d0', 'methlab', '0');
INSERT INTO `user_inventory` VALUES ('9668', 'steam:110000105e314d0', 'rpg_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9669', 'steam:110000105e314d0', 'flare', '0');
INSERT INTO `user_inventory` VALUES ('9670', 'steam:110000105e314d0', 'vodkrb', '0');
INSERT INTO `user_inventory` VALUES ('9671', 'steam:110000105e314d0', 'fabric', '0');
INSERT INTO `user_inventory` VALUES ('9672', 'steam:110000105e314d0', 'knuckle', '0');
INSERT INTO `user_inventory` VALUES ('9673', 'steam:110000105e314d0', 'coke_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9674', 'steam:110000105e314d0', 'fixtool', '0');
INSERT INTO `user_inventory` VALUES ('9675', 'steam:110000105e314d0', 'bague', '0');
INSERT INTO `user_inventory` VALUES ('9676', 'steam:110000105e314d0', 'grenade', '0');
INSERT INTO `user_inventory` VALUES ('9677', 'steam:110000105e314d0', 'advancedrifle', '0');
INSERT INTO `user_inventory` VALUES ('9678', 'steam:110000105e314d0', 'rhumcoca', '0');
INSERT INTO `user_inventory` VALUES ('9679', 'steam:110000105e314d0', 'metreshooter', '0');
INSERT INTO `user_inventory` VALUES ('9680', 'steam:110000105e314d0', 'rolex', '0');
INSERT INTO `user_inventory` VALUES ('9681', 'steam:110000105e314d0', 'snspistol', '0');
INSERT INTO `user_inventory` VALUES ('9682', 'steam:110000105e314d0', 'sawnoffshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9683', 'steam:110000105e314d0', 'ecstasy_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9684', 'steam:110000105e314d0', 'rhum', '0');
INSERT INTO `user_inventory` VALUES ('9685', 'steam:110000105e314d0', 'player_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9686', 'steam:110000105e314d0', 'pumpshotgun_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9687', 'steam:110000105e314d0', 'poolcue', '0');
INSERT INTO `user_inventory` VALUES ('9688', 'steam:110000105e314d0', 'fireextinguisher_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9689', 'steam:110000105e314d0', 'railgun', '0');
INSERT INTO `user_inventory` VALUES ('9690', 'steam:110000105e314d0', 'compactlauncher', '0');
INSERT INTO `user_inventory` VALUES ('9691', 'steam:110000105e314d0', 'menthe', '0');
INSERT INTO `user_inventory` VALUES ('9692', 'steam:110000105e314d0', 'icetea', '0');
INSERT INTO `user_inventory` VALUES ('9693', 'steam:110000105e314d0', 'revolver_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9694', 'steam:110000105e314d0', 'coke', '0');
INSERT INTO `user_inventory` VALUES ('9695', 'steam:110000105e314d0', 'pistol_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9696', 'steam:110000105e314d0', 'pistol', '0');
INSERT INTO `user_inventory` VALUES ('9697', 'steam:110000105e314d0', 'pistol50', '0');
INSERT INTO `user_inventory` VALUES ('9698', 'steam:110000105e314d0', 'phone', '0');
INSERT INTO `user_inventory` VALUES ('9699', 'steam:110000105e314d0', 'pipebomb', '0');
INSERT INTO `user_inventory` VALUES ('9700', 'steam:110000105e314d0', 'digiscanner', '0');
INSERT INTO `user_inventory` VALUES ('9701', 'steam:110000105e314d0', 'petrol_raffin', '0');
INSERT INTO `user_inventory` VALUES ('9702', 'steam:110000105e314d0', 'petrol', '0');
INSERT INTO `user_inventory` VALUES ('9703', 'steam:110000105e314d0', 'poudre', '0');
INSERT INTO `user_inventory` VALUES ('9704', 'steam:110000105e314d0', 'bulletproof', '0');
INSERT INTO `user_inventory` VALUES ('9705', 'steam:110000105e314d0', 'ball_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9706', 'steam:110000105e314d0', 'oxycutter', '0');
INSERT INTO `user_inventory` VALUES ('9707', 'steam:110000105e314d0', 'petrolcan', '0');
INSERT INTO `user_inventory` VALUES ('9708', 'steam:110000105e314d0', 'minigun_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9709', 'steam:110000105e314d0', 'packaged_plank', '0');
INSERT INTO `user_inventory` VALUES ('9710', 'steam:110000105e314d0', 'parachute', '0');
INSERT INTO `user_inventory` VALUES ('9711', 'steam:110000105e314d0', 'nightstick', '0');
INSERT INTO `user_inventory` VALUES ('9712', 'steam:110000105e314d0', 'compactrifle', '0');
INSERT INTO `user_inventory` VALUES ('9713', 'steam:110000105e314d0', 'dbshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9714', 'steam:110000105e314d0', 'myrte_cargo', '0');
INSERT INTO `user_inventory` VALUES ('9715', 'steam:110000105e314d0', 'stickybomb', '0');
INSERT INTO `user_inventory` VALUES ('9716', 'steam:110000105e314d0', 'stinger_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9717', 'steam:110000105e314d0', 'molotov', '0');
INSERT INTO `user_inventory` VALUES ('9718', 'steam:110000105e314d0', 'battleaxe', '0');
INSERT INTO `user_inventory` VALUES ('9719', 'steam:110000105e314d0', 'firework', '0');
INSERT INTO `user_inventory` VALUES ('9720', 'steam:110000105e314d0', 'mixapero', '0');
INSERT INTO `user_inventory` VALUES ('9721', 'steam:110000105e314d0', 'assaultsmg', '0');
INSERT INTO `user_inventory` VALUES ('9722', 'steam:110000105e314d0', 'flare_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9723', 'steam:110000105e314d0', 'opium', '0');
INSERT INTO `user_inventory` VALUES ('9724', 'steam:110000105e314d0', 'energy', '0');
INSERT INTO `user_inventory` VALUES ('9725', 'steam:110000105e314d0', 'specialcarbine', '0');
INSERT INTO `user_inventory` VALUES ('9726', 'steam:110000105e314d0', 'gusenberg', '0');
INSERT INTO `user_inventory` VALUES ('9727', 'steam:110000105e314d0', 'sniper_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9728', 'steam:110000105e314d0', 'ketamine_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9729', 'steam:110000105e314d0', 'opium_pooch', '0');
INSERT INTO `user_inventory` VALUES ('9730', 'steam:110000105e314d0', 'carbinerifle', '0');
INSERT INTO `user_inventory` VALUES ('9731', 'steam:110000105e314d0', 'molotov_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9732', 'steam:110000105e314d0', 'enemy_laser_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9733', 'steam:110000105e314d0', 'vodkaenergy', '0');
INSERT INTO `user_inventory` VALUES ('9734', 'steam:110000105e314d0', 'clothe', '0');
INSERT INTO `user_inventory` VALUES ('9735', 'steam:110000105e314d0', 'garbagebag', '0');
INSERT INTO `user_inventory` VALUES ('9736', 'steam:110000105e314d0', 'marksmanpistol', '0');
INSERT INTO `user_inventory` VALUES ('9737', 'steam:110000105e314d0', 'microsmg', '0');
INSERT INTO `user_inventory` VALUES ('9738', 'steam:110000105e314d0', 'gps', '0');
INSERT INTO `user_inventory` VALUES ('9739', 'steam:110000105e314d0', 'lockpick', '0');
INSERT INTO `user_inventory` VALUES ('9740', 'steam:110000105e314d0', 'hominglauncher', '0');
INSERT INTO `user_inventory` VALUES ('9741', 'steam:110000105e314d0', 'bullpupshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9742', 'steam:110000105e314d0', 'soda', '0');
INSERT INTO `user_inventory` VALUES ('9743', 'steam:110000105e314d0', 'stone', '0');
INSERT INTO `user_inventory` VALUES ('9744', 'steam:110000105e314d0', 'marksmanrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9745', 'steam:110000105e314d0', 'assaultrifle_mk2', '0');
INSERT INTO `user_inventory` VALUES ('9746', 'steam:110000105e314d0', 'bottle', '0');
INSERT INTO `user_inventory` VALUES ('9747', 'steam:110000105e314d0', 'appistol', '0');
INSERT INTO `user_inventory` VALUES ('9748', 'steam:110000105e314d0', 'jusfruit', '0');
INSERT INTO `user_inventory` VALUES ('9749', 'steam:110000105e314d0', 'essence', '0');
INSERT INTO `user_inventory` VALUES ('9750', 'steam:110000105e314d0', 'crack', '0');
INSERT INTO `user_inventory` VALUES ('9751', 'steam:110000105e314d0', 'heavyshotgun', '0');
INSERT INTO `user_inventory` VALUES ('9752', 'steam:110000105e314d0', 'carotool', '0');
INSERT INTO `user_inventory` VALUES ('9753', 'steam:110000105e314d0', 'vine', '0');
INSERT INTO `user_inventory` VALUES ('9754', 'steam:110000105e314d0', 'mg', '0');
INSERT INTO `user_inventory` VALUES ('9755', 'steam:110000105e314d0', 'tank_ammo', '0');
INSERT INTO `user_inventory` VALUES ('9756', 'steam:110000105e314d0', 'acetone', '0');
INSERT INTO `user_inventory` VALUES ('9757', 'steam:110000105e314d0', 'bat', '0');
INSERT INTO `user_inventory` VALUES ('9758', 'steam:110000105e314d0', 'diamond', '0');
INSERT INTO `user_inventory` VALUES ('9759', 'steam:110000105e314d0', 'combatpistol', '0');
INSERT INTO `user_inventory` VALUES ('9760', 'steam:110000105e314d0', 'clip', '0');

-- ----------------------------
-- Table structure for `user_licenses`
-- ----------------------------
DROP TABLE IF EXISTS `user_licenses`;
CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `owner` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_licenses
-- ----------------------------
INSERT INTO `user_licenses` VALUES ('1', 'weapon', 'steam:110000104c989b7');

-- ----------------------------
-- Table structure for `user_sim`
-- ----------------------------
DROP TABLE IF EXISTS `user_sim`;
CREATE TABLE `user_sim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(555) NOT NULL,
  `number` varchar(555) NOT NULL,
  `label` varchar(555) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_sim
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job3` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed3',
  `job3_grade` int(11) DEFAULT 0,
  `job2` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed2',
  `job2_grade` int(11) DEFAULT 0,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `isDead` bit(1) DEFAULT b'0',
  `jail` int(11) NOT NULL DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tattoos` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`identifier`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('steam:110000104c989b7', 'license:377d85abe04fb4db2ab971dc3f006efa1b38476b', '53560656', 'Harry Chards', 0x7B226D616B6575705F34223A302C22776174636865735F32223A302C2265796562726F77735F34223A302C22626C656D69736865735F32223A302C22626C7573685F32223A302C2273686F65735F32223A302C2262726163656C6574735F32223A302C2261726D73223A302C2262656172645F33223A302C2263686573745F33223A302C2265796562726F77735F33223A302C22686169725F636F6C6F725F32223A302C22626C7573685F31223A302C22636861696E5F31223A302C2261726D735F32223A302C22656172735F32223A302C2262656172645F34223A302C226C6970737469636B5F34223A302C22636F6D706C6578696F6E5F32223A302C22636F6D706C6578696F6E5F31223A302C22736B696E223A302C226C6970737469636B5F32223A302C22626167735F32223A302C2266616365223A302C22686169725F31223A302C226D61736B5F31223A302C226D6F6C65735F31223A302C22686169725F32223A302C22626C7573685F33223A302C226167655F32223A302C2265796562726F77735F32223A302C22676C61737365735F32223A302C22646563616C735F31223A302C2262726163656C6574735F31223A2D312C2273756E5F31223A302C22626C656D69736865735F31223A302C2273756E5F32223A302C226C6970737469636B5F33223A302C22626F6479625F32223A302C226D616B6575705F31223A302C2265796562726F77735F31223A302C22676C61737365735F31223A302C22636861696E5F32223A302C226D6F6C65735F32223A302C227473686972745F31223A302C22656172735F31223A2D312C22776174636865735F31223A2D312C22686169725F636F6C6F725F31223A302C22626F6479625F31223A302C22746F72736F5F31223A302C2270616E74735F32223A302C22746F72736F5F32223A302C2268656C6D65745F32223A302C22736578223A302C226270726F6F665F32223A302C2263686573745F31223A302C2268656C6D65745F31223A2D312C226D61736B5F32223A302C2262656172645F31223A302C226579655F636F6C6F72223A302C2263686573745F32223A302C2270616E74735F31223A302C226270726F6F665F31223A302C22626167735F31223A302C226D616B6575705F33223A302C227473686972745F32223A302C226D616B6575705F32223A302C2273686F65735F31223A302C226167655F31223A302C226C6970737469636B5F31223A302C22646563616C735F32223A302C2262656172645F32223A307D, 'unemployed3', '0', 'unemployed2', '0', 'mecano', '4', 0x5B7B22636F6D706F6E656E7473223A5B5D2C22616D6D6F223A3235352C226E616D65223A2241524D45227D2C7B22636F6D706F6E656E7473223A5B5D2C22616D6D6F223A312C226E616D65223A22574541504F4E5F464C4153484C494754227D5D, '{\"x\":2064.5999999996,\"y\":4999.0,\"z\":40.600000000006}', '6800', '4', 'superadmin', 'Harry', 'Chards', '14-02-1990', 'm', '200', 0x5B7B2270657263656E74223A37332E39362C2276616C223A3733393630302C226E616D65223A2268756E676572227D2C7B2270657263656E74223A38302E34372C2276616C223A3830343730302C226E616D65223A22746869727374227D2C7B2270657263656E74223A302E302C2276616C223A302C226E616D65223A226472756E6B227D5D, '555-2072', '\0', '0', null, null);
INSERT INTO `users` VALUES ('steam:110000105e314d0', 'license:ae82f5bd4898399a031e9e852df41881e50c35b9', '0', 'Rtxx', 0x7B226D616B6575705F32223A302C226C6970737469636B5F31223A302C2273686F65735F31223A332C2263686573745F31223A302C22626C7573685F31223A302C22626F6479625F31223A302C22626167735F31223A302C22686169725F31223A31302C22626C656D69736865735F32223A302C226D616B6575705F31223A302C22636F6D706C6578696F6E5F31223A302C22776174636865735F32223A302C22746F72736F5F32223A302C22686169725F636F6C6F725F31223A342C2265796562726F77735F34223A302C2270616E74735F32223A302C226167655F31223A302C2262656172645F32223A302C226D61736B5F32223A302C2261726D73223A302C2273756E5F31223A302C22626C7573685F32223A302C22656172735F31223A2D312C226270726F6F665F32223A302C22636861696E5F32223A302C2265796562726F77735F33223A302C2273686F65735F32223A302C227473686972745F31223A332C226D616B6575705F34223A302C227473686972745F32223A322C2265796562726F77735F31223A302C2265796562726F77735F32223A302C22656172735F32223A302C22686169725F636F6C6F725F32223A362C22776174636865735F31223A2D312C22736B696E223A372C2266616365223A34342C22626167735F32223A302C22626F6479625F32223A302C22646563616C735F31223A302C226C6970737469636B5F33223A302C2268656C6D65745F32223A302C22636F6D706C6578696F6E5F32223A302C226579655F636F6C6F72223A302C22736578223A302C22636861696E5F31223A302C2262656172645F33223A302C2263686573745F33223A302C2262726163656C6574735F32223A302C22626C656D69736865735F31223A302C22746F72736F5F31223A342C22676C61737365735F31223A302C22676C61737365735F32223A302C226D6F6C65735F32223A302C22626C7573685F33223A302C22686169725F32223A302C2270616E74735F31223A302C226D6F6C65735F31223A302C2262656172645F34223A302C2268656C6D65745F31223A2D312C226C6970737469636B5F34223A302C226C6970737469636B5F32223A302C2261726D735F32223A302C2262726163656C6574735F31223A2D312C2273756E5F32223A302C226270726F6F665F31223A302C2263686573745F32223A302C226167655F32223A302C2262656172645F31223A302C226D61736B5F31223A302C226D616B6575705F33223A302C22646563616C735F32223A307D, 'unemployed3', '0', 'unemployed2', '0', 'unemployed', '0', 0x5B5D, '{\"y\":3248.2000000002,\"x\":-2354.2000000002,\"z\":46.199999999998}', '0', '0', 'user', 'Jean-Pierre', 'Chapin', '11-02-1995', 'm', '185', 0x5B7B2276616C223A302C226E616D65223A226472756E6B222C2270657263656E74223A302E307D2C7B2276616C223A3937353130302C226E616D65223A2268756E676572222C2270657263656E74223A39372E35317D2C7B2276616C223A3938313332352C226E616D65223A22746869727374222C2270657263656E74223A39382E313332357D5D, '608-1346', '\0', '0', null, null);
INSERT INTO `users` VALUES ('steam:11000010c2709d9', 'license:b28c9bd529f11c65bb6fe4be498f0f00d66106f9', '929849', 'Dan Smith', 0x7B226C6970737469636B5F33223A302C2263686573745F32223A302C22736B696E223A302C22686169725F636F6C6F725F32223A302C2262726163656C6574735F32223A302C22746F72736F5F32223A302C22636F6D706C6578696F6E5F32223A302C226D616B6575705F33223A302C22646563616C735F31223A302C22626C7573685F31223A302C22746F72736F5F31223A302C22626F6479625F32223A302C2262656172645F34223A302C2265796562726F77735F33223A302C2263686573745F33223A302C226579655F636F6C6F72223A302C2268656C6D65745F31223A2D312C2273686F65735F31223A302C2262656172645F31223A302C22646563616C735F32223A302C2262656172645F32223A302C2273686F65735F32223A302C22776174636865735F31223A2D312C22686169725F636F6C6F725F31223A302C2270616E74735F32223A302C22686169725F31223A302C2262656172645F33223A302C22626F6479625F31223A302C226C6970737469636B5F34223A302C226D61736B5F32223A302C226167655F31223A302C2263686573745F31223A302C226D61736B5F31223A302C2273756E5F31223A302C22626167735F32223A302C22736578223A302C22636861696E5F31223A302C226270726F6F665F31223A302C2266616365223A302C226D616B6575705F34223A302C22776174636865735F32223A302C22676C61737365735F32223A302C226167655F32223A302C22656172735F32223A302C2268656C6D65745F32223A302C22686169725F32223A302C22636F6D706C6578696F6E5F31223A302C22676C61737365735F31223A302C226D6F6C65735F32223A302C227473686972745F31223A302C226D616B6575705F32223A302C226D616B6575705F31223A302C2265796562726F77735F34223A302C226D6F6C65735F31223A302C22626C7573685F32223A302C2262726163656C6574735F31223A2D312C226C6970737469636B5F31223A302C2270616E74735F31223A302C2273756E5F32223A302C226C6970737469636B5F32223A302C2265796562726F77735F31223A302C2261726D73223A302C2265796562726F77735F32223A302C22656172735F31223A2D312C226270726F6F665F32223A302C22626C7573685F33223A302C22636861696E5F32223A302C22626167735F31223A302C227473686972745F32223A302C22626C656D69736865735F32223A302C22626C656D69736865735F31223A302C2261726D735F32223A307D, 'unemployed3', '0', 'unemployed2', '0', 'mecano', '4', 0x5B7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C226C6162656C223A22506973746F6C657420646520636F6D626174222C22616D6D6F223A302C226E616D65223A22574541504F4E5F434F4D424154504953544F4C227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C226C6162656C223A22506973746F6C6574206175746F6D617469717565222C22616D6D6F223A302C226E616D65223A22574541504F4E5F4150504953544F4C227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C226C6162656C223A22536D67206427617373617574222C22616D6D6F223A302C226E616D65223A22574541504F4E5F41535341554C54534D47227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C226C6162656C223A22467573696C206427617373617574222C22616D6D6F223A302C226E616D65223A22574541504F4E5F41535341554C545249464C45227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C226C6162656C223A224361726162696E65206427617373617574222C22616D6D6F223A302C226E616D65223A22574541504F4E5F43415242494E455249464C45227D5D, '{\"x\":-754.0,\"y\":-616.6000000001,\"z\":30.599999999998}', '14995271', '4', 'superadmin', 'Romain', 'Paganotto', '18-02-1997', 'm', '175', 0x5B7B2270657263656E74223A38302E36352C2276616C223A3830363530302C226E616D65223A2268756E676572227D2C7B2270657263656E74223A38352E343837352C2276616C223A3835343837352C226E616D65223A22746869727374227D2C7B2270657263656E74223A302E302C2276616C223A302C226E616D65223A226472756E6B227D5D, '555-1842', '\0', '0', null, null);
INSERT INTO `users` VALUES ('steam:11000010c2b4bd6', 'license:d3fbd2da940f5e3a43cf36c69cb747c9f9c6f34f', '88360695', 'Alexrony', 0x7B2263686573745F31223A302C226C6970737469636B5F34223A302C2262726163656C6574735F31223A2D312C2266616365223A302C22636F6D706C6578696F6E5F31223A302C2262656172645F32223A302C22626167735F31223A302C22646563616C735F31223A302C2262656172645F31223A302C2263686573745F32223A302C2263686573745F33223A302C22626C656D69736865735F32223A302C22636861696E5F32223A302C2273756E5F32223A302C227473686972745F31223A302C2261726D735F32223A302C226C6970737469636B5F31223A302C22686169725F32223A302C22686169725F636F6C6F725F32223A302C2270616E74735F32223A302C22776174636865735F32223A302C226579655F636F6C6F72223A302C22626F6479625F32223A302C22656172735F31223A2D312C2262726163656C6574735F32223A302C2268656C6D65745F31223A2D312C22626F6479625F31223A302C22636F6D706C6578696F6E5F32223A302C226C6970737469636B5F32223A302C2273756E5F31223A302C2273686F65735F32223A302C22676C61737365735F32223A302C22746F72736F5F32223A302C22626C7573685F33223A302C22746F72736F5F31223A302C22776174636865735F31223A2D312C22736B696E223A302C22736578223A302C226167655F32223A302C2261726D73223A302C22676C61737365735F31223A302C22686169725F636F6C6F725F31223A302C226D616B6575705F34223A302C226D6F6C65735F32223A302C2268656C6D65745F32223A302C226D616B6575705F33223A302C2265796562726F77735F32223A302C22646563616C735F32223A302C226D61736B5F31223A302C22626167735F32223A302C2270616E74735F31223A302C226D6F6C65735F31223A302C226270726F6F665F31223A302C22626C656D69736865735F31223A302C2265796562726F77735F31223A302C226D616B6575705F32223A302C22636861696E5F31223A302C22626C7573685F31223A302C2262656172645F34223A302C226C6970737469636B5F33223A302C22626C7573685F32223A302C22686169725F31223A302C226D616B6575705F31223A302C2273686F65735F31223A302C226270726F6F665F32223A302C22656172735F32223A302C2265796562726F77735F34223A302C2265796562726F77735F33223A302C227473686972745F32223A302C226D61736B5F32223A302C226167655F31223A302C2262656172645F33223A307D, 'unemployed3', '0', 'unemployed2', '0', 'biker', '3', 0x5B7B22636F6D706F6E656E7473223A5B5D2C22616D6D6F223A3234302C226C6162656C223A225265766F6C766572222C226E616D65223A22574541504F4E5F5245564F4C564552227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C22616D6D6F223A3235302C226C6162656C223A22536D67206427617373617574222C226E616D65223A22574541504F4E5F41535341554C54534D47227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C22616D6D6F223A3235302C226C6162656C223A22467573696C206427617373617574222C226E616D65223A22574541504F4E5F41535341554C545249464C45227D5D, '{\"y\":3342.2000000002,\"z\":32.800000000002,\"x\":-2835.5999999996}', '13034', '4', 'superadmin', 'Alex', 'Rony', '17-02-1997', 'm', '187', 0x5B7B226E616D65223A226472756E6B222C2276616C223A302C2270657263656E74223A302E307D2C7B226E616D65223A2268756E676572222C2276616C223A3432373030302C2270657263656E74223A34322E377D2C7B226E616D65223A22746869727374222C2276616C223A3434353235302C2270657263656E74223A34342E3532357D5D, '555-2463', '\0', '0', null, null);
INSERT INTO `users` VALUES ('steam:11000010f9264c0', 'license:0ca98e49bd4464c1d683de2f1710d799daa9d72e', '0', 'ColoNeil', 0x7B22636861696E5F31223A302C22636F6D706C6578696F6E5F32223A302C2273686F65735F32223A322C22626C7573685F32223A302C22626C7573685F31223A302C22736B696E223A31352C2270616E74735F32223A302C226167655F31223A302C2262656172645F33223A302C22626C656D69736865735F31223A302C2263686573745F31223A302C22676C61737365735F32223A302C226C6970737469636B5F34223A302C2262656172645F31223A31322C226D61736B5F31223A302C2265796562726F77735F32223A382C22626F6479625F31223A302C22746F72736F5F32223A32332C2268656C6D65745F31223A2D312C226D616B6575705F34223A302C22676C61737365735F31223A302C2265796562726F77735F34223A302C226C6970737469636B5F31223A302C227473686972745F31223A31352C2262656172645F34223A302C22636F6D706C6578696F6E5F31223A302C2268656C6D65745F32223A302C226D6F6C65735F32223A302C22656172735F32223A302C226D616B6575705F31223A302C226167655F32223A302C226D61736B5F32223A302C22746F72736F5F31223A3236302C22626167735F31223A302C2273756E5F31223A302C22646563616C735F32223A302C2265796562726F77735F31223A302C22626C7573685F33223A302C2263686573745F32223A302C2273756E5F32223A302C22656172735F31223A2D312C2261726D73223A302C226D6F6C65735F31223A302C2262656172645F32223A31302C2262726163656C6574735F31223A332C22626167735F32223A302C2265796562726F77735F33223A302C226D616B6575705F32223A302C226579655F636F6C6F72223A312C2273686F65735F31223A33312C22636861696E5F32223A302C2266616365223A33312C22776174636865735F32223A302C226C6970737469636B5F33223A302C22686169725F32223A302C227473686972745F32223A302C22646563616C735F31223A302C226D616B6575705F33223A302C22686169725F636F6C6F725F32223A302C2261726D735F32223A302C22686169725F636F6C6F725F31223A302C22736578223A302C22626F6479625F32223A302C22686169725F31223A32392C22776174636865735F31223A342C226270726F6F665F32223A302C22626C656D69736865735F32223A302C2270616E74735F31223A342C2263686573745F33223A302C226270726F6F665F31223A302C2262726163656C6574735F32223A312C226C6970737469636B5F32223A307D, 'unemployed3', '0', 'unemployed2', '0', 'unemployed', '0', 0x5B5D, '{\"y\":-209.79999999998,\"x\":0.19999999999998,\"z\":52.800000000002}', '900', '0', 'user', 'Neil', 'Barrett', '31/10/1997', 'm', '186', 0x5B7B2276616C223A302C226E616D65223A226472756E6B222C2270657263656E74223A302E307D2C7B2276616C223A3631343730302C226E616D65223A2268756E676572222C2270657263656E74223A36312E34377D2C7B2276616C223A3731313032352C226E616D65223A22746869727374222C2270657263656E74223A37312E313032357D5D, '777-1215', '\0', '0', null, null);
INSERT INTO `users` VALUES ('steam:110000117fd8bdf', 'license:efea12af01f2fe99eb548f58c03011b9dc283af9', '7908', 'MICKEY MOUSE', 0x7B2263686573745F31223A302C2270616E74735F32223A302C227473686972745F32223A302C22636F6D706C6578696F6E5F32223A302C2265796562726F77735F32223A302C226D6F6C65735F32223A302C22686169725F636F6C6F725F32223A302C2273756E5F31223A302C226167655F31223A302C22686169725F31223A31332C22646563616C735F32223A302C226D616B6575705F33223A302C22626C656D69736865735F32223A302C226C6970737469636B5F32223A302C22636861696E5F31223A302C2273686F65735F32223A302C226D6F6C65735F31223A302C22636861696E5F32223A302C2266616365223A372C22776174636865735F32223A302C2261726D735F32223A302C22636F6D706C6578696F6E5F31223A302C227473686972745F31223A302C22736B696E223A352C2265796562726F77735F31223A302C2268656C6D65745F32223A302C22746F72736F5F31223A302C22626C7573685F33223A302C22626C7573685F31223A302C22626167735F31223A302C226C6970737469636B5F34223A302C226C6970737469636B5F33223A302C226C6970737469636B5F31223A302C226D616B6575705F31223A302C22776174636865735F31223A2D312C22686169725F32223A302C2262656172645F31223A302C22676C61737365735F32223A302C226D616B6575705F34223A302C226270726F6F665F31223A302C2270616E74735F31223A302C2262726163656C6574735F31223A2D312C2265796562726F77735F34223A302C2268656C6D65745F31223A2D312C22626C656D69736865735F31223A302C2265796562726F77735F33223A302C22626C7573685F32223A302C22676C61737365735F31223A302C22746F72736F5F32223A302C2262656172645F33223A302C22626167735F32223A302C22736578223A302C22656172735F31223A2D312C2262726163656C6574735F32223A302C226167655F32223A302C2262656172645F32223A302C2273686F65735F31223A302C22626F6479625F31223A302C226D61736B5F32223A302C226270726F6F665F32223A302C22626F6479625F32223A302C2263686573745F32223A302C2261726D73223A302C2273756E5F32223A302C2263686573745F33223A302C226579655F636F6C6F72223A302C22656172735F32223A302C22686169725F636F6C6F725F31223A302C22646563616C735F31223A302C2262656172645F34223A302C226D616B6575705F32223A302C226D61736B5F31223A307D, 'unemployed3', '0', 'taxi', '0', 'realestateagent', '3', 0x5B7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C22616D6D6F223A302C226E616D65223A22574541504F4E5F504953544F4C222C226C6162656C223A22506973746F6C6574227D2C7B22636F6D706F6E656E7473223A5B22636C69705F64656661756C74225D2C22616D6D6F223A3132322C226E616D65223A22574541504F4E5F43415242494E455249464C45222C226C6162656C223A224361726162696E65206427617373617574227D5D, '{\"z\":34.600000000006,\"y\":-571.80000000004,\"x\":-207.59999999998}', '10013123', '4', 'superadmin', 'Joe', 'Cook', '05-11-1995', 'm', '190', 0x5B7B2270657263656E74223A302E302C2276616C223A302C226E616D65223A226472756E6B227D2C7B2270657263656E74223A31302E31362C2276616C223A3130313630302C226E616D65223A2268756E676572227D2C7B2270657263656E74223A32302E31322C2276616C223A3230313230302C226E616D65223A22746869727374227D5D, '555-1396', '\0', '0', null, null);

-- ----------------------------
-- Table structure for `vehicle_categories`
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_categories`;
CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of vehicle_categories
-- ----------------------------
INSERT INTO `vehicle_categories` VALUES ('compacts', 'Compacts');
INSERT INTO `vehicle_categories` VALUES ('coupes', 'Coupés');
INSERT INTO `vehicle_categories` VALUES ('imports', 'Imports');
INSERT INTO `vehicle_categories` VALUES ('muscle', 'Muscle');
INSERT INTO `vehicle_categories` VALUES ('offroad', 'Off Road');
INSERT INTO `vehicle_categories` VALUES ('sedans', 'Sedans');
INSERT INTO `vehicle_categories` VALUES ('sports', 'Sports');
INSERT INTO `vehicle_categories` VALUES ('sportsclassics', 'Sports Classics');
INSERT INTO `vehicle_categories` VALUES ('super', 'Super');
INSERT INTO `vehicle_categories` VALUES ('suvs', 'SUVs');
INSERT INTO `vehicle_categories` VALUES ('vans', 'Vans');

-- ----------------------------
-- Table structure for `vehicle_sold`
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_sold`;
CREATE TABLE `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of vehicle_sold
-- ----------------------------
INSERT INTO `vehicle_sold` VALUES ('Alexrony', 'alpha', 'OMG 984', 'Dan Smith', '2020-05-23 10:36');
INSERT INTO `vehicle_sold` VALUES ('Alexrony', 'alpha', 'PZC 366', 'Dan Smith', '2020-05-23 10:36');
INSERT INTO `vehicle_sold` VALUES ('Alexrony', 'blista', 'RKU 682', 'Dan Smith', '2020-05-23 10:42');
INSERT INTO `vehicle_sold` VALUES ('Alexrony', 'sultanrs', 'VBY 747', 'Dan Smith', '2020-05-23 12:46');

-- ----------------------------
-- Table structure for `vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('Adder', 'adder', '620000', 'super', null);
INSERT INTO `vehicles` VALUES ('Alpha', 'alpha', '60000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Asea', 'asea', '5500', 'sedans', 'asea.jpg');
INSERT INTO `vehicles` VALUES ('Autarch', 'autarch', '450000', 'super', null);
INSERT INTO `vehicles` VALUES ('Baller', 'baller2', '40000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Baller Sport', 'baller3', '60000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Banshee', 'banshee', '60000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Banshee 900R', 'banshee2', '295000', 'super', null);
INSERT INTO `vehicles` VALUES ('Bestia GTS', 'bestiagts', '200000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Bf Injection', 'bfinjection', '15000', 'offroad', 'bfinjection.jpg');
INSERT INTO `vehicles` VALUES ('Bifta', 'bifta', '15000', 'offroad', 'bifta.jpg');
INSERT INTO `vehicles` VALUES ('Bison', 'bison', '15000', 'vans', 'bison.jpg');
INSERT INTO `vehicles` VALUES ('Blade', 'blade', '15000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Blazer', 'blazer', '12500', 'offroad', 'blazer.jpg');
INSERT INTO `vehicles` VALUES ('Blazer Sport', 'blazer4', '15000', 'offroad', 'street-blazer.jpg');
INSERT INTO `vehicles` VALUES ('Blista', 'blista', '25000', 'compacts', 'blista.jpg');
INSERT INTO `vehicles` VALUES ('Bobcat XL', 'bobcatxl', '20000', 'vans', 'bobcat-xl.jpg');
INSERT INTO `vehicles` VALUES ('Brawler', 'brawler', '45000', 'offroad', 'brawler.jpg');
INSERT INTO `vehicles` VALUES ('Brioso R/A', 'brioso', '18000', 'compacts', 'brioso.jpg');
INSERT INTO `vehicles` VALUES ('Btype', 'btype', '120000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Btype Hotroad', 'btype2', '200000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Btype Luxe', 'btype3', '170000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Buccaneer', 'buccaneer', '15000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Buccaneer Rider', 'buccaneer2', '24000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Buffalo', 'buffalo', '12000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Buffalo S', 'buffalo2', '30000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Bullet', 'bullet', '45000', 'super', null);
INSERT INTO `vehicles` VALUES ('Burrito', 'burrito3', '13000', 'vans', 'burrito3.jpg');
INSERT INTO `vehicles` VALUES ('Camper', 'camper', '30000', 'vans', 'camper.jpg');
INSERT INTO `vehicles` VALUES ('Carbonizzare', 'carbonizzare', '75000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Casco', 'casco', '120000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Cavalcade', 'cavalcade2', '35000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Cheetah', 'cheetah', '325000', 'super', null);
INSERT INTO `vehicles` VALUES ('Chino', 'chino', '15000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Chino Luxe', 'chino2', '19000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Cognoscenti Cabrio', 'cogcabrio', '75000', 'coupes', 'cognoscenti.jpg');
INSERT INTO `vehicles` VALUES ('Cognoscenti', 'cognoscenti', '60000', 'sedans', 'cognoscenti.jpg');
INSERT INTO `vehicles` VALUES ('Comet', 'comet2', '65000', 'sports', 'comet-sr.jpg');
INSERT INTO `vehicles` VALUES ('Comet 5', 'comet5', '250000', 'sports', 'comet-sr.jpg');
INSERT INTO `vehicles` VALUES ('Contender', 'contender', '50000', 'suvs', '');
INSERT INTO `vehicles` VALUES ('Coquette', 'coquette', '55000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Coquette Classic', 'coquette2', '40000', 'sportsclassics', 'coquette-classic.jpg');
INSERT INTO `vehicles` VALUES ('Coquette BlackFin', 'coquette3', '41000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Cyclone', 'cyclone', '250000', 'super', null);
INSERT INTO `vehicles` VALUES ('Dominator', 'dominator', '30000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('concept', 'dominator5', '120000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Dubsta', 'dubsta', '45000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Dubsta Luxuary', 'dubsta2', '60000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Bubsta 6x6', 'dubsta3', '100000', 'offroad', null);
INSERT INTO `vehicles` VALUES ('Dukes', 'dukes', '17000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Dune Buggy', 'dune', '8000', 'offroad', 'dune-buggy.jpg');
INSERT INTO `vehicles` VALUES ('Elegy', 'elegy2', '102000', 'sports', 'Elegy.jpg');
INSERT INTO `vehicles` VALUES ('Emperor', 'emperor', '8500', 'sedans', 'emperor.jpg');
INSERT INTO `vehicles` VALUES ('Exemplar', 'exemplar', '32000', 'coupes', 'exemplar.jpg');
INSERT INTO `vehicles` VALUES ('F620', 'f620', '60000', 'coupes', 'f620.jpg');
INSERT INTO `vehicles` VALUES ('Faction', 'faction', '20000', 'muscle', 'faction.jpg');
INSERT INTO `vehicles` VALUES ('Faction Rider', 'faction2', '30000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Faction XL', 'faction3', '40000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Felon', 'felon', '25000', 'coupes', 'felon.jpg');
INSERT INTO `vehicles` VALUES ('Felon GT', 'felon2', '40000', 'coupes', null);
INSERT INTO `vehicles` VALUES ('Feltzer', 'feltzer2', '55000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Stirling GT', 'feltzer3', '85000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('FMJ', 'fmj', '350000', 'super', null);
INSERT INTO `vehicles` VALUES ('Fhantom', 'fq2', '15000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Fugitive', 'fugitive', '12000', 'sedans', 'fugitive.jpg');
INSERT INTO `vehicles` VALUES ('Furore GT', 'furoregt', '80000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Fusilade', 'fusilade', '35000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Gauntlet', 'gauntlet', '30000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Gang Burrito', 'gburrito', '20000', 'vans', 'GangBurrito2.jpg');
INSERT INTO `vehicles` VALUES ('Burrito', 'gburrito2', '20000', 'vans', null);
INSERT INTO `vehicles` VALUES ('Glendale', 'glendale', '6500', 'sedans', 'glendale.jpg');
INSERT INTO `vehicles` VALUES ('Grabger', 'granger', '25000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Gresley', 'gresley', '35000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('GT 500', 'gt500', '585000', 'sportsclassics', 'gt500.jpg');
INSERT INTO `vehicles` VALUES ('Guardian', 'guardian', '45000', 'offroad', null);
INSERT INTO `vehicles` VALUES ('Hermes', 'hermes', '120000', 'muscle', 'hermes.jpg');
INSERT INTO `vehicles` VALUES ('Hotknife', 'hotknife', '125000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Huntley S', 'huntley', '65000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Hustler', 'hustler', '150000', 'muscle', 'hustler.jpg');
INSERT INTO `vehicles` VALUES ('Infernus', 'infernus', '180000', 'super', null);
INSERT INTO `vehicles` VALUES ('Intruder', 'intruder', '7500', 'sedans', 'intruder.jpg');
INSERT INTO `vehicles` VALUES ('Issi', 'issi2', '5000', 'compacts', 'issi.jpg');
INSERT INTO `vehicles` VALUES ('Jackal', 'jackal', '15000', 'coupes', 'jackal.jpg');
INSERT INTO `vehicles` VALUES ('Jester', 'jester', '100000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Jester(Racecar', 'jester2', '150000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Journey', 'journey', '6500', 'vans', 'journey.jpg');
INSERT INTO `vehicles` VALUES ('Kamacho', 'kamacho', '90000', 'offroad', 'kamacho.jpg');
INSERT INTO `vehicles` VALUES ('Khamelion', 'khamelion', '120000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Kuruma', 'kuruma', '100000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Landstalker', 'landstalker', '25000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('RE-7B', 'le7b', '429000', 'super', null);
INSERT INTO `vehicles` VALUES ('Lexus', 'lexgs350', '37999', 'imports', null);
INSERT INTO `vehicles` VALUES ('Lynx', 'lynx', '140000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Mamba', 'mamba', '105000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Manana', 'manana', '10000', 'sportsclassics', 'manana.jpg');
INSERT INTO `vehicles` VALUES ('Massacro', 'massacro', '110000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Massacro(Racecar', 'massacro2', '150000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Mesa', 'mesa', '25000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Mesa Trail', 'mesa3', '50000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Minivan', 'minivan', '10000', 'vans', 'minivan.jpg');
INSERT INTO `vehicles` VALUES ('Monroe', 'monroe', '155000', 'sportsclassics', 'monroe.jpg');
INSERT INTO `vehicles` VALUES ('The Liberator', 'monster', '350000', 'offroad', null);
INSERT INTO `vehicles` VALUES ('Moonbeam', 'moonbeam', '18000', 'vans', 'Moonbeam.jpg');
INSERT INTO `vehicles` VALUES ('Moonbeam Rider', 'moonbeam2', '35000', 'vans', 'Moonbeam2.jpg');
INSERT INTO `vehicles` VALUES ('Neon', 'neon', '320000', 'sports', 'neon-8.jpg');
INSERT INTO `vehicles` VALUES ('Nightshade', 'nightshade', '60000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('9F', 'ninef', '100000', 'sports', null);
INSERT INTO `vehicles` VALUES ('9F Cabrio', 'ninef2', '120000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Omnis', 'omnis', '135000', 'sports', 'omnis.jpg');
INSERT INTO `vehicles` VALUES ('Oracle XS', 'oracle2', '35000', 'coupes', null);
INSERT INTO `vehicles` VALUES ('Osiris', 'osiris', '460000', 'super', null);
INSERT INTO `vehicles` VALUES ('Panto', 'panto', '10000', 'compacts', 'panto-3.jpg');
INSERT INTO `vehicles` VALUES ('Paradise', 'paradise', '15000', 'vans', 'paradise.jpg');
INSERT INTO `vehicles` VALUES ('Pariah', 'pariah', '190000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Patriot', 'patriot', '20000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Penumbra', 'penumbra', '10500', 'sports', null);
INSERT INTO `vehicles` VALUES ('Pfister', 'pfister811', '600000', 'super', null);
INSERT INTO `vehicles` VALUES ('Phoenix', 'phoenix', '10000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Picador', 'picador', '10000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Pigalle', 'pigalle', '20000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Prairie', 'prairie', '12000', 'compacts', 'prairie.jpg');
INSERT INTO `vehicles` VALUES ('Premier', 'premier', '18000', 'sedans', 'premier.jpg');
INSERT INTO `vehicles` VALUES ('Primo Custom', 'primo2', '14000', 'sedans', 'primo.jpg');
INSERT INTO `vehicles` VALUES ('X80 Proto', 'prototipo', '1000000', 'super', null);
INSERT INTO `vehicles` VALUES ('Radius', 'radi', '18000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('raiden', 'raiden', '275000', 'sports', 'raiden.jpg');
INSERT INTO `vehicles` VALUES ('Rapid GT', 'rapidgt', '35000', 'sports', 'rapidegt.jpg');
INSERT INTO `vehicles` VALUES ('Rapid GT Convertible', 'rapidgt2', '100000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Rapid GT3', 'rapidgt3', '95000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Reaper', 'reaper', '250000', 'super', null);
INSERT INTO `vehicles` VALUES ('Rebel', 'rebel2', '4000', 'offroad', null);
INSERT INTO `vehicles` VALUES ('Regina', 'regina', '4000', 'sedans', 'regina.jpg');
INSERT INTO `vehicles` VALUES ('Renault 5', 'ren_lecar', '9999', 'imports', null);
INSERT INTO `vehicles` VALUES ('Retinue', 'retinue', '50000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Revolter', 'revolter', '150000', 'sports', 'revolter.jpg');
INSERT INTO `vehicles` VALUES ('riata', 'riata', '70000', 'offroad', 'riata-8.jpg');
INSERT INTO `vehicles` VALUES ('Rocoto', 'rocoto', '45000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Rumpo', 'rumpo', '15000', 'vans', 'rumpo.jpg');
INSERT INTO `vehicles` VALUES ('Rumpo Trail', 'rumpo3', '19500', 'vans', 'Rumpo3.jpg');
INSERT INTO `vehicles` VALUES ('Sabre Turbo', 'sabregt', '10900', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Sabre GT', 'sabregt2', '15000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Sandking', 'sandking', '55000', 'offroad', 'SandkingXL.jpg');
INSERT INTO `vehicles` VALUES ('Savestra', 'savestra', '99000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('SC 1', 'sc1', '4560000', 'super', null);
INSERT INTO `vehicles` VALUES ('Schafter', 'schafter2', '35000', 'sedans', '');
INSERT INTO `vehicles` VALUES ('Schafter V12', 'schafter3', '80000', 'sports', 'schafter-v12.jpg');
INSERT INTO `vehicles` VALUES ('Seminole', 'seminole', '25000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Sentinel', 'sentinel', '20000', 'coupes', 'sentinel-classic.jpg');
INSERT INTO `vehicles` VALUES ('Sentinel XS', 'sentinel2', '25000', 'coupes', 'sentinel.jpg');
INSERT INTO `vehicles` VALUES ('Sentinel3', 'sentinel3', '45000', 'sports', 'sentinel.jpg');
INSERT INTO `vehicles` VALUES ('Seven 70', 'seven70', '200000', 'sports', null);
INSERT INTO `vehicles` VALUES ('ETR1', 'sheava', '390000', 'super', null);
INSERT INTO `vehicles` VALUES ('Slam Van', 'slamvan3', '11500', 'muscle', 'slamvan.jpg');
INSERT INTO `vehicles` VALUES ('Stinger', 'stinger', '280000', 'sportsclassics', 'stinger.jpg');
INSERT INTO `vehicles` VALUES ('Stinger GT', 'stingergt', '350000', 'sportsclassics', 'stinger-gt.jpg');
INSERT INTO `vehicles` VALUES ('Streiter', 'streiter', '120000', 'sports', 'streiter.jpg');
INSERT INTO `vehicles` VALUES ('Stretch', 'stretch', '90000', 'sedans', 'stretch.jpg');
INSERT INTO `vehicles` VALUES ('Sultan', 'sultan', '55000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Sultan RS', 'sultanrs', '365000', 'super', null);
INSERT INTO `vehicles` VALUES ('Super Diamond', 'superd', '100000', 'sedans', 'super-diamond.jpg');
INSERT INTO `vehicles` VALUES ('Surano', 'surano', '75000', 'sports', 'surano.jpg');
INSERT INTO `vehicles` VALUES ('Surfer', 'surfer', '11000', 'vans', 'surfer.jpg');
INSERT INTO `vehicles` VALUES ('T20', 't20', '600000', 'super', null);
INSERT INTO `vehicles` VALUES ('Tailgater', 'tailgater', '25000', 'sedans', 'tailgater.jpg');
INSERT INTO `vehicles` VALUES ('Tampa', 'tampa', '19000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Drift Tampa', 'tampa2', '210000', 'sports', null);
INSERT INTO `vehicles` VALUES ('Trophy Truck', 'trophytruck', '60000', 'offroad', null);
INSERT INTO `vehicles` VALUES ('Trophy Truck Limited', 'trophytruck2', '80000', 'offroad', 'TrophyTruck2.jpg');
INSERT INTO `vehicles` VALUES ('Tropos', 'tropos', '115000', 'sports', 'Tropos.jpg');
INSERT INTO `vehicles` VALUES ('Turismo R', 'turismor', '530000', 'super', null);
INSERT INTO `vehicles` VALUES ('Tyrus', 'tyrus', '600000', 'super', null);
INSERT INTO `vehicles` VALUES ('Vacca', 'vacca', '250000', 'super', null);
INSERT INTO `vehicles` VALUES ('Verlierer', 'verlierer2', '170000', 'sports', 'verlierer.jpg');
INSERT INTO `vehicles` VALUES ('Vigero', 'vigero', '12500', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Virgo', 'virgo', '14000', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Viseris', 'viseris', '226000', 'sportsclassics', null);
INSERT INTO `vehicles` VALUES ('Visione', 'visione', '953000', 'super', null);
INSERT INTO `vehicles` VALUES ('Voltic', 'voltic', '103000', 'super', null);
INSERT INTO `vehicles` VALUES ('Voodoo', 'voodoo', '7200', 'muscle', null);
INSERT INTO `vehicles` VALUES ('Warrener', 'warrener', '6000', 'sedans', 'warrener.jpg');
INSERT INTO `vehicles` VALUES ('Washington', 'washington', '9000', 'sedans', 'washington.jpg');
INSERT INTO `vehicles` VALUES ('Windsor', 'windsor', '136000', 'coupes', 'windsor.jpg');
INSERT INTO `vehicles` VALUES ('Windsor Drop', 'windsor2', '150000', 'coupes', null);
INSERT INTO `vehicles` VALUES ('XLS', 'xls', '42000', 'suvs', null);
INSERT INTO `vehicles` VALUES ('Yosemite', 'yosemite', '30000', 'muscle', 'yosemite.jpg');
INSERT INTO `vehicles` VALUES ('Youga', 'youga', '9500', 'vans', 'youga.jpg');
INSERT INTO `vehicles` VALUES ('Youga Luxuary', 'youga2', '12000', 'vans', null);
INSERT INTO `vehicles` VALUES ('Z190', 'z190', '105000', 'sportsclassics', 'z190.jpg');
INSERT INTO `vehicles` VALUES ('Zentorno', 'zentorno', '730000', 'super', null);
INSERT INTO `vehicles` VALUES ('Zion', 'zion', '25000', 'coupes', 'zion.jpg');
INSERT INTO `vehicles` VALUES ('Zion Cabrio', 'zion2', '30000', 'coupes', null);
INSERT INTO `vehicles` VALUES ('ZR380', 'zr380', '10', 'super', null);
INSERT INTO `vehicles` VALUES ('Z-Type', 'ztype', '550000', 'sportsclassics', null);

-- ----------------------------
-- Table structure for `weapon_boutique`
-- ----------------------------
DROP TABLE IF EXISTS `weapon_boutique`;
CREATE TABLE `weapon_boutique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weapon` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weapon_boutique
-- ----------------------------
INSERT INTO `weapon_boutique` VALUES ('1', 'Batte de baseball', 'corpcorp', '1000', 'cac-batte.png');
INSERT INTO `weapon_boutique` VALUES ('2', 'Couteau', 'corpcorp', '10000', 'cac-couteau.png');
INSERT INTO `weapon_boutique` VALUES ('3', 'Lampe de poche', 'corpcorp', '500', 'cac-lampe-de-poche.png');
INSERT INTO `weapon_boutique` VALUES ('4', 'Matraque', 'corpcorp', '2000', 'cac-matraque.png');
INSERT INTO `weapon_boutique` VALUES ('5', 'Poing americain', 'corpcorp', '10000', 'cac-poing-americain.png');
INSERT INTO `weapon_boutique` VALUES ('6', 'Machette', 'corpcorp', '15000', 'cac-machette.png');
INSERT INTO `weapon_boutique` VALUES ('7', 'Pistolet', 'pistolet', '30000', 'pi-pistolet.png');
INSERT INTO `weapon_boutique` VALUES ('8', 'Pistolet Cal.50', 'pistolet', '45000', 'pi-pistolet-cal50.png');
INSERT INTO `weapon_boutique` VALUES ('9', 'Pistolet paralysant', 'pistolet', '20000', 'pi-pistolet-paralysant.png');

-- ----------------------------
-- Table structure for `weapon_boutique_cat`
-- ----------------------------
DROP TABLE IF EXISTS `weapon_boutique_cat`;
CREATE TABLE `weapon_boutique_cat` (
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weapon_boutique_cat
-- ----------------------------
INSERT INTO `weapon_boutique_cat` VALUES ('corpcorp', 'Corps à corps');
INSERT INTO `weapon_boutique_cat` VALUES ('pistolet', 'Pistolets et revolvers');

-- ----------------------------
-- Table structure for `weashops`
-- ----------------------------
DROP TABLE IF EXISTS `weashops`;
CREATE TABLE `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of weashops
-- ----------------------------
INSERT INTO `weashops` VALUES ('3', 'GunShop', 'WEAPON_FLASHLIGHT', '600');
INSERT INTO `weashops` VALUES ('4', 'BlackWeashop', 'WEAPON_FLASHLIGHT', '700');
INSERT INTO `weashops` VALUES ('5', 'GunShop', 'WEAPON_MACHETE', '900');
INSERT INTO `weashops` VALUES ('6', 'BlackWeashop', 'WEAPON_MACHETE', '1100');
INSERT INTO `weashops` VALUES ('7', 'GunShop', 'WEAPON_NIGHTSTICK', '1500');
INSERT INTO `weashops` VALUES ('8', 'BlackWeashop', 'WEAPON_NIGHTSTICK', '1500');
INSERT INTO `weashops` VALUES ('9', 'GunShop', 'WEAPON_BAT', '1000');
INSERT INTO `weashops` VALUES ('10', 'BlackWeashop', 'WEAPON_BAT', '1000');
INSERT INTO `weashops` VALUES ('11', 'GunShop', 'WEAPON_STUNGUN', '500');
INSERT INTO `weashops` VALUES ('12', 'BlackWeashop', 'WEAPON_STUNGUN', '500');

-- ----------------------------
-- Table structure for `whitelist`
-- ----------------------------
DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE `whitelist` (
  `identifier` varchar(75) NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of whitelist
-- ----------------------------
INSERT INTO `whitelist` VALUES ('steam:110000104c989b7');
INSERT INTO `whitelist` VALUES ('steam:11000010c2709d9');
INSERT INTO `whitelist` VALUES ('steam:11000010c2b4bd6');
INSERT INTO `whitelist` VALUES ('steam:11000010f9264c0');
